package com.cloudmetrics.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudmetricDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudmetricDbApplication.class, args);
	}

}
