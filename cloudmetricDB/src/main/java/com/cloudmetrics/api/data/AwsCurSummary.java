// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Version;

@Entity(name="aws_cur_summary")
public class AwsCurSummary implements Serializable {

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Column(name="identity_timeinterval", length=50)
    private String identityTimeinterval;
    @Column(name="bill_invoiceid", length=50)
    private String billInvoiceid;
    @Column(name="bill_billingentity", length=50)
    private String billBillingentity;
    @Column(name="bill_payeraccountid", length=50)
    private String billPayeraccountid;
    @Column(name="bill_billingperiodstartdate")
    private Timestamp billBillingperiodstartdate;
    @Column(name="bill_billingperiodenddate")
    private Timestamp billBillingperiodenddate;
    @Column(name="lineitem_usageaccountid", length=50)
    private String lineitemUsageaccountid;
    @Column(name="lineitem_productcode", length=50)
    private String lineitemProductcode;
    @Column(name="lineitem_resourceid", length=100)
    private String lineitemResourceid;
    @Column(name="lineitem_usageamount", precision=131089)
    private BigDecimal lineitemUsageamount;
    @Column(name="lineitem_normalizationfactor", precision=131089)
    private BigDecimal lineitemNormalizationfactor;
    @Column(name="lineitem_normalizedusageamount", precision=131089)
    private BigDecimal lineitemNormalizedusageamount;
    @Column(name="lineitem_currencycode", length=10)
    private String lineitemCurrencycode;
    @Column(name="lineitem_unblendedrate", precision=131089)
    private BigDecimal lineitemUnblendedrate;
    @Column(name="lineitem_unblendedcost", precision=131089)
    private BigDecimal lineitemUnblendedcost;
    @Column(name="lineitem_blendedrate", precision=131089)
    private BigDecimal lineitemBlendedrate;
    @Column(name="lineitem_blendedcost", precision=131089)
    private BigDecimal lineitemBlendedcost;
    @Column(name="lineitem_taxtype", length=50)
    private String lineitemTaxtype;
    @Column(name="product_productname", length=50)
    private String productProductname;
    @Column(name="product_availability", length=50)
    private String productAvailability;
    @Column(name="product_instancetype", length=50)
    private String productInstancetype;
    @Column(name="product_productfamily", length=100)
    private String productProductfamily;
    @Column(name="product_region", length=50)
    private String productRegion;
    @Column(name="pricing_publicondemandcost", precision=131089)
    private BigDecimal pricingPublicondemandcost;
    @Column(name="pricing_publicondemandrate", precision=131089)
    private BigDecimal pricingPublicondemandrate;
    @Column(name="pricing_unit", length=50)
    private String pricingUnit;
    @Column(precision=10)
    private int month;
    @Column(precision=10)
    private int year;
    @Column(name="total_cost", precision=131089)
    private BigDecimal totalCost;
    @Column(length=4000)
    private String tags;

    /** Default constructor. */
    public AwsCurSummary() {
        super();
    }

    /**
     * Access method for identityTimeinterval.
     *
     * @return the current value of identityTimeinterval
     */
    public String getIdentityTimeinterval() {
        return identityTimeinterval;
    }

    /**
     * Setter method for identityTimeinterval.
     *
     * @param aIdentityTimeinterval the new value for identityTimeinterval
     */
    public void setIdentityTimeinterval(String aIdentityTimeinterval) {
        identityTimeinterval = aIdentityTimeinterval;
    }

    /**
     * Access method for billInvoiceid.
     *
     * @return the current value of billInvoiceid
     */
    public String getBillInvoiceid() {
        return billInvoiceid;
    }

    /**
     * Setter method for billInvoiceid.
     *
     * @param aBillInvoiceid the new value for billInvoiceid
     */
    public void setBillInvoiceid(String aBillInvoiceid) {
        billInvoiceid = aBillInvoiceid;
    }

    /**
     * Access method for billBillingentity.
     *
     * @return the current value of billBillingentity
     */
    public String getBillBillingentity() {
        return billBillingentity;
    }

    /**
     * Setter method for billBillingentity.
     *
     * @param aBillBillingentity the new value for billBillingentity
     */
    public void setBillBillingentity(String aBillBillingentity) {
        billBillingentity = aBillBillingentity;
    }

    /**
     * Access method for billPayeraccountid.
     *
     * @return the current value of billPayeraccountid
     */
    public String getBillPayeraccountid() {
        return billPayeraccountid;
    }

    /**
     * Setter method for billPayeraccountid.
     *
     * @param aBillPayeraccountid the new value for billPayeraccountid
     */
    public void setBillPayeraccountid(String aBillPayeraccountid) {
        billPayeraccountid = aBillPayeraccountid;
    }

    /**
     * Access method for billBillingperiodstartdate.
     *
     * @return the current value of billBillingperiodstartdate
     */
    public Timestamp getBillBillingperiodstartdate() {
        return billBillingperiodstartdate;
    }

    /**
     * Setter method for billBillingperiodstartdate.
     *
     * @param aBillBillingperiodstartdate the new value for billBillingperiodstartdate
     */
    public void setBillBillingperiodstartdate(Timestamp aBillBillingperiodstartdate) {
        billBillingperiodstartdate = aBillBillingperiodstartdate;
    }

    /**
     * Access method for billBillingperiodenddate.
     *
     * @return the current value of billBillingperiodenddate
     */
    public Timestamp getBillBillingperiodenddate() {
        return billBillingperiodenddate;
    }

    /**
     * Setter method for billBillingperiodenddate.
     *
     * @param aBillBillingperiodenddate the new value for billBillingperiodenddate
     */
    public void setBillBillingperiodenddate(Timestamp aBillBillingperiodenddate) {
        billBillingperiodenddate = aBillBillingperiodenddate;
    }

    /**
     * Access method for lineitemUsageaccountid.
     *
     * @return the current value of lineitemUsageaccountid
     */
    public String getLineitemUsageaccountid() {
        return lineitemUsageaccountid;
    }

    /**
     * Setter method for lineitemUsageaccountid.
     *
     * @param aLineitemUsageaccountid the new value for lineitemUsageaccountid
     */
    public void setLineitemUsageaccountid(String aLineitemUsageaccountid) {
        lineitemUsageaccountid = aLineitemUsageaccountid;
    }

    /**
     * Access method for lineitemProductcode.
     *
     * @return the current value of lineitemProductcode
     */
    public String getLineitemProductcode() {
        return lineitemProductcode;
    }

    /**
     * Setter method for lineitemProductcode.
     *
     * @param aLineitemProductcode the new value for lineitemProductcode
     */
    public void setLineitemProductcode(String aLineitemProductcode) {
        lineitemProductcode = aLineitemProductcode;
    }

    /**
     * Access method for lineitemResourceid.
     *
     * @return the current value of lineitemResourceid
     */
    public String getLineitemResourceid() {
        return lineitemResourceid;
    }

    /**
     * Setter method for lineitemResourceid.
     *
     * @param aLineitemResourceid the new value for lineitemResourceid
     */
    public void setLineitemResourceid(String aLineitemResourceid) {
        lineitemResourceid = aLineitemResourceid;
    }

    /**
     * Access method for lineitemUsageamount.
     *
     * @return the current value of lineitemUsageamount
     */
    public BigDecimal getLineitemUsageamount() {
        return lineitemUsageamount;
    }

    /**
     * Setter method for lineitemUsageamount.
     *
     * @param aLineitemUsageamount the new value for lineitemUsageamount
     */
    public void setLineitemUsageamount(BigDecimal aLineitemUsageamount) {
        lineitemUsageamount = aLineitemUsageamount;
    }

    /**
     * Access method for lineitemNormalizationfactor.
     *
     * @return the current value of lineitemNormalizationfactor
     */
    public BigDecimal getLineitemNormalizationfactor() {
        return lineitemNormalizationfactor;
    }

    /**
     * Setter method for lineitemNormalizationfactor.
     *
     * @param aLineitemNormalizationfactor the new value for lineitemNormalizationfactor
     */
    public void setLineitemNormalizationfactor(BigDecimal aLineitemNormalizationfactor) {
        lineitemNormalizationfactor = aLineitemNormalizationfactor;
    }

    /**
     * Access method for lineitemNormalizedusageamount.
     *
     * @return the current value of lineitemNormalizedusageamount
     */
    public BigDecimal getLineitemNormalizedusageamount() {
        return lineitemNormalizedusageamount;
    }

    /**
     * Setter method for lineitemNormalizedusageamount.
     *
     * @param aLineitemNormalizedusageamount the new value for lineitemNormalizedusageamount
     */
    public void setLineitemNormalizedusageamount(BigDecimal aLineitemNormalizedusageamount) {
        lineitemNormalizedusageamount = aLineitemNormalizedusageamount;
    }

    /**
     * Access method for lineitemCurrencycode.
     *
     * @return the current value of lineitemCurrencycode
     */
    public String getLineitemCurrencycode() {
        return lineitemCurrencycode;
    }

    /**
     * Setter method for lineitemCurrencycode.
     *
     * @param aLineitemCurrencycode the new value for lineitemCurrencycode
     */
    public void setLineitemCurrencycode(String aLineitemCurrencycode) {
        lineitemCurrencycode = aLineitemCurrencycode;
    }

    /**
     * Access method for lineitemUnblendedrate.
     *
     * @return the current value of lineitemUnblendedrate
     */
    public BigDecimal getLineitemUnblendedrate() {
        return lineitemUnblendedrate;
    }

    /**
     * Setter method for lineitemUnblendedrate.
     *
     * @param aLineitemUnblendedrate the new value for lineitemUnblendedrate
     */
    public void setLineitemUnblendedrate(BigDecimal aLineitemUnblendedrate) {
        lineitemUnblendedrate = aLineitemUnblendedrate;
    }

    /**
     * Access method for lineitemUnblendedcost.
     *
     * @return the current value of lineitemUnblendedcost
     */
    public BigDecimal getLineitemUnblendedcost() {
        return lineitemUnblendedcost;
    }

    /**
     * Setter method for lineitemUnblendedcost.
     *
     * @param aLineitemUnblendedcost the new value for lineitemUnblendedcost
     */
    public void setLineitemUnblendedcost(BigDecimal aLineitemUnblendedcost) {
        lineitemUnblendedcost = aLineitemUnblendedcost;
    }

    /**
     * Access method for lineitemBlendedrate.
     *
     * @return the current value of lineitemBlendedrate
     */
    public BigDecimal getLineitemBlendedrate() {
        return lineitemBlendedrate;
    }

    /**
     * Setter method for lineitemBlendedrate.
     *
     * @param aLineitemBlendedrate the new value for lineitemBlendedrate
     */
    public void setLineitemBlendedrate(BigDecimal aLineitemBlendedrate) {
        lineitemBlendedrate = aLineitemBlendedrate;
    }

    /**
     * Access method for lineitemBlendedcost.
     *
     * @return the current value of lineitemBlendedcost
     */
    public BigDecimal getLineitemBlendedcost() {
        return lineitemBlendedcost;
    }

    /**
     * Setter method for lineitemBlendedcost.
     *
     * @param aLineitemBlendedcost the new value for lineitemBlendedcost
     */
    public void setLineitemBlendedcost(BigDecimal aLineitemBlendedcost) {
        lineitemBlendedcost = aLineitemBlendedcost;
    }

    /**
     * Access method for lineitemTaxtype.
     *
     * @return the current value of lineitemTaxtype
     */
    public String getLineitemTaxtype() {
        return lineitemTaxtype;
    }

    /**
     * Setter method for lineitemTaxtype.
     *
     * @param aLineitemTaxtype the new value for lineitemTaxtype
     */
    public void setLineitemTaxtype(String aLineitemTaxtype) {
        lineitemTaxtype = aLineitemTaxtype;
    }

    /**
     * Access method for productProductname.
     *
     * @return the current value of productProductname
     */
    public String getProductProductname() {
        return productProductname;
    }

    /**
     * Setter method for productProductname.
     *
     * @param aProductProductname the new value for productProductname
     */
    public void setProductProductname(String aProductProductname) {
        productProductname = aProductProductname;
    }

    /**
     * Access method for productAvailability.
     *
     * @return the current value of productAvailability
     */
    public String getProductAvailability() {
        return productAvailability;
    }

    /**
     * Setter method for productAvailability.
     *
     * @param aProductAvailability the new value for productAvailability
     */
    public void setProductAvailability(String aProductAvailability) {
        productAvailability = aProductAvailability;
    }

    /**
     * Access method for productInstancetype.
     *
     * @return the current value of productInstancetype
     */
    public String getProductInstancetype() {
        return productInstancetype;
    }

    /**
     * Setter method for productInstancetype.
     *
     * @param aProductInstancetype the new value for productInstancetype
     */
    public void setProductInstancetype(String aProductInstancetype) {
        productInstancetype = aProductInstancetype;
    }

    /**
     * Access method for productProductfamily.
     *
     * @return the current value of productProductfamily
     */
    public String getProductProductfamily() {
        return productProductfamily;
    }

    /**
     * Setter method for productProductfamily.
     *
     * @param aProductProductfamily the new value for productProductfamily
     */
    public void setProductProductfamily(String aProductProductfamily) {
        productProductfamily = aProductProductfamily;
    }

    /**
     * Access method for productRegion.
     *
     * @return the current value of productRegion
     */
    public String getProductRegion() {
        return productRegion;
    }

    /**
     * Setter method for productRegion.
     *
     * @param aProductRegion the new value for productRegion
     */
    public void setProductRegion(String aProductRegion) {
        productRegion = aProductRegion;
    }

    /**
     * Access method for pricingPublicondemandcost.
     *
     * @return the current value of pricingPublicondemandcost
     */
    public BigDecimal getPricingPublicondemandcost() {
        return pricingPublicondemandcost;
    }

    /**
     * Setter method for pricingPublicondemandcost.
     *
     * @param aPricingPublicondemandcost the new value for pricingPublicondemandcost
     */
    public void setPricingPublicondemandcost(BigDecimal aPricingPublicondemandcost) {
        pricingPublicondemandcost = aPricingPublicondemandcost;
    }

    /**
     * Access method for pricingPublicondemandrate.
     *
     * @return the current value of pricingPublicondemandrate
     */
    public BigDecimal getPricingPublicondemandrate() {
        return pricingPublicondemandrate;
    }

    /**
     * Setter method for pricingPublicondemandrate.
     *
     * @param aPricingPublicondemandrate the new value for pricingPublicondemandrate
     */
    public void setPricingPublicondemandrate(BigDecimal aPricingPublicondemandrate) {
        pricingPublicondemandrate = aPricingPublicondemandrate;
    }

    /**
     * Access method for pricingUnit.
     *
     * @return the current value of pricingUnit
     */
    public String getPricingUnit() {
        return pricingUnit;
    }

    /**
     * Setter method for pricingUnit.
     *
     * @param aPricingUnit the new value for pricingUnit
     */
    public void setPricingUnit(String aPricingUnit) {
        pricingUnit = aPricingUnit;
    }

    /**
     * Access method for month.
     *
     * @return the current value of month
     */
    public int getMonth() {
        return month;
    }

    /**
     * Setter method for month.
     *
     * @param aMonth the new value for month
     */
    public void setMonth(int aMonth) {
        month = aMonth;
    }

    /**
     * Access method for year.
     *
     * @return the current value of year
     */
    public int getYear() {
        return year;
    }

    /**
     * Setter method for year.
     *
     * @param aYear the new value for year
     */
    public void setYear(int aYear) {
        year = aYear;
    }

    /**
     * Access method for totalCost.
     *
     * @return the current value of totalCost
     */
    public BigDecimal getTotalCost() {
        return totalCost;
    }

    /**
     * Setter method for totalCost.
     *
     * @param aTotalCost the new value for totalCost
     */
    public void setTotalCost(BigDecimal aTotalCost) {
        totalCost = aTotalCost;
    }

    /**
     * Access method for tags.
     *
     * @return the current value of tags
     */
    public String getTags() {
        return tags;
    }

    /**
     * Setter method for tags.
     *
     * @param aTags the new value for tags
     */
    public void setTags(String aTags) {
        tags = aTags;
    }

}
