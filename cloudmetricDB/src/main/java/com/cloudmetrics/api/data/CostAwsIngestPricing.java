// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="cost_aws_ingest_pricing", indexes={@Index(name="costAwsIngestPricingCostAwsIngestPricingTermUnitC3978af3Uniq", columnList="term,unit,rateid", unique=true)})
public class CostAwsIngestPricing implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(length=63)
    private String term;
    @Column(length=63)
    private String unit;
    @Column(precision=10)
    private int rateid;
    @OneToMany(mappedBy="costAwsIngestPricing")
    private Set<CostAwsIngestActuallineitem> costAwsIngestActuallineitem;
    @OneToMany(mappedBy="costAwsIngestPricing")
    private Set<CostAwsIngestEstimatelineitem> costAwsIngestEstimatelineitem;
    @OneToMany(mappedBy="costAwsIngestPricing")
    private Set<CostAwsIngestEstimatelineitemTest> costAwsIngestEstimatelineitemTest;
    @OneToMany(mappedBy="costAwsIngestPricing")
    private Set<CostAwsIngestActuallineitemTest> costAwsIngestActuallineitemTest;

    /** Default constructor. */
    public CostAwsIngestPricing() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for term.
     *
     * @return the current value of term
     */
    public String getTerm() {
        return term;
    }

    /**
     * Setter method for term.
     *
     * @param aTerm the new value for term
     */
    public void setTerm(String aTerm) {
        term = aTerm;
    }

    /**
     * Access method for unit.
     *
     * @return the current value of unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Setter method for unit.
     *
     * @param aUnit the new value for unit
     */
    public void setUnit(String aUnit) {
        unit = aUnit;
    }

    /**
     * Access method for rateid.
     *
     * @return the current value of rateid
     */
    public int getRateid() {
        return rateid;
    }

    /**
     * Setter method for rateid.
     *
     * @param aRateid the new value for rateid
     */
    public void setRateid(int aRateid) {
        rateid = aRateid;
    }

    /**
     * Access method for costAwsIngestActuallineitem.
     *
     * @return the current value of costAwsIngestActuallineitem
     */
    public Set<CostAwsIngestActuallineitem> getCostAwsIngestActuallineitem() {
        return costAwsIngestActuallineitem;
    }

    /**
     * Setter method for costAwsIngestActuallineitem.
     *
     * @param aCostAwsIngestActuallineitem the new value for costAwsIngestActuallineitem
     */
    public void setCostAwsIngestActuallineitem(Set<CostAwsIngestActuallineitem> aCostAwsIngestActuallineitem) {
        costAwsIngestActuallineitem = aCostAwsIngestActuallineitem;
    }

    /**
     * Access method for costAwsIngestEstimatelineitem.
     *
     * @return the current value of costAwsIngestEstimatelineitem
     */
    public Set<CostAwsIngestEstimatelineitem> getCostAwsIngestEstimatelineitem() {
        return costAwsIngestEstimatelineitem;
    }

    /**
     * Setter method for costAwsIngestEstimatelineitem.
     *
     * @param aCostAwsIngestEstimatelineitem the new value for costAwsIngestEstimatelineitem
     */
    public void setCostAwsIngestEstimatelineitem(Set<CostAwsIngestEstimatelineitem> aCostAwsIngestEstimatelineitem) {
        costAwsIngestEstimatelineitem = aCostAwsIngestEstimatelineitem;
    }

    /**
     * Access method for costAwsIngestEstimatelineitemTest.
     *
     * @return the current value of costAwsIngestEstimatelineitemTest
     */
    public Set<CostAwsIngestEstimatelineitemTest> getCostAwsIngestEstimatelineitemTest() {
        return costAwsIngestEstimatelineitemTest;
    }

    /**
     * Setter method for costAwsIngestEstimatelineitemTest.
     *
     * @param aCostAwsIngestEstimatelineitemTest the new value for costAwsIngestEstimatelineitemTest
     */
    public void setCostAwsIngestEstimatelineitemTest(Set<CostAwsIngestEstimatelineitemTest> aCostAwsIngestEstimatelineitemTest) {
        costAwsIngestEstimatelineitemTest = aCostAwsIngestEstimatelineitemTest;
    }

    /**
     * Access method for costAwsIngestActuallineitemTest.
     *
     * @return the current value of costAwsIngestActuallineitemTest
     */
    public Set<CostAwsIngestActuallineitemTest> getCostAwsIngestActuallineitemTest() {
        return costAwsIngestActuallineitemTest;
    }

    /**
     * Setter method for costAwsIngestActuallineitemTest.
     *
     * @param aCostAwsIngestActuallineitemTest the new value for costAwsIngestActuallineitemTest
     */
    public void setCostAwsIngestActuallineitemTest(Set<CostAwsIngestActuallineitemTest> aCostAwsIngestActuallineitemTest) {
        costAwsIngestActuallineitemTest = aCostAwsIngestActuallineitemTest;
    }

    /**
     * Compares the key for this instance with another CostAwsIngestPricing.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class CostAwsIngestPricing and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof CostAwsIngestPricing)) {
            return false;
        }
        CostAwsIngestPricing that = (CostAwsIngestPricing) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another CostAwsIngestPricing.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CostAwsIngestPricing)) return false;
        return this.equalKeys(other) && ((CostAwsIngestPricing)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[CostAwsIngestPricing |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
