// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Version;

@Entity(name="total")
public class Total implements Serializable {

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Column(precision=19)
    private long count;

    /** Default constructor. */
    public Total() {
        super();
    }

    /**
     * Access method for count.
     *
     * @return the current value of count
     */
    public long getCount() {
        return count;
    }

    /**
     * Setter method for count.
     *
     * @param aCount the new value for count
     */
    public void setCount(long aCount) {
        count = aCount;
    }

}
