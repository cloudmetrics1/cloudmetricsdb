// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity
public class AspNetUserClaims implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="Id", unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="ClaimType")
    private String claimType;
    @Column(name="ClaimValue")
    private String claimValue;
    @ManyToOne(optional=false)
    @JoinColumn(name="UserId", nullable=false)
    private AspNetUsers aspNetUsers;

    /** Default constructor. */
    public AspNetUserClaims() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for claimType.
     *
     * @return the current value of claimType
     */
    public String getClaimType() {
        return claimType;
    }

    /**
     * Setter method for claimType.
     *
     * @param aClaimType the new value for claimType
     */
    public void setClaimType(String aClaimType) {
        claimType = aClaimType;
    }

    /**
     * Access method for claimValue.
     *
     * @return the current value of claimValue
     */
    public String getClaimValue() {
        return claimValue;
    }

    /**
     * Setter method for claimValue.
     *
     * @param aClaimValue the new value for claimValue
     */
    public void setClaimValue(String aClaimValue) {
        claimValue = aClaimValue;
    }

    /**
     * Access method for aspNetUsers.
     *
     * @return the current value of aspNetUsers
     */
    public AspNetUsers getAspNetUsers() {
        return aspNetUsers;
    }

    /**
     * Setter method for aspNetUsers.
     *
     * @param aAspNetUsers the new value for aspNetUsers
     */
    public void setAspNetUsers(AspNetUsers aAspNetUsers) {
        aspNetUsers = aAspNetUsers;
    }

    /**
     * Compares the key for this instance with another AspNetUserClaims.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class AspNetUserClaims and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof AspNetUserClaims)) {
            return false;
        }
        AspNetUserClaims that = (AspNetUserClaims) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another AspNetUserClaims.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof AspNetUserClaims)) return false;
        return this.equalKeys(other) && ((AspNetUserClaims)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[AspNetUserClaims |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
