// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity(name="rprt_aws_cost_actualsummary_test")
public class RprtAwsCostActualsummaryTest implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=19)
    private long id;
    @Column(name="usage_start", nullable=false)
    private Timestamp usageStart;
    @Column(name="usage_end")
    private Timestamp usageEnd;
    @Column(name="usage_account_id", nullable=false, length=50)
    private String usageAccountId;
    @Column(name="product_code", nullable=false, length=50)
    private String productCode;
    @Column(name="product_family", length=150)
    private String productFamily;
    @Column(name="availability_zone", length=50)
    private String availabilityZone;
    @Column(length=50)
    private String region;
    @Column(name="instance_type", length=50)
    private String instanceType;
    @Column(length=63)
    private String unit;
    @Column(name="resource_count", precision=10)
    private int resourceCount;
    @Column(name="usage_amount", precision=24, scale=9)
    private BigDecimal usageAmount;
    @Column(name="normalization_factor", precision=17, scale=17)
    private double normalizationFactor;
    @Column(name="normalized_usage_amount", precision=17, scale=17)
    private double normalizedUsageAmount;
    @Column(name="currency_code", nullable=false, length=10)
    private String currencyCode;
    @Column(name="unblended_rate", precision=17, scale=9)
    private BigDecimal unblendedRate;
    @Column(name="unblended_cost", precision=17, scale=9)
    private BigDecimal unblendedCost;
    @Column(name="blended_rate", precision=17, scale=9)
    private BigDecimal blendedRate;
    @Column(name="blended_cost", precision=17, scale=9)
    private BigDecimal blendedCost;
    @Column(name="public_on_demand_cost", precision=17, scale=9)
    private BigDecimal publicOnDemandCost;
    @Column(name="public_on_demand_rate", precision=17, scale=9)
    private BigDecimal publicOnDemandRate;
    @Column(name="tax_type")
    private String taxType;
    @Column(name="account_alias_id", precision=10)
    private int accountAliasId;
    private String tags;
    @Column(name="resource_id")
    private String resourceId;
    @Column(name="markup_cost", precision=17, scale=9)
    private BigDecimal markupCost;
    @Column(name="reporting_month", precision=10)
    private int reportingMonth;
    @Column(name="reporting_year", precision=10)
    private int reportingYear;
    @ManyToOne
    @JoinColumn(name="cost_aws_ingest_curdata_id")
    private CostAwsIngestCurdata costAwsIngestCurdata;

    /** Default constructor. */
    public RprtAwsCostActualsummaryTest() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(long aId) {
        id = aId;
    }

    /**
     * Access method for usageStart.
     *
     * @return the current value of usageStart
     */
    public Timestamp getUsageStart() {
        return usageStart;
    }

    /**
     * Setter method for usageStart.
     *
     * @param aUsageStart the new value for usageStart
     */
    public void setUsageStart(Timestamp aUsageStart) {
        usageStart = aUsageStart;
    }

    /**
     * Access method for usageEnd.
     *
     * @return the current value of usageEnd
     */
    public Timestamp getUsageEnd() {
        return usageEnd;
    }

    /**
     * Setter method for usageEnd.
     *
     * @param aUsageEnd the new value for usageEnd
     */
    public void setUsageEnd(Timestamp aUsageEnd) {
        usageEnd = aUsageEnd;
    }

    /**
     * Access method for usageAccountId.
     *
     * @return the current value of usageAccountId
     */
    public String getUsageAccountId() {
        return usageAccountId;
    }

    /**
     * Setter method for usageAccountId.
     *
     * @param aUsageAccountId the new value for usageAccountId
     */
    public void setUsageAccountId(String aUsageAccountId) {
        usageAccountId = aUsageAccountId;
    }

    /**
     * Access method for productCode.
     *
     * @return the current value of productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Setter method for productCode.
     *
     * @param aProductCode the new value for productCode
     */
    public void setProductCode(String aProductCode) {
        productCode = aProductCode;
    }

    /**
     * Access method for productFamily.
     *
     * @return the current value of productFamily
     */
    public String getProductFamily() {
        return productFamily;
    }

    /**
     * Setter method for productFamily.
     *
     * @param aProductFamily the new value for productFamily
     */
    public void setProductFamily(String aProductFamily) {
        productFamily = aProductFamily;
    }

    /**
     * Access method for availabilityZone.
     *
     * @return the current value of availabilityZone
     */
    public String getAvailabilityZone() {
        return availabilityZone;
    }

    /**
     * Setter method for availabilityZone.
     *
     * @param aAvailabilityZone the new value for availabilityZone
     */
    public void setAvailabilityZone(String aAvailabilityZone) {
        availabilityZone = aAvailabilityZone;
    }

    /**
     * Access method for region.
     *
     * @return the current value of region
     */
    public String getRegion() {
        return region;
    }

    /**
     * Setter method for region.
     *
     * @param aRegion the new value for region
     */
    public void setRegion(String aRegion) {
        region = aRegion;
    }

    /**
     * Access method for instanceType.
     *
     * @return the current value of instanceType
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Setter method for instanceType.
     *
     * @param aInstanceType the new value for instanceType
     */
    public void setInstanceType(String aInstanceType) {
        instanceType = aInstanceType;
    }

    /**
     * Access method for unit.
     *
     * @return the current value of unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Setter method for unit.
     *
     * @param aUnit the new value for unit
     */
    public void setUnit(String aUnit) {
        unit = aUnit;
    }

    /**
     * Access method for resourceCount.
     *
     * @return the current value of resourceCount
     */
    public int getResourceCount() {
        return resourceCount;
    }

    /**
     * Setter method for resourceCount.
     *
     * @param aResourceCount the new value for resourceCount
     */
    public void setResourceCount(int aResourceCount) {
        resourceCount = aResourceCount;
    }

    /**
     * Access method for usageAmount.
     *
     * @return the current value of usageAmount
     */
    public BigDecimal getUsageAmount() {
        return usageAmount;
    }

    /**
     * Setter method for usageAmount.
     *
     * @param aUsageAmount the new value for usageAmount
     */
    public void setUsageAmount(BigDecimal aUsageAmount) {
        usageAmount = aUsageAmount;
    }

    /**
     * Access method for normalizationFactor.
     *
     * @return the current value of normalizationFactor
     */
    public double getNormalizationFactor() {
        return normalizationFactor;
    }

    /**
     * Setter method for normalizationFactor.
     *
     * @param aNormalizationFactor the new value for normalizationFactor
     */
    public void setNormalizationFactor(double aNormalizationFactor) {
        normalizationFactor = aNormalizationFactor;
    }

    /**
     * Access method for normalizedUsageAmount.
     *
     * @return the current value of normalizedUsageAmount
     */
    public double getNormalizedUsageAmount() {
        return normalizedUsageAmount;
    }

    /**
     * Setter method for normalizedUsageAmount.
     *
     * @param aNormalizedUsageAmount the new value for normalizedUsageAmount
     */
    public void setNormalizedUsageAmount(double aNormalizedUsageAmount) {
        normalizedUsageAmount = aNormalizedUsageAmount;
    }

    /**
     * Access method for currencyCode.
     *
     * @return the current value of currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Setter method for currencyCode.
     *
     * @param aCurrencyCode the new value for currencyCode
     */
    public void setCurrencyCode(String aCurrencyCode) {
        currencyCode = aCurrencyCode;
    }

    /**
     * Access method for unblendedRate.
     *
     * @return the current value of unblendedRate
     */
    public BigDecimal getUnblendedRate() {
        return unblendedRate;
    }

    /**
     * Setter method for unblendedRate.
     *
     * @param aUnblendedRate the new value for unblendedRate
     */
    public void setUnblendedRate(BigDecimal aUnblendedRate) {
        unblendedRate = aUnblendedRate;
    }

    /**
     * Access method for unblendedCost.
     *
     * @return the current value of unblendedCost
     */
    public BigDecimal getUnblendedCost() {
        return unblendedCost;
    }

    /**
     * Setter method for unblendedCost.
     *
     * @param aUnblendedCost the new value for unblendedCost
     */
    public void setUnblendedCost(BigDecimal aUnblendedCost) {
        unblendedCost = aUnblendedCost;
    }

    /**
     * Access method for blendedRate.
     *
     * @return the current value of blendedRate
     */
    public BigDecimal getBlendedRate() {
        return blendedRate;
    }

    /**
     * Setter method for blendedRate.
     *
     * @param aBlendedRate the new value for blendedRate
     */
    public void setBlendedRate(BigDecimal aBlendedRate) {
        blendedRate = aBlendedRate;
    }

    /**
     * Access method for blendedCost.
     *
     * @return the current value of blendedCost
     */
    public BigDecimal getBlendedCost() {
        return blendedCost;
    }

    /**
     * Setter method for blendedCost.
     *
     * @param aBlendedCost the new value for blendedCost
     */
    public void setBlendedCost(BigDecimal aBlendedCost) {
        blendedCost = aBlendedCost;
    }

    /**
     * Access method for publicOnDemandCost.
     *
     * @return the current value of publicOnDemandCost
     */
    public BigDecimal getPublicOnDemandCost() {
        return publicOnDemandCost;
    }

    /**
     * Setter method for publicOnDemandCost.
     *
     * @param aPublicOnDemandCost the new value for publicOnDemandCost
     */
    public void setPublicOnDemandCost(BigDecimal aPublicOnDemandCost) {
        publicOnDemandCost = aPublicOnDemandCost;
    }

    /**
     * Access method for publicOnDemandRate.
     *
     * @return the current value of publicOnDemandRate
     */
    public BigDecimal getPublicOnDemandRate() {
        return publicOnDemandRate;
    }

    /**
     * Setter method for publicOnDemandRate.
     *
     * @param aPublicOnDemandRate the new value for publicOnDemandRate
     */
    public void setPublicOnDemandRate(BigDecimal aPublicOnDemandRate) {
        publicOnDemandRate = aPublicOnDemandRate;
    }

    /**
     * Access method for taxType.
     *
     * @return the current value of taxType
     */
    public String getTaxType() {
        return taxType;
    }

    /**
     * Setter method for taxType.
     *
     * @param aTaxType the new value for taxType
     */
    public void setTaxType(String aTaxType) {
        taxType = aTaxType;
    }

    /**
     * Access method for accountAliasId.
     *
     * @return the current value of accountAliasId
     */
    public int getAccountAliasId() {
        return accountAliasId;
    }

    /**
     * Setter method for accountAliasId.
     *
     * @param aAccountAliasId the new value for accountAliasId
     */
    public void setAccountAliasId(int aAccountAliasId) {
        accountAliasId = aAccountAliasId;
    }

    /**
     * Access method for tags.
     *
     * @return the current value of tags
     */
    public String getTags() {
        return tags;
    }

    /**
     * Setter method for tags.
     *
     * @param aTags the new value for tags
     */
    public void setTags(String aTags) {
        tags = aTags;
    }

    /**
     * Access method for resourceId.
     *
     * @return the current value of resourceId
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Setter method for resourceId.
     *
     * @param aResourceId the new value for resourceId
     */
    public void setResourceId(String aResourceId) {
        resourceId = aResourceId;
    }

    /**
     * Access method for markupCost.
     *
     * @return the current value of markupCost
     */
    public BigDecimal getMarkupCost() {
        return markupCost;
    }

    /**
     * Setter method for markupCost.
     *
     * @param aMarkupCost the new value for markupCost
     */
    public void setMarkupCost(BigDecimal aMarkupCost) {
        markupCost = aMarkupCost;
    }

    /**
     * Access method for reportingMonth.
     *
     * @return the current value of reportingMonth
     */
    public int getReportingMonth() {
        return reportingMonth;
    }

    /**
     * Setter method for reportingMonth.
     *
     * @param aReportingMonth the new value for reportingMonth
     */
    public void setReportingMonth(int aReportingMonth) {
        reportingMonth = aReportingMonth;
    }

    /**
     * Access method for reportingYear.
     *
     * @return the current value of reportingYear
     */
    public int getReportingYear() {
        return reportingYear;
    }

    /**
     * Setter method for reportingYear.
     *
     * @param aReportingYear the new value for reportingYear
     */
    public void setReportingYear(int aReportingYear) {
        reportingYear = aReportingYear;
    }

    /**
     * Access method for costAwsIngestCurdata.
     *
     * @return the current value of costAwsIngestCurdata
     */
    public CostAwsIngestCurdata getCostAwsIngestCurdata() {
        return costAwsIngestCurdata;
    }

    /**
     * Setter method for costAwsIngestCurdata.
     *
     * @param aCostAwsIngestCurdata the new value for costAwsIngestCurdata
     */
    public void setCostAwsIngestCurdata(CostAwsIngestCurdata aCostAwsIngestCurdata) {
        costAwsIngestCurdata = aCostAwsIngestCurdata;
    }

    /**
     * Compares the key for this instance with another RprtAwsCostActualsummaryTest.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class RprtAwsCostActualsummaryTest and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof RprtAwsCostActualsummaryTest)) {
            return false;
        }
        RprtAwsCostActualsummaryTest that = (RprtAwsCostActualsummaryTest) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another RprtAwsCostActualsummaryTest.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof RprtAwsCostActualsummaryTest)) return false;
        return this.equalKeys(other) && ((RprtAwsCostActualsummaryTest)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = (int)(getId() ^ (getId()>>>32));
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[RprtAwsCostActualsummaryTest |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Long.valueOf(getId()));
        return ret;
    }

}
