// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Version;

@Entity(name="ambar.rprt_aws_cost_actualsummary_test")
public class Ambar_rprtAwsCostActualsummaryTest implements Serializable {

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Column(name="usage_account_id", precision=19)
    private long usageAccountId;
    @Column(name="usage_start")
    private String usageStart;
    @Column(name="usage_end")
    private String usageEnd;
    @Column(name="product_code")
    private String productCode;
    @Column(name="product_family")
    private String productFamily;
    @Column(name="usage_type")
    private String usageType;
    @Column(name="availability_zone")
    private String availabilityZone;
    @Column(name="resource_id")
    private String resourceId;
    @Column(name="usage_amount", precision=17, scale=17)
    private double usageAmount;
    @Column(name="normalization_factor", precision=17, scale=17)
    private double normalizationFactor;
    @Column(name="normalized_usage_amount", precision=17, scale=17)
    private double normalizedUsageAmount;
    @Column(name="currency_code")
    private String currencyCode;
    @Column(name="unblended_rate", precision=17, scale=17)
    private double unblendedRate;
    @Column(name="unblended_cost", precision=17, scale=17)
    private double unblendedCost;
    @Column(name="blended_rate", precision=17, scale=17)
    private double blendedRate;
    @Column(name="blended_cost", precision=17, scale=17)
    private double blendedCost;
    @Column(name="public_on_demand_cost", precision=17, scale=17)
    private double publicOnDemandCost;
    @Column(name="public_on_demand_rate", precision=17, scale=17)
    private double publicOnDemandRate;
    @Column(name="tax_type")
    private String taxType;
    @Column(name="reporting_month")
    private String reportingMonth;
    @Column(name="reporting_year")
    private String reportingYear;

    /** Default constructor. */
    public Ambar_rprtAwsCostActualsummaryTest() {
        super();
    }

    /**
     * Access method for usageAccountId.
     *
     * @return the current value of usageAccountId
     */
    public long getUsageAccountId() {
        return usageAccountId;
    }

    /**
     * Setter method for usageAccountId.
     *
     * @param aUsageAccountId the new value for usageAccountId
     */
    public void setUsageAccountId(long aUsageAccountId) {
        usageAccountId = aUsageAccountId;
    }

    /**
     * Access method for usageStart.
     *
     * @return the current value of usageStart
     */
    public String getUsageStart() {
        return usageStart;
    }

    /**
     * Setter method for usageStart.
     *
     * @param aUsageStart the new value for usageStart
     */
    public void setUsageStart(String aUsageStart) {
        usageStart = aUsageStart;
    }

    /**
     * Access method for usageEnd.
     *
     * @return the current value of usageEnd
     */
    public String getUsageEnd() {
        return usageEnd;
    }

    /**
     * Setter method for usageEnd.
     *
     * @param aUsageEnd the new value for usageEnd
     */
    public void setUsageEnd(String aUsageEnd) {
        usageEnd = aUsageEnd;
    }

    /**
     * Access method for productCode.
     *
     * @return the current value of productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Setter method for productCode.
     *
     * @param aProductCode the new value for productCode
     */
    public void setProductCode(String aProductCode) {
        productCode = aProductCode;
    }

    /**
     * Access method for productFamily.
     *
     * @return the current value of productFamily
     */
    public String getProductFamily() {
        return productFamily;
    }

    /**
     * Setter method for productFamily.
     *
     * @param aProductFamily the new value for productFamily
     */
    public void setProductFamily(String aProductFamily) {
        productFamily = aProductFamily;
    }

    /**
     * Access method for usageType.
     *
     * @return the current value of usageType
     */
    public String getUsageType() {
        return usageType;
    }

    /**
     * Setter method for usageType.
     *
     * @param aUsageType the new value for usageType
     */
    public void setUsageType(String aUsageType) {
        usageType = aUsageType;
    }

    /**
     * Access method for availabilityZone.
     *
     * @return the current value of availabilityZone
     */
    public String getAvailabilityZone() {
        return availabilityZone;
    }

    /**
     * Setter method for availabilityZone.
     *
     * @param aAvailabilityZone the new value for availabilityZone
     */
    public void setAvailabilityZone(String aAvailabilityZone) {
        availabilityZone = aAvailabilityZone;
    }

    /**
     * Access method for resourceId.
     *
     * @return the current value of resourceId
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Setter method for resourceId.
     *
     * @param aResourceId the new value for resourceId
     */
    public void setResourceId(String aResourceId) {
        resourceId = aResourceId;
    }

    /**
     * Access method for usageAmount.
     *
     * @return the current value of usageAmount
     */
    public double getUsageAmount() {
        return usageAmount;
    }

    /**
     * Setter method for usageAmount.
     *
     * @param aUsageAmount the new value for usageAmount
     */
    public void setUsageAmount(double aUsageAmount) {
        usageAmount = aUsageAmount;
    }

    /**
     * Access method for normalizationFactor.
     *
     * @return the current value of normalizationFactor
     */
    public double getNormalizationFactor() {
        return normalizationFactor;
    }

    /**
     * Setter method for normalizationFactor.
     *
     * @param aNormalizationFactor the new value for normalizationFactor
     */
    public void setNormalizationFactor(double aNormalizationFactor) {
        normalizationFactor = aNormalizationFactor;
    }

    /**
     * Access method for normalizedUsageAmount.
     *
     * @return the current value of normalizedUsageAmount
     */
    public double getNormalizedUsageAmount() {
        return normalizedUsageAmount;
    }

    /**
     * Setter method for normalizedUsageAmount.
     *
     * @param aNormalizedUsageAmount the new value for normalizedUsageAmount
     */
    public void setNormalizedUsageAmount(double aNormalizedUsageAmount) {
        normalizedUsageAmount = aNormalizedUsageAmount;
    }

    /**
     * Access method for currencyCode.
     *
     * @return the current value of currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Setter method for currencyCode.
     *
     * @param aCurrencyCode the new value for currencyCode
     */
    public void setCurrencyCode(String aCurrencyCode) {
        currencyCode = aCurrencyCode;
    }

    /**
     * Access method for unblendedRate.
     *
     * @return the current value of unblendedRate
     */
    public double getUnblendedRate() {
        return unblendedRate;
    }

    /**
     * Setter method for unblendedRate.
     *
     * @param aUnblendedRate the new value for unblendedRate
     */
    public void setUnblendedRate(double aUnblendedRate) {
        unblendedRate = aUnblendedRate;
    }

    /**
     * Access method for unblendedCost.
     *
     * @return the current value of unblendedCost
     */
    public double getUnblendedCost() {
        return unblendedCost;
    }

    /**
     * Setter method for unblendedCost.
     *
     * @param aUnblendedCost the new value for unblendedCost
     */
    public void setUnblendedCost(double aUnblendedCost) {
        unblendedCost = aUnblendedCost;
    }

    /**
     * Access method for blendedRate.
     *
     * @return the current value of blendedRate
     */
    public double getBlendedRate() {
        return blendedRate;
    }

    /**
     * Setter method for blendedRate.
     *
     * @param aBlendedRate the new value for blendedRate
     */
    public void setBlendedRate(double aBlendedRate) {
        blendedRate = aBlendedRate;
    }

    /**
     * Access method for blendedCost.
     *
     * @return the current value of blendedCost
     */
    public double getBlendedCost() {
        return blendedCost;
    }

    /**
     * Setter method for blendedCost.
     *
     * @param aBlendedCost the new value for blendedCost
     */
    public void setBlendedCost(double aBlendedCost) {
        blendedCost = aBlendedCost;
    }

    /**
     * Access method for publicOnDemandCost.
     *
     * @return the current value of publicOnDemandCost
     */
    public double getPublicOnDemandCost() {
        return publicOnDemandCost;
    }

    /**
     * Setter method for publicOnDemandCost.
     *
     * @param aPublicOnDemandCost the new value for publicOnDemandCost
     */
    public void setPublicOnDemandCost(double aPublicOnDemandCost) {
        publicOnDemandCost = aPublicOnDemandCost;
    }

    /**
     * Access method for publicOnDemandRate.
     *
     * @return the current value of publicOnDemandRate
     */
    public double getPublicOnDemandRate() {
        return publicOnDemandRate;
    }

    /**
     * Setter method for publicOnDemandRate.
     *
     * @param aPublicOnDemandRate the new value for publicOnDemandRate
     */
    public void setPublicOnDemandRate(double aPublicOnDemandRate) {
        publicOnDemandRate = aPublicOnDemandRate;
    }

    /**
     * Access method for taxType.
     *
     * @return the current value of taxType
     */
    public String getTaxType() {
        return taxType;
    }

    /**
     * Setter method for taxType.
     *
     * @param aTaxType the new value for taxType
     */
    public void setTaxType(String aTaxType) {
        taxType = aTaxType;
    }

    /**
     * Access method for reportingMonth.
     *
     * @return the current value of reportingMonth
     */
    public String getReportingMonth() {
        return reportingMonth;
    }

    /**
     * Setter method for reportingMonth.
     *
     * @param aReportingMonth the new value for reportingMonth
     */
    public void setReportingMonth(String aReportingMonth) {
        reportingMonth = aReportingMonth;
    }

    /**
     * Access method for reportingYear.
     *
     * @return the current value of reportingYear
     */
    public String getReportingYear() {
        return reportingYear;
    }

    /**
     * Setter method for reportingYear.
     *
     * @param aReportingYear the new value for reportingYear
     */
    public void setReportingYear(String aReportingYear) {
        reportingYear = aReportingYear;
    }

}
