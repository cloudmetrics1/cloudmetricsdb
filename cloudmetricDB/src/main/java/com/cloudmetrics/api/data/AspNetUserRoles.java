// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity
public class AspNetUserRoles implements Serializable {

    /** Primary key. */
    protected static final String PK = "AspNetUserRolesAspNetUserRolesPkey";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @ManyToOne(optional=false)
    @JoinColumn(name="RoleId", nullable=false)
    private AspNetRoles aspNetRoles;
    @ManyToOne(optional=false)
    @JoinColumn(name="UserId", nullable=false)
    private AspNetUsers aspNetUsers;

    /** Default constructor. */
    public AspNetUserRoles() {
        super();
    }

    /**
     * Access method for aspNetRoles.
     *
     * @return the current value of aspNetRoles
     */
    public AspNetRoles getAspNetRoles() {
        return aspNetRoles;
    }

    /**
     * Setter method for aspNetRoles.
     *
     * @param aAspNetRoles the new value for aspNetRoles
     */
    public void setAspNetRoles(AspNetRoles aAspNetRoles) {
        aspNetRoles = aAspNetRoles;
    }

    /**
     * Access method for aspNetUsers.
     *
     * @return the current value of aspNetUsers
     */
    public AspNetUsers getAspNetUsers() {
        return aspNetUsers;
    }

    /**
     * Setter method for aspNetUsers.
     *
     * @param aAspNetUsers the new value for aspNetUsers
     */
    public void setAspNetUsers(AspNetUsers aAspNetUsers) {
        aspNetUsers = aAspNetUsers;
    }

    /** Temporary value holder for group key fragment aspNetUsersId */
    private transient String tempAspNetUsersId;

    /**
     * Gets the key fragment id for member aspNetUsers.
     * If this.aspNetUsers is null, a temporary stored value for the key
     * fragment will be returned. The temporary value is set by setAspNetUsersId.
     * This behavior is required by some persistence libraries to allow
     * fetching of objects in arbitrary order.
     *
     * @return Current (or temporary) value of the key fragment
     * @see AspNetUsers
     */
    public String getAspNetUsersId() {
        return (getAspNetUsers() == null ? tempAspNetUsersId : getAspNetUsers().getId());
    }

    /**
     * Sets the key fragment id from member aspNetUsers.
     * If this.aspNetUsers is null, the passed value will be temporary
     * stored, and returned by subsequent calls to getAspNetUsersId.
     * This behaviour is required by some persistence libraries to allow
     * fetching of objects in arbitrary order.
     *
     * @param aId New value for the key fragment
     * @see AspNetUsers
     */
    public void setAspNetUsersId(String aId) {
        if (getAspNetUsers() == null) {
            tempAspNetUsersId = aId;
        } else {
            getAspNetUsers().setId(aId);
        }
    }

    /** Temporary value holder for group key fragment aspNetRolesId */
    private transient String tempAspNetRolesId;

    /**
     * Gets the key fragment id for member aspNetRoles.
     * If this.aspNetRoles is null, a temporary stored value for the key
     * fragment will be returned. The temporary value is set by setAspNetRolesId.
     * This behavior is required by some persistence libraries to allow
     * fetching of objects in arbitrary order.
     *
     * @return Current (or temporary) value of the key fragment
     * @see AspNetRoles
     */
    public String getAspNetRolesId() {
        return (getAspNetRoles() == null ? tempAspNetRolesId : getAspNetRoles().getId());
    }

    /**
     * Sets the key fragment id from member aspNetRoles.
     * If this.aspNetRoles is null, the passed value will be temporary
     * stored, and returned by subsequent calls to getAspNetRolesId.
     * This behaviour is required by some persistence libraries to allow
     * fetching of objects in arbitrary order.
     *
     * @param aId New value for the key fragment
     * @see AspNetRoles
     */
    public void setAspNetRolesId(String aId) {
        if (getAspNetRoles() == null) {
            tempAspNetRolesId = aId;
        } else {
            getAspNetRoles().setId(aId);
        }
    }

    /**
     * Compares the key for this instance with another AspNetUserRoles.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class AspNetUserRoles and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof AspNetUserRoles)) {
            return false;
        }
        AspNetUserRoles that = (AspNetUserRoles) other;
        Object myAspNetUsersId = this.getAspNetUsersId();
        Object yourAspNetUsersId = that.getAspNetUsersId();
        if (myAspNetUsersId==null ? yourAspNetUsersId!=null : !myAspNetUsersId.equals(yourAspNetUsersId)) {
            return false;
        }
        Object myAspNetRolesId = this.getAspNetRolesId();
        Object yourAspNetRolesId = that.getAspNetRolesId();
        if (myAspNetRolesId==null ? yourAspNetRolesId!=null : !myAspNetRolesId.equals(yourAspNetRolesId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another AspNetUserRoles.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof AspNetUserRoles)) return false;
        return this.equalKeys(other) && ((AspNetUserRoles)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getAspNetUsersId() == null) {
            i = 0;
        } else {
            i = getAspNetUsersId().hashCode();
        }
        result = 37*result + i;
        if (getAspNetRolesId() == null) {
            i = 0;
        } else {
            i = getAspNetRolesId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[AspNetUserRoles |");
        sb.append(" aspNetUsersId=").append(getAspNetUsersId());
        sb.append(" aspNetRolesId=").append(getAspNetRolesId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("aspNetUsersId", getAspNetUsersId());
        ret.put("aspNetRolesId", getAspNetRolesId());
        return ret;
    }

}
