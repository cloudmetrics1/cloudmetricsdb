// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Version;

@Entity(name="aws_data_proc_stats_test")
public class AwsDataProcStatsTest implements Serializable {

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Column(unique=true, nullable=false, precision=10)
    private int rowid;
    private String account;
    private String bucket;
    private String filename;
    @Column(precision=131089)
    private BigDecimal records;
    private String processed;
    @Column(name="processed_date")
    private Timestamp processedDate;
    @Column(precision=131089)
    private BigDecimal filesize;

    /** Default constructor. */
    public AwsDataProcStatsTest() {
        super();
    }

    /**
     * Access method for rowid.
     *
     * @return the current value of rowid
     */
    public int getRowid() {
        return rowid;
    }

    /**
     * Setter method for rowid.
     *
     * @param aRowid the new value for rowid
     */
    public void setRowid(int aRowid) {
        rowid = aRowid;
    }

    /**
     * Access method for account.
     *
     * @return the current value of account
     */
    public String getAccount() {
        return account;
    }

    /**
     * Setter method for account.
     *
     * @param aAccount the new value for account
     */
    public void setAccount(String aAccount) {
        account = aAccount;
    }

    /**
     * Access method for bucket.
     *
     * @return the current value of bucket
     */
    public String getBucket() {
        return bucket;
    }

    /**
     * Setter method for bucket.
     *
     * @param aBucket the new value for bucket
     */
    public void setBucket(String aBucket) {
        bucket = aBucket;
    }

    /**
     * Access method for filename.
     *
     * @return the current value of filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Setter method for filename.
     *
     * @param aFilename the new value for filename
     */
    public void setFilename(String aFilename) {
        filename = aFilename;
    }

    /**
     * Access method for records.
     *
     * @return the current value of records
     */
    public BigDecimal getRecords() {
        return records;
    }

    /**
     * Setter method for records.
     *
     * @param aRecords the new value for records
     */
    public void setRecords(BigDecimal aRecords) {
        records = aRecords;
    }

    /**
     * Access method for processed.
     *
     * @return the current value of processed
     */
    public String getProcessed() {
        return processed;
    }

    /**
     * Setter method for processed.
     *
     * @param aProcessed the new value for processed
     */
    public void setProcessed(String aProcessed) {
        processed = aProcessed;
    }

    /**
     * Access method for processedDate.
     *
     * @return the current value of processedDate
     */
    public Timestamp getProcessedDate() {
        return processedDate;
    }

    /**
     * Setter method for processedDate.
     *
     * @param aProcessedDate the new value for processedDate
     */
    public void setProcessedDate(Timestamp aProcessedDate) {
        processedDate = aProcessedDate;
    }

    /**
     * Access method for filesize.
     *
     * @return the current value of filesize
     */
    public BigDecimal getFilesize() {
        return filesize;
    }

    /**
     * Setter method for filesize.
     *
     * @param aFilesize the new value for filesize
     */
    public void setFilesize(BigDecimal aFilesize) {
        filesize = aFilesize;
    }

}
