// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Version;

@Entity(name="awsbudgets")
public class Awsbudgets implements Serializable {

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Column(unique=true, nullable=false, precision=10)
    private int id;
    private String name;
    @Column(name="Year", precision=131089)
    private BigDecimal year;
    private String planid;
    private String category;
    @Column(precision=131089)
    private BigDecimal amount;
    @Column(precision=131089)
    private BigDecimal actual;

    /** Default constructor. */
    public Awsbudgets() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for year.
     *
     * @return the current value of year
     */
    public BigDecimal getYear() {
        return year;
    }

    /**
     * Setter method for year.
     *
     * @param aYear the new value for year
     */
    public void setYear(BigDecimal aYear) {
        year = aYear;
    }

    /**
     * Access method for planid.
     *
     * @return the current value of planid
     */
    public String getPlanid() {
        return planid;
    }

    /**
     * Setter method for planid.
     *
     * @param aPlanid the new value for planid
     */
    public void setPlanid(String aPlanid) {
        planid = aPlanid;
    }

    /**
     * Access method for category.
     *
     * @return the current value of category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Setter method for category.
     *
     * @param aCategory the new value for category
     */
    public void setCategory(String aCategory) {
        category = aCategory;
    }

    /**
     * Access method for amount.
     *
     * @return the current value of amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Setter method for amount.
     *
     * @param aAmount the new value for amount
     */
    public void setAmount(BigDecimal aAmount) {
        amount = aAmount;
    }

    /**
     * Access method for actual.
     *
     * @return the current value of actual
     */
    public BigDecimal getActual() {
        return actual;
    }

    /**
     * Setter method for actual.
     *
     * @param aActual the new value for actual
     */
    public void setActual(BigDecimal aActual) {
        actual = aActual;
    }

}
