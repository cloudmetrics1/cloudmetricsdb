// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Version;

@Entity(name="ambar.cost_aws_ingest_actuallineitem_test")
public class Ambar_costAwsIngestActuallineitemTest implements Serializable {

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Column(name="invoice_id", precision=19)
    private long invoiceId;
    @Column(name="line_item_type")
    private String lineItemType;
    @Column(name="usage_account_id", precision=19)
    private long usageAccountId;
    @Column(name="usage_start")
    private String usageStart;
    @Column(name="usage_end")
    private String usageEnd;
    @Column(name="product_code")
    private String productCode;
    @Column(name="usage_type")
    private String usageType;
    private String operation;
    @Column(name="availability_zone")
    private String availabilityZone;
    @Column(name="resource_id")
    private String resourceId;
    @Column(name="usage_amount", precision=17, scale=17)
    private double usageAmount;
    @Column(name="normalization_factor", precision=17, scale=17)
    private double normalizationFactor;
    @Column(name="normalized_usage_amount", precision=17, scale=17)
    private double normalizedUsageAmount;
    @Column(name="currency_code")
    private String currencyCode;
    @Column(name="unblended_rate", precision=17, scale=17)
    private double unblendedRate;
    @Column(name="unblended_cost", precision=17, scale=17)
    private double unblendedCost;
    @Column(name="blended_rate", precision=17, scale=17)
    private double blendedRate;
    @Column(name="blended_cost", precision=17, scale=17)
    private double blendedCost;
    @Column(name="public_on_demand_cost", precision=17, scale=17)
    private double publicOnDemandCost;
    @Column(name="public_on_demand_rate", precision=17, scale=17)
    private double publicOnDemandRate;
    @Column(name="reservation_amortized_upfront_fee")
    private String reservationAmortizedUpfrontFee;
    @Column(name="reservation_amortized_upfront_cost_for_usage")
    private String reservationAmortizedUpfrontCostForUsage;
    @Column(name="reservation_recurring_fee_for_usage")
    private String reservationRecurringFeeForUsage;
    @Column(name="reservation_unused_quantity")
    private String reservationUnusedQuantity;
    @Column(name="reservation_unused_recurring_fee")
    private String reservationUnusedRecurringFee;
    @Column(name="tax_type")
    private String taxType;

    /** Default constructor. */
    public Ambar_costAwsIngestActuallineitemTest() {
        super();
    }

    /**
     * Access method for invoiceId.
     *
     * @return the current value of invoiceId
     */
    public long getInvoiceId() {
        return invoiceId;
    }

    /**
     * Setter method for invoiceId.
     *
     * @param aInvoiceId the new value for invoiceId
     */
    public void setInvoiceId(long aInvoiceId) {
        invoiceId = aInvoiceId;
    }

    /**
     * Access method for lineItemType.
     *
     * @return the current value of lineItemType
     */
    public String getLineItemType() {
        return lineItemType;
    }

    /**
     * Setter method for lineItemType.
     *
     * @param aLineItemType the new value for lineItemType
     */
    public void setLineItemType(String aLineItemType) {
        lineItemType = aLineItemType;
    }

    /**
     * Access method for usageAccountId.
     *
     * @return the current value of usageAccountId
     */
    public long getUsageAccountId() {
        return usageAccountId;
    }

    /**
     * Setter method for usageAccountId.
     *
     * @param aUsageAccountId the new value for usageAccountId
     */
    public void setUsageAccountId(long aUsageAccountId) {
        usageAccountId = aUsageAccountId;
    }

    /**
     * Access method for usageStart.
     *
     * @return the current value of usageStart
     */
    public String getUsageStart() {
        return usageStart;
    }

    /**
     * Setter method for usageStart.
     *
     * @param aUsageStart the new value for usageStart
     */
    public void setUsageStart(String aUsageStart) {
        usageStart = aUsageStart;
    }

    /**
     * Access method for usageEnd.
     *
     * @return the current value of usageEnd
     */
    public String getUsageEnd() {
        return usageEnd;
    }

    /**
     * Setter method for usageEnd.
     *
     * @param aUsageEnd the new value for usageEnd
     */
    public void setUsageEnd(String aUsageEnd) {
        usageEnd = aUsageEnd;
    }

    /**
     * Access method for productCode.
     *
     * @return the current value of productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Setter method for productCode.
     *
     * @param aProductCode the new value for productCode
     */
    public void setProductCode(String aProductCode) {
        productCode = aProductCode;
    }

    /**
     * Access method for usageType.
     *
     * @return the current value of usageType
     */
    public String getUsageType() {
        return usageType;
    }

    /**
     * Setter method for usageType.
     *
     * @param aUsageType the new value for usageType
     */
    public void setUsageType(String aUsageType) {
        usageType = aUsageType;
    }

    /**
     * Access method for operation.
     *
     * @return the current value of operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Setter method for operation.
     *
     * @param aOperation the new value for operation
     */
    public void setOperation(String aOperation) {
        operation = aOperation;
    }

    /**
     * Access method for availabilityZone.
     *
     * @return the current value of availabilityZone
     */
    public String getAvailabilityZone() {
        return availabilityZone;
    }

    /**
     * Setter method for availabilityZone.
     *
     * @param aAvailabilityZone the new value for availabilityZone
     */
    public void setAvailabilityZone(String aAvailabilityZone) {
        availabilityZone = aAvailabilityZone;
    }

    /**
     * Access method for resourceId.
     *
     * @return the current value of resourceId
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Setter method for resourceId.
     *
     * @param aResourceId the new value for resourceId
     */
    public void setResourceId(String aResourceId) {
        resourceId = aResourceId;
    }

    /**
     * Access method for usageAmount.
     *
     * @return the current value of usageAmount
     */
    public double getUsageAmount() {
        return usageAmount;
    }

    /**
     * Setter method for usageAmount.
     *
     * @param aUsageAmount the new value for usageAmount
     */
    public void setUsageAmount(double aUsageAmount) {
        usageAmount = aUsageAmount;
    }

    /**
     * Access method for normalizationFactor.
     *
     * @return the current value of normalizationFactor
     */
    public double getNormalizationFactor() {
        return normalizationFactor;
    }

    /**
     * Setter method for normalizationFactor.
     *
     * @param aNormalizationFactor the new value for normalizationFactor
     */
    public void setNormalizationFactor(double aNormalizationFactor) {
        normalizationFactor = aNormalizationFactor;
    }

    /**
     * Access method for normalizedUsageAmount.
     *
     * @return the current value of normalizedUsageAmount
     */
    public double getNormalizedUsageAmount() {
        return normalizedUsageAmount;
    }

    /**
     * Setter method for normalizedUsageAmount.
     *
     * @param aNormalizedUsageAmount the new value for normalizedUsageAmount
     */
    public void setNormalizedUsageAmount(double aNormalizedUsageAmount) {
        normalizedUsageAmount = aNormalizedUsageAmount;
    }

    /**
     * Access method for currencyCode.
     *
     * @return the current value of currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Setter method for currencyCode.
     *
     * @param aCurrencyCode the new value for currencyCode
     */
    public void setCurrencyCode(String aCurrencyCode) {
        currencyCode = aCurrencyCode;
    }

    /**
     * Access method for unblendedRate.
     *
     * @return the current value of unblendedRate
     */
    public double getUnblendedRate() {
        return unblendedRate;
    }

    /**
     * Setter method for unblendedRate.
     *
     * @param aUnblendedRate the new value for unblendedRate
     */
    public void setUnblendedRate(double aUnblendedRate) {
        unblendedRate = aUnblendedRate;
    }

    /**
     * Access method for unblendedCost.
     *
     * @return the current value of unblendedCost
     */
    public double getUnblendedCost() {
        return unblendedCost;
    }

    /**
     * Setter method for unblendedCost.
     *
     * @param aUnblendedCost the new value for unblendedCost
     */
    public void setUnblendedCost(double aUnblendedCost) {
        unblendedCost = aUnblendedCost;
    }

    /**
     * Access method for blendedRate.
     *
     * @return the current value of blendedRate
     */
    public double getBlendedRate() {
        return blendedRate;
    }

    /**
     * Setter method for blendedRate.
     *
     * @param aBlendedRate the new value for blendedRate
     */
    public void setBlendedRate(double aBlendedRate) {
        blendedRate = aBlendedRate;
    }

    /**
     * Access method for blendedCost.
     *
     * @return the current value of blendedCost
     */
    public double getBlendedCost() {
        return blendedCost;
    }

    /**
     * Setter method for blendedCost.
     *
     * @param aBlendedCost the new value for blendedCost
     */
    public void setBlendedCost(double aBlendedCost) {
        blendedCost = aBlendedCost;
    }

    /**
     * Access method for publicOnDemandCost.
     *
     * @return the current value of publicOnDemandCost
     */
    public double getPublicOnDemandCost() {
        return publicOnDemandCost;
    }

    /**
     * Setter method for publicOnDemandCost.
     *
     * @param aPublicOnDemandCost the new value for publicOnDemandCost
     */
    public void setPublicOnDemandCost(double aPublicOnDemandCost) {
        publicOnDemandCost = aPublicOnDemandCost;
    }

    /**
     * Access method for publicOnDemandRate.
     *
     * @return the current value of publicOnDemandRate
     */
    public double getPublicOnDemandRate() {
        return publicOnDemandRate;
    }

    /**
     * Setter method for publicOnDemandRate.
     *
     * @param aPublicOnDemandRate the new value for publicOnDemandRate
     */
    public void setPublicOnDemandRate(double aPublicOnDemandRate) {
        publicOnDemandRate = aPublicOnDemandRate;
    }

    /**
     * Access method for reservationAmortizedUpfrontFee.
     *
     * @return the current value of reservationAmortizedUpfrontFee
     */
    public String getReservationAmortizedUpfrontFee() {
        return reservationAmortizedUpfrontFee;
    }

    /**
     * Setter method for reservationAmortizedUpfrontFee.
     *
     * @param aReservationAmortizedUpfrontFee the new value for reservationAmortizedUpfrontFee
     */
    public void setReservationAmortizedUpfrontFee(String aReservationAmortizedUpfrontFee) {
        reservationAmortizedUpfrontFee = aReservationAmortizedUpfrontFee;
    }

    /**
     * Access method for reservationAmortizedUpfrontCostForUsage.
     *
     * @return the current value of reservationAmortizedUpfrontCostForUsage
     */
    public String getReservationAmortizedUpfrontCostForUsage() {
        return reservationAmortizedUpfrontCostForUsage;
    }

    /**
     * Setter method for reservationAmortizedUpfrontCostForUsage.
     *
     * @param aReservationAmortizedUpfrontCostForUsage the new value for reservationAmortizedUpfrontCostForUsage
     */
    public void setReservationAmortizedUpfrontCostForUsage(String aReservationAmortizedUpfrontCostForUsage) {
        reservationAmortizedUpfrontCostForUsage = aReservationAmortizedUpfrontCostForUsage;
    }

    /**
     * Access method for reservationRecurringFeeForUsage.
     *
     * @return the current value of reservationRecurringFeeForUsage
     */
    public String getReservationRecurringFeeForUsage() {
        return reservationRecurringFeeForUsage;
    }

    /**
     * Setter method for reservationRecurringFeeForUsage.
     *
     * @param aReservationRecurringFeeForUsage the new value for reservationRecurringFeeForUsage
     */
    public void setReservationRecurringFeeForUsage(String aReservationRecurringFeeForUsage) {
        reservationRecurringFeeForUsage = aReservationRecurringFeeForUsage;
    }

    /**
     * Access method for reservationUnusedQuantity.
     *
     * @return the current value of reservationUnusedQuantity
     */
    public String getReservationUnusedQuantity() {
        return reservationUnusedQuantity;
    }

    /**
     * Setter method for reservationUnusedQuantity.
     *
     * @param aReservationUnusedQuantity the new value for reservationUnusedQuantity
     */
    public void setReservationUnusedQuantity(String aReservationUnusedQuantity) {
        reservationUnusedQuantity = aReservationUnusedQuantity;
    }

    /**
     * Access method for reservationUnusedRecurringFee.
     *
     * @return the current value of reservationUnusedRecurringFee
     */
    public String getReservationUnusedRecurringFee() {
        return reservationUnusedRecurringFee;
    }

    /**
     * Setter method for reservationUnusedRecurringFee.
     *
     * @param aReservationUnusedRecurringFee the new value for reservationUnusedRecurringFee
     */
    public void setReservationUnusedRecurringFee(String aReservationUnusedRecurringFee) {
        reservationUnusedRecurringFee = aReservationUnusedRecurringFee;
    }

    /**
     * Access method for taxType.
     *
     * @return the current value of taxType
     */
    public String getTaxType() {
        return taxType;
    }

    /**
     * Setter method for taxType.
     *
     * @param aTaxType the new value for taxType
     */
    public void setTaxType(String aTaxType) {
        taxType = aTaxType;
    }

}
