// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="amazons3details")
public class Amazons3details implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    private String ownername;
    private String bucketname;
    private Timestamp createdate;

    /** Default constructor. */
    public Amazons3details() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for ownername.
     *
     * @return the current value of ownername
     */
    public String getOwnername() {
        return ownername;
    }

    /**
     * Setter method for ownername.
     *
     * @param aOwnername the new value for ownername
     */
    public void setOwnername(String aOwnername) {
        ownername = aOwnername;
    }

    /**
     * Access method for bucketname.
     *
     * @return the current value of bucketname
     */
    public String getBucketname() {
        return bucketname;
    }

    /**
     * Setter method for bucketname.
     *
     * @param aBucketname the new value for bucketname
     */
    public void setBucketname(String aBucketname) {
        bucketname = aBucketname;
    }

    /**
     * Access method for createdate.
     *
     * @return the current value of createdate
     */
    public Timestamp getCreatedate() {
        return createdate;
    }

    /**
     * Setter method for createdate.
     *
     * @param aCreatedate the new value for createdate
     */
    public void setCreatedate(Timestamp aCreatedate) {
        createdate = aCreatedate;
    }

    /**
     * Compares the key for this instance with another Amazons3details.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Amazons3details and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Amazons3details)) {
            return false;
        }
        Amazons3details that = (Amazons3details) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Amazons3details.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Amazons3details)) return false;
        return this.equalKeys(other) && ((Amazons3details)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Amazons3details |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
