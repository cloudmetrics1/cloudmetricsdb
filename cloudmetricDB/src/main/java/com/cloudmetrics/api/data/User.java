// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="user")
public class User implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="first_name")
    private String firstName;
    @Column(name="password_hash")
    private String passwordHash;
    @Column(name="last_name")
    private String lastName;
    @Column(nullable=false)
    private String email;
    @Column(precision=10)
    private int age;
    @Column(name="address_street")
    private String addressStreet;
    @Column(name="address_city")
    private String addressCity;
    @Column(name="address_zip_code")
    private String addressZipCode;
    @Column(name="address_lat", precision=17, scale=17)
    private double addressLat;
    @Column(name="address_lng", precision=17, scale=17)
    private double addressLng;
    private String phone;
    private String company;
    private String role;
    @Column(name="user_name")
    private String userName;
    @Column(name="created_at")
    private Timestamp createdAt;
    @Column(name="updated_at")
    private Timestamp updatedAt;
    @OneToMany(mappedBy="user")
    private Set<UserRoles> userRoles;

    /** Default constructor. */
    public User() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for firstName.
     *
     * @return the current value of firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter method for firstName.
     *
     * @param aFirstName the new value for firstName
     */
    public void setFirstName(String aFirstName) {
        firstName = aFirstName;
    }

    /**
     * Access method for passwordHash.
     *
     * @return the current value of passwordHash
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * Setter method for passwordHash.
     *
     * @param aPasswordHash the new value for passwordHash
     */
    public void setPasswordHash(String aPasswordHash) {
        passwordHash = aPasswordHash;
    }

    /**
     * Access method for lastName.
     *
     * @return the current value of lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter method for lastName.
     *
     * @param aLastName the new value for lastName
     */
    public void setLastName(String aLastName) {
        lastName = aLastName;
    }

    /**
     * Access method for email.
     *
     * @return the current value of email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter method for email.
     *
     * @param aEmail the new value for email
     */
    public void setEmail(String aEmail) {
        email = aEmail;
    }

    /**
     * Access method for age.
     *
     * @return the current value of age
     */
    public int getAge() {
        return age;
    }

    /**
     * Setter method for age.
     *
     * @param aAge the new value for age
     */
    public void setAge(int aAge) {
        age = aAge;
    }

    /**
     * Access method for addressStreet.
     *
     * @return the current value of addressStreet
     */
    public String getAddressStreet() {
        return addressStreet;
    }

    /**
     * Setter method for addressStreet.
     *
     * @param aAddressStreet the new value for addressStreet
     */
    public void setAddressStreet(String aAddressStreet) {
        addressStreet = aAddressStreet;
    }

    /**
     * Access method for addressCity.
     *
     * @return the current value of addressCity
     */
    public String getAddressCity() {
        return addressCity;
    }

    /**
     * Setter method for addressCity.
     *
     * @param aAddressCity the new value for addressCity
     */
    public void setAddressCity(String aAddressCity) {
        addressCity = aAddressCity;
    }

    /**
     * Access method for addressZipCode.
     *
     * @return the current value of addressZipCode
     */
    public String getAddressZipCode() {
        return addressZipCode;
    }

    /**
     * Setter method for addressZipCode.
     *
     * @param aAddressZipCode the new value for addressZipCode
     */
    public void setAddressZipCode(String aAddressZipCode) {
        addressZipCode = aAddressZipCode;
    }

    /**
     * Access method for addressLat.
     *
     * @return the current value of addressLat
     */
    public double getAddressLat() {
        return addressLat;
    }

    /**
     * Setter method for addressLat.
     *
     * @param aAddressLat the new value for addressLat
     */
    public void setAddressLat(double aAddressLat) {
        addressLat = aAddressLat;
    }

    /**
     * Access method for addressLng.
     *
     * @return the current value of addressLng
     */
    public double getAddressLng() {
        return addressLng;
    }

    /**
     * Setter method for addressLng.
     *
     * @param aAddressLng the new value for addressLng
     */
    public void setAddressLng(double aAddressLng) {
        addressLng = aAddressLng;
    }

    /**
     * Access method for phone.
     *
     * @return the current value of phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Setter method for phone.
     *
     * @param aPhone the new value for phone
     */
    public void setPhone(String aPhone) {
        phone = aPhone;
    }

    /**
     * Access method for company.
     *
     * @return the current value of company
     */
    public String getCompany() {
        return company;
    }

    /**
     * Setter method for company.
     *
     * @param aCompany the new value for company
     */
    public void setCompany(String aCompany) {
        company = aCompany;
    }

    /**
     * Access method for role.
     *
     * @return the current value of role
     */
    public String getRole() {
        return role;
    }

    /**
     * Setter method for role.
     *
     * @param aRole the new value for role
     */
    public void setRole(String aRole) {
        role = aRole;
    }

    /**
     * Access method for userName.
     *
     * @return the current value of userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Setter method for userName.
     *
     * @param aUserName the new value for userName
     */
    public void setUserName(String aUserName) {
        userName = aUserName;
    }

    /**
     * Access method for createdAt.
     *
     * @return the current value of createdAt
     */
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    /**
     * Setter method for createdAt.
     *
     * @param aCreatedAt the new value for createdAt
     */
    public void setCreatedAt(Timestamp aCreatedAt) {
        createdAt = aCreatedAt;
    }

    /**
     * Access method for updatedAt.
     *
     * @return the current value of updatedAt
     */
    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Setter method for updatedAt.
     *
     * @param aUpdatedAt the new value for updatedAt
     */
    public void setUpdatedAt(Timestamp aUpdatedAt) {
        updatedAt = aUpdatedAt;
    }

    /**
     * Access method for userRoles.
     *
     * @return the current value of userRoles
     */
    public Set<UserRoles> getUserRoles() {
        return userRoles;
    }

    /**
     * Setter method for userRoles.
     *
     * @param aUserRoles the new value for userRoles
     */
    public void setUserRoles(Set<UserRoles> aUserRoles) {
        userRoles = aUserRoles;
    }

    /**
     * Compares the key for this instance with another User.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class User and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof User)) {
            return false;
        }
        User that = (User) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another User.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof User)) return false;
        return this.equalKeys(other) && ((User)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[User |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
