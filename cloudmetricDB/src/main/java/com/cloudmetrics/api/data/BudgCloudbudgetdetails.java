// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="budg_cloudbudgetdetails")
public class BudgCloudbudgetdetails implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    private String amount;
    @OneToMany(mappedBy="budgCloudbudgetdetails")
    private Set<BudgCloudbudgetactuals> budgCloudbudgetactuals;
    @ManyToOne
    @JoinColumn(name="mstr_category_id")
    private MstrCategories mstrCategories;
    @ManyToOne
    @JoinColumn(name="budg_cloudbudget_id")
    private BudgCloudbudgets budgCloudbudgets;
    @ManyToOne
    @JoinColumn(name="mstr_vendor_id")
    private MstrVendors mstrVendors;
    @ManyToOne
    @JoinColumn(name="mstr_project_id")
    private MstrProjects mstrProjects;
    @ManyToOne
    @JoinColumn(name="mstr_team_id")
    private MstrTeams mstrTeams;

    /** Default constructor. */
    public BudgCloudbudgetdetails() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for amount.
     *
     * @return the current value of amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Setter method for amount.
     *
     * @param aAmount the new value for amount
     */
    public void setAmount(String aAmount) {
        amount = aAmount;
    }

    /**
     * Access method for budgCloudbudgetactuals.
     *
     * @return the current value of budgCloudbudgetactuals
     */
    public Set<BudgCloudbudgetactuals> getBudgCloudbudgetactuals() {
        return budgCloudbudgetactuals;
    }

    /**
     * Setter method for budgCloudbudgetactuals.
     *
     * @param aBudgCloudbudgetactuals the new value for budgCloudbudgetactuals
     */
    public void setBudgCloudbudgetactuals(Set<BudgCloudbudgetactuals> aBudgCloudbudgetactuals) {
        budgCloudbudgetactuals = aBudgCloudbudgetactuals;
    }

    /**
     * Access method for mstrCategories.
     *
     * @return the current value of mstrCategories
     */
    public MstrCategories getMstrCategories() {
        return mstrCategories;
    }

    /**
     * Setter method for mstrCategories.
     *
     * @param aMstrCategories the new value for mstrCategories
     */
    public void setMstrCategories(MstrCategories aMstrCategories) {
        mstrCategories = aMstrCategories;
    }

    /**
     * Access method for budgCloudbudgets.
     *
     * @return the current value of budgCloudbudgets
     */
    public BudgCloudbudgets getBudgCloudbudgets() {
        return budgCloudbudgets;
    }

    /**
     * Setter method for budgCloudbudgets.
     *
     * @param aBudgCloudbudgets the new value for budgCloudbudgets
     */
    public void setBudgCloudbudgets(BudgCloudbudgets aBudgCloudbudgets) {
        budgCloudbudgets = aBudgCloudbudgets;
    }

    /**
     * Access method for mstrVendors.
     *
     * @return the current value of mstrVendors
     */
    public MstrVendors getMstrVendors() {
        return mstrVendors;
    }

    /**
     * Setter method for mstrVendors.
     *
     * @param aMstrVendors the new value for mstrVendors
     */
    public void setMstrVendors(MstrVendors aMstrVendors) {
        mstrVendors = aMstrVendors;
    }

    /**
     * Access method for mstrProjects.
     *
     * @return the current value of mstrProjects
     */
    public MstrProjects getMstrProjects() {
        return mstrProjects;
    }

    /**
     * Setter method for mstrProjects.
     *
     * @param aMstrProjects the new value for mstrProjects
     */
    public void setMstrProjects(MstrProjects aMstrProjects) {
        mstrProjects = aMstrProjects;
    }

    /**
     * Access method for mstrTeams.
     *
     * @return the current value of mstrTeams
     */
    public MstrTeams getMstrTeams() {
        return mstrTeams;
    }

    /**
     * Setter method for mstrTeams.
     *
     * @param aMstrTeams the new value for mstrTeams
     */
    public void setMstrTeams(MstrTeams aMstrTeams) {
        mstrTeams = aMstrTeams;
    }

    /**
     * Compares the key for this instance with another BudgCloudbudgetdetails.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class BudgCloudbudgetdetails and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof BudgCloudbudgetdetails)) {
            return false;
        }
        BudgCloudbudgetdetails that = (BudgCloudbudgetdetails) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another BudgCloudbudgetdetails.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BudgCloudbudgetdetails)) return false;
        return this.equalKeys(other) && ((BudgCloudbudgetdetails)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BudgCloudbudgetdetails |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
