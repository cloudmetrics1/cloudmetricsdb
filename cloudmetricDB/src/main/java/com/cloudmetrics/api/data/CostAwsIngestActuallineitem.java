// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity(name="cost_aws_ingest_actuallineitem")
public class CostAwsIngestActuallineitem implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=19)
    private long id;
    private String tags;
    @Column(name="invoice_id", length=63)
    private String invoiceId;
    @Column(name="line_item_type", nullable=false, length=50)
    private String lineItemType;
    @Column(name="usage_account_id", nullable=false, length=50)
    private String usageAccountId;
    @Column(name="usage_start", nullable=false)
    private Timestamp usageStart;
    @Column(name="usage_end", nullable=false)
    private Timestamp usageEnd;
    @Column(name="product_code", nullable=false, length=50)
    private String productCode;
    @Column(name="usage_type", length=50)
    private String usageType;
    @Column(length=50)
    private String operation;
    @Column(name="availability_zone", length=50)
    private String availabilityZone;
    @Column(name="resource_id", length=256)
    private String resourceId;
    @Column(name="usage_amount", precision=17, scale=9)
    private BigDecimal usageAmount;
    @Column(name="normalization_factor", precision=17, scale=17)
    private double normalizationFactor;
    @Column(name="normalized_usage_amount", precision=17, scale=9)
    private BigDecimal normalizedUsageAmount;
    @Column(name="currency_code", nullable=false, length=10)
    private String currencyCode;
    @Column(name="unblended_rate", precision=17, scale=9)
    private BigDecimal unblendedRate;
    @Column(name="unblended_cost", precision=17, scale=9)
    private BigDecimal unblendedCost;
    @Column(name="blended_rate", precision=17, scale=9)
    private BigDecimal blendedRate;
    @Column(name="blended_cost", precision=17, scale=9)
    private BigDecimal blendedCost;
    @Column(name="public_on_demand_cost", precision=17, scale=9)
    private BigDecimal publicOnDemandCost;
    @Column(name="public_on_demand_rate", precision=17, scale=9)
    private BigDecimal publicOnDemandRate;
    @Column(name="reservation_amortized_upfront_fee", precision=17, scale=9)
    private BigDecimal reservationAmortizedUpfrontFee;
    @Column(name="reservation_amortized_upfront_cost_for_usage", precision=17, scale=9)
    private BigDecimal reservationAmortizedUpfrontCostForUsage;
    @Column(name="reservation_recurring_fee_for_usage", precision=17, scale=9)
    private BigDecimal reservationRecurringFeeForUsage;
    @Column(name="reservation_unused_quantity", precision=17, scale=9)
    private BigDecimal reservationUnusedQuantity;
    @Column(name="reservation_unused_recurring_fee", precision=17, scale=9)
    private BigDecimal reservationUnusedRecurringFee;
    @Column(name="tax_type")
    private String taxType;
    @ManyToOne(optional=false)
    @JoinColumn(name="cost_aws_ingest_curdata_id", nullable=false)
    private CostAwsIngestCurdata costAwsIngestCurdata;
    @ManyToOne(optional=false)
    @JoinColumn(name="cost_aws_ingest_interval_id", nullable=false)
    private CostAwsIngestInterval costAwsIngestInterval;
    @ManyToOne
    @JoinColumn(name="cost_aws_ingest_pricing_id")
    private CostAwsIngestPricing costAwsIngestPricing;
    @ManyToOne
    @JoinColumn(name="cost_aws_ingest_product_id")
    private CostAwsIngestProduct costAwsIngestProduct;
    @ManyToOne
    @JoinColumn(name="cost_aws_ingest_reservation_id")
    private CostAwsIngestReservation costAwsIngestReservation;

    /** Default constructor. */
    public CostAwsIngestActuallineitem() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(long aId) {
        id = aId;
    }

    /**
     * Access method for tags.
     *
     * @return the current value of tags
     */
    public String getTags() {
        return tags;
    }

    /**
     * Setter method for tags.
     *
     * @param aTags the new value for tags
     */
    public void setTags(String aTags) {
        tags = aTags;
    }

    /**
     * Access method for invoiceId.
     *
     * @return the current value of invoiceId
     */
    public String getInvoiceId() {
        return invoiceId;
    }

    /**
     * Setter method for invoiceId.
     *
     * @param aInvoiceId the new value for invoiceId
     */
    public void setInvoiceId(String aInvoiceId) {
        invoiceId = aInvoiceId;
    }

    /**
     * Access method for lineItemType.
     *
     * @return the current value of lineItemType
     */
    public String getLineItemType() {
        return lineItemType;
    }

    /**
     * Setter method for lineItemType.
     *
     * @param aLineItemType the new value for lineItemType
     */
    public void setLineItemType(String aLineItemType) {
        lineItemType = aLineItemType;
    }

    /**
     * Access method for usageAccountId.
     *
     * @return the current value of usageAccountId
     */
    public String getUsageAccountId() {
        return usageAccountId;
    }

    /**
     * Setter method for usageAccountId.
     *
     * @param aUsageAccountId the new value for usageAccountId
     */
    public void setUsageAccountId(String aUsageAccountId) {
        usageAccountId = aUsageAccountId;
    }

    /**
     * Access method for usageStart.
     *
     * @return the current value of usageStart
     */
    public Timestamp getUsageStart() {
        return usageStart;
    }

    /**
     * Setter method for usageStart.
     *
     * @param aUsageStart the new value for usageStart
     */
    public void setUsageStart(Timestamp aUsageStart) {
        usageStart = aUsageStart;
    }

    /**
     * Access method for usageEnd.
     *
     * @return the current value of usageEnd
     */
    public Timestamp getUsageEnd() {
        return usageEnd;
    }

    /**
     * Setter method for usageEnd.
     *
     * @param aUsageEnd the new value for usageEnd
     */
    public void setUsageEnd(Timestamp aUsageEnd) {
        usageEnd = aUsageEnd;
    }

    /**
     * Access method for productCode.
     *
     * @return the current value of productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Setter method for productCode.
     *
     * @param aProductCode the new value for productCode
     */
    public void setProductCode(String aProductCode) {
        productCode = aProductCode;
    }

    /**
     * Access method for usageType.
     *
     * @return the current value of usageType
     */
    public String getUsageType() {
        return usageType;
    }

    /**
     * Setter method for usageType.
     *
     * @param aUsageType the new value for usageType
     */
    public void setUsageType(String aUsageType) {
        usageType = aUsageType;
    }

    /**
     * Access method for operation.
     *
     * @return the current value of operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Setter method for operation.
     *
     * @param aOperation the new value for operation
     */
    public void setOperation(String aOperation) {
        operation = aOperation;
    }

    /**
     * Access method for availabilityZone.
     *
     * @return the current value of availabilityZone
     */
    public String getAvailabilityZone() {
        return availabilityZone;
    }

    /**
     * Setter method for availabilityZone.
     *
     * @param aAvailabilityZone the new value for availabilityZone
     */
    public void setAvailabilityZone(String aAvailabilityZone) {
        availabilityZone = aAvailabilityZone;
    }

    /**
     * Access method for resourceId.
     *
     * @return the current value of resourceId
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * Setter method for resourceId.
     *
     * @param aResourceId the new value for resourceId
     */
    public void setResourceId(String aResourceId) {
        resourceId = aResourceId;
    }

    /**
     * Access method for usageAmount.
     *
     * @return the current value of usageAmount
     */
    public BigDecimal getUsageAmount() {
        return usageAmount;
    }

    /**
     * Setter method for usageAmount.
     *
     * @param aUsageAmount the new value for usageAmount
     */
    public void setUsageAmount(BigDecimal aUsageAmount) {
        usageAmount = aUsageAmount;
    }

    /**
     * Access method for normalizationFactor.
     *
     * @return the current value of normalizationFactor
     */
    public double getNormalizationFactor() {
        return normalizationFactor;
    }

    /**
     * Setter method for normalizationFactor.
     *
     * @param aNormalizationFactor the new value for normalizationFactor
     */
    public void setNormalizationFactor(double aNormalizationFactor) {
        normalizationFactor = aNormalizationFactor;
    }

    /**
     * Access method for normalizedUsageAmount.
     *
     * @return the current value of normalizedUsageAmount
     */
    public BigDecimal getNormalizedUsageAmount() {
        return normalizedUsageAmount;
    }

    /**
     * Setter method for normalizedUsageAmount.
     *
     * @param aNormalizedUsageAmount the new value for normalizedUsageAmount
     */
    public void setNormalizedUsageAmount(BigDecimal aNormalizedUsageAmount) {
        normalizedUsageAmount = aNormalizedUsageAmount;
    }

    /**
     * Access method for currencyCode.
     *
     * @return the current value of currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Setter method for currencyCode.
     *
     * @param aCurrencyCode the new value for currencyCode
     */
    public void setCurrencyCode(String aCurrencyCode) {
        currencyCode = aCurrencyCode;
    }

    /**
     * Access method for unblendedRate.
     *
     * @return the current value of unblendedRate
     */
    public BigDecimal getUnblendedRate() {
        return unblendedRate;
    }

    /**
     * Setter method for unblendedRate.
     *
     * @param aUnblendedRate the new value for unblendedRate
     */
    public void setUnblendedRate(BigDecimal aUnblendedRate) {
        unblendedRate = aUnblendedRate;
    }

    /**
     * Access method for unblendedCost.
     *
     * @return the current value of unblendedCost
     */
    public BigDecimal getUnblendedCost() {
        return unblendedCost;
    }

    /**
     * Setter method for unblendedCost.
     *
     * @param aUnblendedCost the new value for unblendedCost
     */
    public void setUnblendedCost(BigDecimal aUnblendedCost) {
        unblendedCost = aUnblendedCost;
    }

    /**
     * Access method for blendedRate.
     *
     * @return the current value of blendedRate
     */
    public BigDecimal getBlendedRate() {
        return blendedRate;
    }

    /**
     * Setter method for blendedRate.
     *
     * @param aBlendedRate the new value for blendedRate
     */
    public void setBlendedRate(BigDecimal aBlendedRate) {
        blendedRate = aBlendedRate;
    }

    /**
     * Access method for blendedCost.
     *
     * @return the current value of blendedCost
     */
    public BigDecimal getBlendedCost() {
        return blendedCost;
    }

    /**
     * Setter method for blendedCost.
     *
     * @param aBlendedCost the new value for blendedCost
     */
    public void setBlendedCost(BigDecimal aBlendedCost) {
        blendedCost = aBlendedCost;
    }

    /**
     * Access method for publicOnDemandCost.
     *
     * @return the current value of publicOnDemandCost
     */
    public BigDecimal getPublicOnDemandCost() {
        return publicOnDemandCost;
    }

    /**
     * Setter method for publicOnDemandCost.
     *
     * @param aPublicOnDemandCost the new value for publicOnDemandCost
     */
    public void setPublicOnDemandCost(BigDecimal aPublicOnDemandCost) {
        publicOnDemandCost = aPublicOnDemandCost;
    }

    /**
     * Access method for publicOnDemandRate.
     *
     * @return the current value of publicOnDemandRate
     */
    public BigDecimal getPublicOnDemandRate() {
        return publicOnDemandRate;
    }

    /**
     * Setter method for publicOnDemandRate.
     *
     * @param aPublicOnDemandRate the new value for publicOnDemandRate
     */
    public void setPublicOnDemandRate(BigDecimal aPublicOnDemandRate) {
        publicOnDemandRate = aPublicOnDemandRate;
    }

    /**
     * Access method for reservationAmortizedUpfrontFee.
     *
     * @return the current value of reservationAmortizedUpfrontFee
     */
    public BigDecimal getReservationAmortizedUpfrontFee() {
        return reservationAmortizedUpfrontFee;
    }

    /**
     * Setter method for reservationAmortizedUpfrontFee.
     *
     * @param aReservationAmortizedUpfrontFee the new value for reservationAmortizedUpfrontFee
     */
    public void setReservationAmortizedUpfrontFee(BigDecimal aReservationAmortizedUpfrontFee) {
        reservationAmortizedUpfrontFee = aReservationAmortizedUpfrontFee;
    }

    /**
     * Access method for reservationAmortizedUpfrontCostForUsage.
     *
     * @return the current value of reservationAmortizedUpfrontCostForUsage
     */
    public BigDecimal getReservationAmortizedUpfrontCostForUsage() {
        return reservationAmortizedUpfrontCostForUsage;
    }

    /**
     * Setter method for reservationAmortizedUpfrontCostForUsage.
     *
     * @param aReservationAmortizedUpfrontCostForUsage the new value for reservationAmortizedUpfrontCostForUsage
     */
    public void setReservationAmortizedUpfrontCostForUsage(BigDecimal aReservationAmortizedUpfrontCostForUsage) {
        reservationAmortizedUpfrontCostForUsage = aReservationAmortizedUpfrontCostForUsage;
    }

    /**
     * Access method for reservationRecurringFeeForUsage.
     *
     * @return the current value of reservationRecurringFeeForUsage
     */
    public BigDecimal getReservationRecurringFeeForUsage() {
        return reservationRecurringFeeForUsage;
    }

    /**
     * Setter method for reservationRecurringFeeForUsage.
     *
     * @param aReservationRecurringFeeForUsage the new value for reservationRecurringFeeForUsage
     */
    public void setReservationRecurringFeeForUsage(BigDecimal aReservationRecurringFeeForUsage) {
        reservationRecurringFeeForUsage = aReservationRecurringFeeForUsage;
    }

    /**
     * Access method for reservationUnusedQuantity.
     *
     * @return the current value of reservationUnusedQuantity
     */
    public BigDecimal getReservationUnusedQuantity() {
        return reservationUnusedQuantity;
    }

    /**
     * Setter method for reservationUnusedQuantity.
     *
     * @param aReservationUnusedQuantity the new value for reservationUnusedQuantity
     */
    public void setReservationUnusedQuantity(BigDecimal aReservationUnusedQuantity) {
        reservationUnusedQuantity = aReservationUnusedQuantity;
    }

    /**
     * Access method for reservationUnusedRecurringFee.
     *
     * @return the current value of reservationUnusedRecurringFee
     */
    public BigDecimal getReservationUnusedRecurringFee() {
        return reservationUnusedRecurringFee;
    }

    /**
     * Setter method for reservationUnusedRecurringFee.
     *
     * @param aReservationUnusedRecurringFee the new value for reservationUnusedRecurringFee
     */
    public void setReservationUnusedRecurringFee(BigDecimal aReservationUnusedRecurringFee) {
        reservationUnusedRecurringFee = aReservationUnusedRecurringFee;
    }

    /**
     * Access method for taxType.
     *
     * @return the current value of taxType
     */
    public String getTaxType() {
        return taxType;
    }

    /**
     * Setter method for taxType.
     *
     * @param aTaxType the new value for taxType
     */
    public void setTaxType(String aTaxType) {
        taxType = aTaxType;
    }

    /**
     * Access method for costAwsIngestCurdata.
     *
     * @return the current value of costAwsIngestCurdata
     */
    public CostAwsIngestCurdata getCostAwsIngestCurdata() {
        return costAwsIngestCurdata;
    }

    /**
     * Setter method for costAwsIngestCurdata.
     *
     * @param aCostAwsIngestCurdata the new value for costAwsIngestCurdata
     */
    public void setCostAwsIngestCurdata(CostAwsIngestCurdata aCostAwsIngestCurdata) {
        costAwsIngestCurdata = aCostAwsIngestCurdata;
    }

    /**
     * Access method for costAwsIngestInterval.
     *
     * @return the current value of costAwsIngestInterval
     */
    public CostAwsIngestInterval getCostAwsIngestInterval() {
        return costAwsIngestInterval;
    }

    /**
     * Setter method for costAwsIngestInterval.
     *
     * @param aCostAwsIngestInterval the new value for costAwsIngestInterval
     */
    public void setCostAwsIngestInterval(CostAwsIngestInterval aCostAwsIngestInterval) {
        costAwsIngestInterval = aCostAwsIngestInterval;
    }

    /**
     * Access method for costAwsIngestPricing.
     *
     * @return the current value of costAwsIngestPricing
     */
    public CostAwsIngestPricing getCostAwsIngestPricing() {
        return costAwsIngestPricing;
    }

    /**
     * Setter method for costAwsIngestPricing.
     *
     * @param aCostAwsIngestPricing the new value for costAwsIngestPricing
     */
    public void setCostAwsIngestPricing(CostAwsIngestPricing aCostAwsIngestPricing) {
        costAwsIngestPricing = aCostAwsIngestPricing;
    }

    /**
     * Access method for costAwsIngestProduct.
     *
     * @return the current value of costAwsIngestProduct
     */
    public CostAwsIngestProduct getCostAwsIngestProduct() {
        return costAwsIngestProduct;
    }

    /**
     * Setter method for costAwsIngestProduct.
     *
     * @param aCostAwsIngestProduct the new value for costAwsIngestProduct
     */
    public void setCostAwsIngestProduct(CostAwsIngestProduct aCostAwsIngestProduct) {
        costAwsIngestProduct = aCostAwsIngestProduct;
    }

    /**
     * Access method for costAwsIngestReservation.
     *
     * @return the current value of costAwsIngestReservation
     */
    public CostAwsIngestReservation getCostAwsIngestReservation() {
        return costAwsIngestReservation;
    }

    /**
     * Setter method for costAwsIngestReservation.
     *
     * @param aCostAwsIngestReservation the new value for costAwsIngestReservation
     */
    public void setCostAwsIngestReservation(CostAwsIngestReservation aCostAwsIngestReservation) {
        costAwsIngestReservation = aCostAwsIngestReservation;
    }

    /**
     * Compares the key for this instance with another CostAwsIngestActuallineitem.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class CostAwsIngestActuallineitem and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof CostAwsIngestActuallineitem)) {
            return false;
        }
        CostAwsIngestActuallineitem that = (CostAwsIngestActuallineitem) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another CostAwsIngestActuallineitem.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CostAwsIngestActuallineitem)) return false;
        return this.equalKeys(other) && ((CostAwsIngestActuallineitem)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = (int)(getId() ^ (getId()>>>32));
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[CostAwsIngestActuallineitem |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Long.valueOf(getId()));
        return ret;
    }

}
