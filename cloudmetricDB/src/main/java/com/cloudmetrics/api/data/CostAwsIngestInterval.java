// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="cost_aws_ingest_interval")
public class CostAwsIngestInterval implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="interval_start", nullable=false)
    private Timestamp intervalStart;
    @Column(name="interval_end", nullable=false)
    private Timestamp intervalEnd;
    @Column(name="bill_id", nullable=false, precision=10)
    private int billId;
    @OneToMany(mappedBy="costAwsIngestInterval")
    private Set<CostAwsIngestActuallineitem> costAwsIngestActuallineitem;
    @OneToMany(mappedBy="costAwsIngestInterval")
    private Set<CostAwsIngestActuallineitemTest> costAwsIngestActuallineitemTest;

    /** Default constructor. */
    public CostAwsIngestInterval() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for intervalStart.
     *
     * @return the current value of intervalStart
     */
    public Timestamp getIntervalStart() {
        return intervalStart;
    }

    /**
     * Setter method for intervalStart.
     *
     * @param aIntervalStart the new value for intervalStart
     */
    public void setIntervalStart(Timestamp aIntervalStart) {
        intervalStart = aIntervalStart;
    }

    /**
     * Access method for intervalEnd.
     *
     * @return the current value of intervalEnd
     */
    public Timestamp getIntervalEnd() {
        return intervalEnd;
    }

    /**
     * Setter method for intervalEnd.
     *
     * @param aIntervalEnd the new value for intervalEnd
     */
    public void setIntervalEnd(Timestamp aIntervalEnd) {
        intervalEnd = aIntervalEnd;
    }

    /**
     * Access method for billId.
     *
     * @return the current value of billId
     */
    public int getBillId() {
        return billId;
    }

    /**
     * Setter method for billId.
     *
     * @param aBillId the new value for billId
     */
    public void setBillId(int aBillId) {
        billId = aBillId;
    }

    /**
     * Access method for costAwsIngestActuallineitem.
     *
     * @return the current value of costAwsIngestActuallineitem
     */
    public Set<CostAwsIngestActuallineitem> getCostAwsIngestActuallineitem() {
        return costAwsIngestActuallineitem;
    }

    /**
     * Setter method for costAwsIngestActuallineitem.
     *
     * @param aCostAwsIngestActuallineitem the new value for costAwsIngestActuallineitem
     */
    public void setCostAwsIngestActuallineitem(Set<CostAwsIngestActuallineitem> aCostAwsIngestActuallineitem) {
        costAwsIngestActuallineitem = aCostAwsIngestActuallineitem;
    }

    /**
     * Access method for costAwsIngestActuallineitemTest.
     *
     * @return the current value of costAwsIngestActuallineitemTest
     */
    public Set<CostAwsIngestActuallineitemTest> getCostAwsIngestActuallineitemTest() {
        return costAwsIngestActuallineitemTest;
    }

    /**
     * Setter method for costAwsIngestActuallineitemTest.
     *
     * @param aCostAwsIngestActuallineitemTest the new value for costAwsIngestActuallineitemTest
     */
    public void setCostAwsIngestActuallineitemTest(Set<CostAwsIngestActuallineitemTest> aCostAwsIngestActuallineitemTest) {
        costAwsIngestActuallineitemTest = aCostAwsIngestActuallineitemTest;
    }

    /**
     * Compares the key for this instance with another CostAwsIngestInterval.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class CostAwsIngestInterval and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof CostAwsIngestInterval)) {
            return false;
        }
        CostAwsIngestInterval that = (CostAwsIngestInterval) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another CostAwsIngestInterval.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CostAwsIngestInterval)) return false;
        return this.equalKeys(other) && ((CostAwsIngestInterval)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[CostAwsIngestInterval |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
