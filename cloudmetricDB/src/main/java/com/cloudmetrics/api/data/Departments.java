// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="departments")
public class Departments implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(unique=true, nullable=false, precision=131089)
    private BigDecimal id;
    private String name;
    private String description;
    @Column(precision=131089)
    private BigDecimal headcount;
    @Column(precision=131089)
    private BigDecimal parentid;
    @Column(precision=131089)
    private BigDecimal costcenterid;
    private String deptcode;
    @OneToMany(mappedBy="departments")
    private Set<AllcDepartmentsTest> allcDepartmentsTest;
    @OneToMany(mappedBy="departments")
    private Set<AllcDepartments> allcDepartments;
    @OneToMany(mappedBy="departments")
    private Set<BudgCloudbudgets> budgCloudbudgets;
    @ManyToOne
    @JoinColumn(name="businessunitid")
    private Businessunit businessunit;

    /** Default constructor. */
    public Departments() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(BigDecimal aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for description.
     *
     * @return the current value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter method for description.
     *
     * @param aDescription the new value for description
     */
    public void setDescription(String aDescription) {
        description = aDescription;
    }

    /**
     * Access method for headcount.
     *
     * @return the current value of headcount
     */
    public BigDecimal getHeadcount() {
        return headcount;
    }

    /**
     * Setter method for headcount.
     *
     * @param aHeadcount the new value for headcount
     */
    public void setHeadcount(BigDecimal aHeadcount) {
        headcount = aHeadcount;
    }

    /**
     * Access method for parentid.
     *
     * @return the current value of parentid
     */
    public BigDecimal getParentid() {
        return parentid;
    }

    /**
     * Setter method for parentid.
     *
     * @param aParentid the new value for parentid
     */
    public void setParentid(BigDecimal aParentid) {
        parentid = aParentid;
    }

    /**
     * Access method for costcenterid.
     *
     * @return the current value of costcenterid
     */
    public BigDecimal getCostcenterid() {
        return costcenterid;
    }

    /**
     * Setter method for costcenterid.
     *
     * @param aCostcenterid the new value for costcenterid
     */
    public void setCostcenterid(BigDecimal aCostcenterid) {
        costcenterid = aCostcenterid;
    }

    /**
     * Access method for deptcode.
     *
     * @return the current value of deptcode
     */
    public String getDeptcode() {
        return deptcode;
    }

    /**
     * Setter method for deptcode.
     *
     * @param aDeptcode the new value for deptcode
     */
    public void setDeptcode(String aDeptcode) {
        deptcode = aDeptcode;
    }

    /**
     * Access method for allcDepartmentsTest.
     *
     * @return the current value of allcDepartmentsTest
     */
    public Set<AllcDepartmentsTest> getAllcDepartmentsTest() {
        return allcDepartmentsTest;
    }

    /**
     * Setter method for allcDepartmentsTest.
     *
     * @param aAllcDepartmentsTest the new value for allcDepartmentsTest
     */
    public void setAllcDepartmentsTest(Set<AllcDepartmentsTest> aAllcDepartmentsTest) {
        allcDepartmentsTest = aAllcDepartmentsTest;
    }

    /**
     * Access method for allcDepartments.
     *
     * @return the current value of allcDepartments
     */
    public Set<AllcDepartments> getAllcDepartments() {
        return allcDepartments;
    }

    /**
     * Setter method for allcDepartments.
     *
     * @param aAllcDepartments the new value for allcDepartments
     */
    public void setAllcDepartments(Set<AllcDepartments> aAllcDepartments) {
        allcDepartments = aAllcDepartments;
    }

    /**
     * Access method for budgCloudbudgets.
     *
     * @return the current value of budgCloudbudgets
     */
    public Set<BudgCloudbudgets> getBudgCloudbudgets() {
        return budgCloudbudgets;
    }

    /**
     * Setter method for budgCloudbudgets.
     *
     * @param aBudgCloudbudgets the new value for budgCloudbudgets
     */
    public void setBudgCloudbudgets(Set<BudgCloudbudgets> aBudgCloudbudgets) {
        budgCloudbudgets = aBudgCloudbudgets;
    }

    /**
     * Access method for businessunit.
     *
     * @return the current value of businessunit
     */
    public Businessunit getBusinessunit() {
        return businessunit;
    }

    /**
     * Setter method for businessunit.
     *
     * @param aBusinessunit the new value for businessunit
     */
    public void setBusinessunit(Businessunit aBusinessunit) {
        businessunit = aBusinessunit;
    }

    /**
     * Compares the key for this instance with another Departments.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Departments and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Departments)) {
            return false;
        }
        Departments that = (Departments) other;
        Object myId = this.getId();
        Object yourId = that.getId();
        if (myId==null ? yourId!=null : !myId.equals(yourId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Departments.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Departments)) return false;
        return this.equalKeys(other) && ((Departments)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getId() == null) {
            i = 0;
        } else {
            i = getId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Departments |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", getId());
        return ret;
    }

}
