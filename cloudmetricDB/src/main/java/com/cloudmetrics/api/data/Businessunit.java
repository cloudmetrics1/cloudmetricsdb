// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="businessunit")
public class Businessunit implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(unique=true, nullable=false, precision=131089)
    private BigDecimal id;
    private String name;
    private String description;
    @Column(precision=131089)
    private BigDecimal hierarchylevel;
    @Column(precision=131089)
    private BigDecimal parentid;
    private String bucode;
    @OneToMany(mappedBy="businessunit")
    private Set<AllcBusinessunit> allcBusinessunit;
    @OneToMany(mappedBy="businessunit")
    private Set<AllcBusinessunitTest> allcBusinessunitTest;
    @ManyToOne
    @JoinColumn(name="companyid")
    private Company company;
    @OneToMany(mappedBy="businessunit")
    private Set<Departments> departments;

    /** Default constructor. */
    public Businessunit() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(BigDecimal aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for description.
     *
     * @return the current value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter method for description.
     *
     * @param aDescription the new value for description
     */
    public void setDescription(String aDescription) {
        description = aDescription;
    }

    /**
     * Access method for hierarchylevel.
     *
     * @return the current value of hierarchylevel
     */
    public BigDecimal getHierarchylevel() {
        return hierarchylevel;
    }

    /**
     * Setter method for hierarchylevel.
     *
     * @param aHierarchylevel the new value for hierarchylevel
     */
    public void setHierarchylevel(BigDecimal aHierarchylevel) {
        hierarchylevel = aHierarchylevel;
    }

    /**
     * Access method for parentid.
     *
     * @return the current value of parentid
     */
    public BigDecimal getParentid() {
        return parentid;
    }

    /**
     * Setter method for parentid.
     *
     * @param aParentid the new value for parentid
     */
    public void setParentid(BigDecimal aParentid) {
        parentid = aParentid;
    }

    /**
     * Access method for bucode.
     *
     * @return the current value of bucode
     */
    public String getBucode() {
        return bucode;
    }

    /**
     * Setter method for bucode.
     *
     * @param aBucode the new value for bucode
     */
    public void setBucode(String aBucode) {
        bucode = aBucode;
    }

    /**
     * Access method for allcBusinessunit.
     *
     * @return the current value of allcBusinessunit
     */
    public Set<AllcBusinessunit> getAllcBusinessunit() {
        return allcBusinessunit;
    }

    /**
     * Setter method for allcBusinessunit.
     *
     * @param aAllcBusinessunit the new value for allcBusinessunit
     */
    public void setAllcBusinessunit(Set<AllcBusinessunit> aAllcBusinessunit) {
        allcBusinessunit = aAllcBusinessunit;
    }

    /**
     * Access method for allcBusinessunitTest.
     *
     * @return the current value of allcBusinessunitTest
     */
    public Set<AllcBusinessunitTest> getAllcBusinessunitTest() {
        return allcBusinessunitTest;
    }

    /**
     * Setter method for allcBusinessunitTest.
     *
     * @param aAllcBusinessunitTest the new value for allcBusinessunitTest
     */
    public void setAllcBusinessunitTest(Set<AllcBusinessunitTest> aAllcBusinessunitTest) {
        allcBusinessunitTest = aAllcBusinessunitTest;
    }

    /**
     * Access method for company.
     *
     * @return the current value of company
     */
    public Company getCompany() {
        return company;
    }

    /**
     * Setter method for company.
     *
     * @param aCompany the new value for company
     */
    public void setCompany(Company aCompany) {
        company = aCompany;
    }

    /**
     * Access method for departments.
     *
     * @return the current value of departments
     */
    public Set<Departments> getDepartments() {
        return departments;
    }

    /**
     * Setter method for departments.
     *
     * @param aDepartments the new value for departments
     */
    public void setDepartments(Set<Departments> aDepartments) {
        departments = aDepartments;
    }

    /**
     * Compares the key for this instance with another Businessunit.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Businessunit and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Businessunit)) {
            return false;
        }
        Businessunit that = (Businessunit) other;
        Object myId = this.getId();
        Object yourId = that.getId();
        if (myId==null ? yourId!=null : !myId.equals(yourId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Businessunit.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Businessunit)) return false;
        return this.equalKeys(other) && ((Businessunit)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getId() == null) {
            i = 0;
        } else {
            i = getId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Businessunit |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", getId());
        return ret;
    }

}
