// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity(name="budg_cloudbudgetactuals")
public class BudgCloudbudgetactuals implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="mstr_date_id", precision=10)
    private int mstrDateId;
    @Column(precision=17, scale=9)
    private BigDecimal actualamount;
    @ManyToOne
    @JoinColumn(name="budg_cloudbudgetdetail_id")
    private BudgCloudbudgetdetails budgCloudbudgetdetails;

    /** Default constructor. */
    public BudgCloudbudgetactuals() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for mstrDateId.
     *
     * @return the current value of mstrDateId
     */
    public int getMstrDateId() {
        return mstrDateId;
    }

    /**
     * Setter method for mstrDateId.
     *
     * @param aMstrDateId the new value for mstrDateId
     */
    public void setMstrDateId(int aMstrDateId) {
        mstrDateId = aMstrDateId;
    }

    /**
     * Access method for actualamount.
     *
     * @return the current value of actualamount
     */
    public BigDecimal getActualamount() {
        return actualamount;
    }

    /**
     * Setter method for actualamount.
     *
     * @param aActualamount the new value for actualamount
     */
    public void setActualamount(BigDecimal aActualamount) {
        actualamount = aActualamount;
    }

    /**
     * Access method for budgCloudbudgetdetails.
     *
     * @return the current value of budgCloudbudgetdetails
     */
    public BudgCloudbudgetdetails getBudgCloudbudgetdetails() {
        return budgCloudbudgetdetails;
    }

    /**
     * Setter method for budgCloudbudgetdetails.
     *
     * @param aBudgCloudbudgetdetails the new value for budgCloudbudgetdetails
     */
    public void setBudgCloudbudgetdetails(BudgCloudbudgetdetails aBudgCloudbudgetdetails) {
        budgCloudbudgetdetails = aBudgCloudbudgetdetails;
    }

    /**
     * Compares the key for this instance with another BudgCloudbudgetactuals.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class BudgCloudbudgetactuals and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof BudgCloudbudgetactuals)) {
            return false;
        }
        BudgCloudbudgetactuals that = (BudgCloudbudgetactuals) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another BudgCloudbudgetactuals.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BudgCloudbudgetactuals)) return false;
        return this.equalKeys(other) && ((BudgCloudbudgetactuals)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BudgCloudbudgetactuals |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
