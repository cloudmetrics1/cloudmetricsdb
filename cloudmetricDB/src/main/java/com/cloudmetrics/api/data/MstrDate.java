// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="mstr_date")
public class MstrDate implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="date_actual", nullable=false)
    private Date dateActual;
    @Column(nullable=false, precision=19)
    private long epoch;
    @Column(name="day_suffix", nullable=false, length=4)
    private String daySuffix;
    @Column(name="day_name", nullable=false, length=9)
    private String dayName;
    @Column(name="day_of_week", nullable=false, precision=10)
    private int dayOfWeek;
    @Column(name="day_of_month", nullable=false, precision=10)
    private int dayOfMonth;
    @Column(name="day_of_quarter", nullable=false, precision=10)
    private int dayOfQuarter;
    @Column(name="day_of_year", nullable=false, precision=10)
    private int dayOfYear;
    @Column(name="week_of_month", nullable=false, precision=10)
    private int weekOfMonth;
    @Column(name="week_of_year", nullable=false, precision=10)
    private int weekOfYear;
    @Column(name="week_of_year_iso", nullable=false, length=10)
    private String weekOfYearIso;
    @Column(name="month_actual", nullable=false, precision=10)
    private int monthActual;
    @Column(name="month_name", nullable=false, length=9)
    private String monthName;
    @Column(name="month_name_abbreviated", nullable=false, length=3)
    private String monthNameAbbreviated;
    @Column(name="quarter_actual", nullable=false, precision=10)
    private int quarterActual;
    @Column(name="quarter_name", nullable=false, length=9)
    private String quarterName;
    @Column(name="year_actual", nullable=false, precision=10)
    private int yearActual;
    @Column(name="first_day_of_week", nullable=false)
    private Date firstDayOfWeek;
    @Column(name="last_day_of_week", nullable=false)
    private Date lastDayOfWeek;
    @Column(name="first_day_of_month", nullable=false)
    private Date firstDayOfMonth;
    @Column(name="last_day_of_month", nullable=false)
    private Date lastDayOfMonth;
    @Column(name="first_day_of_quarter", nullable=false)
    private Date firstDayOfQuarter;
    @Column(name="last_day_of_quarter", nullable=false)
    private Date lastDayOfQuarter;
    @Column(name="first_day_of_year", nullable=false)
    private Date firstDayOfYear;
    @Column(name="last_day_of_year", nullable=false)
    private Date lastDayOfYear;
    @Column(nullable=false, length=6)
    private String mmyyyy;
    @Column(nullable=false, length=10)
    private String mmddyyyy;
    @Column(name="weekend_indr", nullable=false, length=1)
    private boolean weekendIndr;

    /** Default constructor. */
    public MstrDate() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for dateActual.
     *
     * @return the current value of dateActual
     */
    public Date getDateActual() {
        return dateActual;
    }

    /**
     * Setter method for dateActual.
     *
     * @param aDateActual the new value for dateActual
     */
    public void setDateActual(Date aDateActual) {
        dateActual = aDateActual;
    }

    /**
     * Access method for epoch.
     *
     * @return the current value of epoch
     */
    public long getEpoch() {
        return epoch;
    }

    /**
     * Setter method for epoch.
     *
     * @param aEpoch the new value for epoch
     */
    public void setEpoch(long aEpoch) {
        epoch = aEpoch;
    }

    /**
     * Access method for daySuffix.
     *
     * @return the current value of daySuffix
     */
    public String getDaySuffix() {
        return daySuffix;
    }

    /**
     * Setter method for daySuffix.
     *
     * @param aDaySuffix the new value for daySuffix
     */
    public void setDaySuffix(String aDaySuffix) {
        daySuffix = aDaySuffix;
    }

    /**
     * Access method for dayName.
     *
     * @return the current value of dayName
     */
    public String getDayName() {
        return dayName;
    }

    /**
     * Setter method for dayName.
     *
     * @param aDayName the new value for dayName
     */
    public void setDayName(String aDayName) {
        dayName = aDayName;
    }

    /**
     * Access method for dayOfWeek.
     *
     * @return the current value of dayOfWeek
     */
    public int getDayOfWeek() {
        return dayOfWeek;
    }

    /**
     * Setter method for dayOfWeek.
     *
     * @param aDayOfWeek the new value for dayOfWeek
     */
    public void setDayOfWeek(int aDayOfWeek) {
        dayOfWeek = aDayOfWeek;
    }

    /**
     * Access method for dayOfMonth.
     *
     * @return the current value of dayOfMonth
     */
    public int getDayOfMonth() {
        return dayOfMonth;
    }

    /**
     * Setter method for dayOfMonth.
     *
     * @param aDayOfMonth the new value for dayOfMonth
     */
    public void setDayOfMonth(int aDayOfMonth) {
        dayOfMonth = aDayOfMonth;
    }

    /**
     * Access method for dayOfQuarter.
     *
     * @return the current value of dayOfQuarter
     */
    public int getDayOfQuarter() {
        return dayOfQuarter;
    }

    /**
     * Setter method for dayOfQuarter.
     *
     * @param aDayOfQuarter the new value for dayOfQuarter
     */
    public void setDayOfQuarter(int aDayOfQuarter) {
        dayOfQuarter = aDayOfQuarter;
    }

    /**
     * Access method for dayOfYear.
     *
     * @return the current value of dayOfYear
     */
    public int getDayOfYear() {
        return dayOfYear;
    }

    /**
     * Setter method for dayOfYear.
     *
     * @param aDayOfYear the new value for dayOfYear
     */
    public void setDayOfYear(int aDayOfYear) {
        dayOfYear = aDayOfYear;
    }

    /**
     * Access method for weekOfMonth.
     *
     * @return the current value of weekOfMonth
     */
    public int getWeekOfMonth() {
        return weekOfMonth;
    }

    /**
     * Setter method for weekOfMonth.
     *
     * @param aWeekOfMonth the new value for weekOfMonth
     */
    public void setWeekOfMonth(int aWeekOfMonth) {
        weekOfMonth = aWeekOfMonth;
    }

    /**
     * Access method for weekOfYear.
     *
     * @return the current value of weekOfYear
     */
    public int getWeekOfYear() {
        return weekOfYear;
    }

    /**
     * Setter method for weekOfYear.
     *
     * @param aWeekOfYear the new value for weekOfYear
     */
    public void setWeekOfYear(int aWeekOfYear) {
        weekOfYear = aWeekOfYear;
    }

    /**
     * Access method for weekOfYearIso.
     *
     * @return the current value of weekOfYearIso
     */
    public String getWeekOfYearIso() {
        return weekOfYearIso;
    }

    /**
     * Setter method for weekOfYearIso.
     *
     * @param aWeekOfYearIso the new value for weekOfYearIso
     */
    public void setWeekOfYearIso(String aWeekOfYearIso) {
        weekOfYearIso = aWeekOfYearIso;
    }

    /**
     * Access method for monthActual.
     *
     * @return the current value of monthActual
     */
    public int getMonthActual() {
        return monthActual;
    }

    /**
     * Setter method for monthActual.
     *
     * @param aMonthActual the new value for monthActual
     */
    public void setMonthActual(int aMonthActual) {
        monthActual = aMonthActual;
    }

    /**
     * Access method for monthName.
     *
     * @return the current value of monthName
     */
    public String getMonthName() {
        return monthName;
    }

    /**
     * Setter method for monthName.
     *
     * @param aMonthName the new value for monthName
     */
    public void setMonthName(String aMonthName) {
        monthName = aMonthName;
    }

    /**
     * Access method for monthNameAbbreviated.
     *
     * @return the current value of monthNameAbbreviated
     */
    public String getMonthNameAbbreviated() {
        return monthNameAbbreviated;
    }

    /**
     * Setter method for monthNameAbbreviated.
     *
     * @param aMonthNameAbbreviated the new value for monthNameAbbreviated
     */
    public void setMonthNameAbbreviated(String aMonthNameAbbreviated) {
        monthNameAbbreviated = aMonthNameAbbreviated;
    }

    /**
     * Access method for quarterActual.
     *
     * @return the current value of quarterActual
     */
    public int getQuarterActual() {
        return quarterActual;
    }

    /**
     * Setter method for quarterActual.
     *
     * @param aQuarterActual the new value for quarterActual
     */
    public void setQuarterActual(int aQuarterActual) {
        quarterActual = aQuarterActual;
    }

    /**
     * Access method for quarterName.
     *
     * @return the current value of quarterName
     */
    public String getQuarterName() {
        return quarterName;
    }

    /**
     * Setter method for quarterName.
     *
     * @param aQuarterName the new value for quarterName
     */
    public void setQuarterName(String aQuarterName) {
        quarterName = aQuarterName;
    }

    /**
     * Access method for yearActual.
     *
     * @return the current value of yearActual
     */
    public int getYearActual() {
        return yearActual;
    }

    /**
     * Setter method for yearActual.
     *
     * @param aYearActual the new value for yearActual
     */
    public void setYearActual(int aYearActual) {
        yearActual = aYearActual;
    }

    /**
     * Access method for firstDayOfWeek.
     *
     * @return the current value of firstDayOfWeek
     */
    public Date getFirstDayOfWeek() {
        return firstDayOfWeek;
    }

    /**
     * Setter method for firstDayOfWeek.
     *
     * @param aFirstDayOfWeek the new value for firstDayOfWeek
     */
    public void setFirstDayOfWeek(Date aFirstDayOfWeek) {
        firstDayOfWeek = aFirstDayOfWeek;
    }

    /**
     * Access method for lastDayOfWeek.
     *
     * @return the current value of lastDayOfWeek
     */
    public Date getLastDayOfWeek() {
        return lastDayOfWeek;
    }

    /**
     * Setter method for lastDayOfWeek.
     *
     * @param aLastDayOfWeek the new value for lastDayOfWeek
     */
    public void setLastDayOfWeek(Date aLastDayOfWeek) {
        lastDayOfWeek = aLastDayOfWeek;
    }

    /**
     * Access method for firstDayOfMonth.
     *
     * @return the current value of firstDayOfMonth
     */
    public Date getFirstDayOfMonth() {
        return firstDayOfMonth;
    }

    /**
     * Setter method for firstDayOfMonth.
     *
     * @param aFirstDayOfMonth the new value for firstDayOfMonth
     */
    public void setFirstDayOfMonth(Date aFirstDayOfMonth) {
        firstDayOfMonth = aFirstDayOfMonth;
    }

    /**
     * Access method for lastDayOfMonth.
     *
     * @return the current value of lastDayOfMonth
     */
    public Date getLastDayOfMonth() {
        return lastDayOfMonth;
    }

    /**
     * Setter method for lastDayOfMonth.
     *
     * @param aLastDayOfMonth the new value for lastDayOfMonth
     */
    public void setLastDayOfMonth(Date aLastDayOfMonth) {
        lastDayOfMonth = aLastDayOfMonth;
    }

    /**
     * Access method for firstDayOfQuarter.
     *
     * @return the current value of firstDayOfQuarter
     */
    public Date getFirstDayOfQuarter() {
        return firstDayOfQuarter;
    }

    /**
     * Setter method for firstDayOfQuarter.
     *
     * @param aFirstDayOfQuarter the new value for firstDayOfQuarter
     */
    public void setFirstDayOfQuarter(Date aFirstDayOfQuarter) {
        firstDayOfQuarter = aFirstDayOfQuarter;
    }

    /**
     * Access method for lastDayOfQuarter.
     *
     * @return the current value of lastDayOfQuarter
     */
    public Date getLastDayOfQuarter() {
        return lastDayOfQuarter;
    }

    /**
     * Setter method for lastDayOfQuarter.
     *
     * @param aLastDayOfQuarter the new value for lastDayOfQuarter
     */
    public void setLastDayOfQuarter(Date aLastDayOfQuarter) {
        lastDayOfQuarter = aLastDayOfQuarter;
    }

    /**
     * Access method for firstDayOfYear.
     *
     * @return the current value of firstDayOfYear
     */
    public Date getFirstDayOfYear() {
        return firstDayOfYear;
    }

    /**
     * Setter method for firstDayOfYear.
     *
     * @param aFirstDayOfYear the new value for firstDayOfYear
     */
    public void setFirstDayOfYear(Date aFirstDayOfYear) {
        firstDayOfYear = aFirstDayOfYear;
    }

    /**
     * Access method for lastDayOfYear.
     *
     * @return the current value of lastDayOfYear
     */
    public Date getLastDayOfYear() {
        return lastDayOfYear;
    }

    /**
     * Setter method for lastDayOfYear.
     *
     * @param aLastDayOfYear the new value for lastDayOfYear
     */
    public void setLastDayOfYear(Date aLastDayOfYear) {
        lastDayOfYear = aLastDayOfYear;
    }

    /**
     * Access method for mmyyyy.
     *
     * @return the current value of mmyyyy
     */
    public String getMmyyyy() {
        return mmyyyy;
    }

    /**
     * Setter method for mmyyyy.
     *
     * @param aMmyyyy the new value for mmyyyy
     */
    public void setMmyyyy(String aMmyyyy) {
        mmyyyy = aMmyyyy;
    }

    /**
     * Access method for mmddyyyy.
     *
     * @return the current value of mmddyyyy
     */
    public String getMmddyyyy() {
        return mmddyyyy;
    }

    /**
     * Setter method for mmddyyyy.
     *
     * @param aMmddyyyy the new value for mmddyyyy
     */
    public void setMmddyyyy(String aMmddyyyy) {
        mmddyyyy = aMmddyyyy;
    }

    /**
     * Access method for weekendIndr.
     *
     * @return true if and only if weekendIndr is currently true
     */
    public boolean getWeekendIndr() {
        return weekendIndr;
    }

    /**
     * Setter method for weekendIndr.
     *
     * @param aWeekendIndr the new value for weekendIndr
     */
    public void setWeekendIndr(boolean aWeekendIndr) {
        weekendIndr = aWeekendIndr;
    }

    /**
     * Compares the key for this instance with another MstrDate.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MstrDate and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MstrDate)) {
            return false;
        }
        MstrDate that = (MstrDate) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MstrDate.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MstrDate)) return false;
        return this.equalKeys(other) && ((MstrDate)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MstrDate |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
