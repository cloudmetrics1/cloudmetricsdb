// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="costcenters")
public class Costcenters implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(unique=true, nullable=false, precision=131089)
    private BigDecimal id;
    private String name;
    private String description;
    private String accountnumber;
    private String code;
    @Column(precision=131089)
    private BigDecimal parentid;
    @Column(length=1)
    private boolean active;
    private Timestamp validfrom;
    private Timestamp validto;
    @OneToMany(mappedBy="costcenters")
    private Set<BudgCloudbudgets> budgCloudbudgets;

    /** Default constructor. */
    public Costcenters() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(BigDecimal aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for description.
     *
     * @return the current value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter method for description.
     *
     * @param aDescription the new value for description
     */
    public void setDescription(String aDescription) {
        description = aDescription;
    }

    /**
     * Access method for accountnumber.
     *
     * @return the current value of accountnumber
     */
    public String getAccountnumber() {
        return accountnumber;
    }

    /**
     * Setter method for accountnumber.
     *
     * @param aAccountnumber the new value for accountnumber
     */
    public void setAccountnumber(String aAccountnumber) {
        accountnumber = aAccountnumber;
    }

    /**
     * Access method for code.
     *
     * @return the current value of code
     */
    public String getCode() {
        return code;
    }

    /**
     * Setter method for code.
     *
     * @param aCode the new value for code
     */
    public void setCode(String aCode) {
        code = aCode;
    }

    /**
     * Access method for parentid.
     *
     * @return the current value of parentid
     */
    public BigDecimal getParentid() {
        return parentid;
    }

    /**
     * Setter method for parentid.
     *
     * @param aParentid the new value for parentid
     */
    public void setParentid(BigDecimal aParentid) {
        parentid = aParentid;
    }

    /**
     * Access method for active.
     *
     * @return true if and only if active is currently true
     */
    public boolean getActive() {
        return active;
    }

    /**
     * Setter method for active.
     *
     * @param aActive the new value for active
     */
    public void setActive(boolean aActive) {
        active = aActive;
    }

    /**
     * Access method for validfrom.
     *
     * @return the current value of validfrom
     */
    public Timestamp getValidfrom() {
        return validfrom;
    }

    /**
     * Setter method for validfrom.
     *
     * @param aValidfrom the new value for validfrom
     */
    public void setValidfrom(Timestamp aValidfrom) {
        validfrom = aValidfrom;
    }

    /**
     * Access method for validto.
     *
     * @return the current value of validto
     */
    public Timestamp getValidto() {
        return validto;
    }

    /**
     * Setter method for validto.
     *
     * @param aValidto the new value for validto
     */
    public void setValidto(Timestamp aValidto) {
        validto = aValidto;
    }

    /**
     * Access method for budgCloudbudgets.
     *
     * @return the current value of budgCloudbudgets
     */
    public Set<BudgCloudbudgets> getBudgCloudbudgets() {
        return budgCloudbudgets;
    }

    /**
     * Setter method for budgCloudbudgets.
     *
     * @param aBudgCloudbudgets the new value for budgCloudbudgets
     */
    public void setBudgCloudbudgets(Set<BudgCloudbudgets> aBudgCloudbudgets) {
        budgCloudbudgets = aBudgCloudbudgets;
    }

    /**
     * Compares the key for this instance with another Costcenters.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Costcenters and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Costcenters)) {
            return false;
        }
        Costcenters that = (Costcenters) other;
        Object myId = this.getId();
        Object yourId = that.getId();
        if (myId==null ? yourId!=null : !myId.equals(yourId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Costcenters.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Costcenters)) return false;
        return this.equalKeys(other) && ((Costcenters)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getId() == null) {
            i = 0;
        } else {
            i = getId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Costcenters |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", getId());
        return ret;
    }

}
