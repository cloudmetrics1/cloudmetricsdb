// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity
public class AspNetUsers implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(name="Id", unique=true, nullable=false)
    private String id;
    @Column(name="UserName", nullable=false)
    private String userName;
    @Column(name="PasswordHash")
    private String passwordHash;
    @Column(name="SecurityStamp")
    private String securityStamp;
    @Column(name="Email")
    private String email;
    @Column(name="EmailConfirmed", nullable=false, length=1)
    private boolean emailConfirmed;
    @Column(name="PhoneNumber")
    private String phoneNumber;
    @Column(name="PhoneNumberConfirmed", nullable=false, length=1)
    private boolean phoneNumberConfirmed;
    @Column(name="TwoFactorEnabled", nullable=false, length=1)
    private boolean twoFactorEnabled;
    @Column(name="LockoutEndDateUtc")
    private Timestamp lockoutEndDateUtc;
    @Column(name="LockoutEnabled", nullable=false, length=1)
    private boolean lockoutEnabled;
    @Column(name="AccessFailedCount", nullable=false, precision=10)
    private int accessFailedCount;
    @Column(name="ConcurrencyStamp")
    private String concurrencyStamp;
    @OneToMany(mappedBy="aspNetUsers")
    private Set<AspNetUserClaims> aspNetUserClaims;
    @OneToMany(mappedBy="aspNetUsers")
    private Set<AspNetUserLogins> aspNetUserLogins;
    @OneToMany(mappedBy="aspNetUsers")
    private Set<AspNetUserRoles> aspNetUserRoles;

    /** Default constructor. */
    public AspNetUsers() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public String getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(String aId) {
        id = aId;
    }

    /**
     * Access method for userName.
     *
     * @return the current value of userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Setter method for userName.
     *
     * @param aUserName the new value for userName
     */
    public void setUserName(String aUserName) {
        userName = aUserName;
    }

    /**
     * Access method for passwordHash.
     *
     * @return the current value of passwordHash
     */
    public String getPasswordHash() {
        return passwordHash;
    }

    /**
     * Setter method for passwordHash.
     *
     * @param aPasswordHash the new value for passwordHash
     */
    public void setPasswordHash(String aPasswordHash) {
        passwordHash = aPasswordHash;
    }

    /**
     * Access method for securityStamp.
     *
     * @return the current value of securityStamp
     */
    public String getSecurityStamp() {
        return securityStamp;
    }

    /**
     * Setter method for securityStamp.
     *
     * @param aSecurityStamp the new value for securityStamp
     */
    public void setSecurityStamp(String aSecurityStamp) {
        securityStamp = aSecurityStamp;
    }

    /**
     * Access method for email.
     *
     * @return the current value of email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter method for email.
     *
     * @param aEmail the new value for email
     */
    public void setEmail(String aEmail) {
        email = aEmail;
    }

    /**
     * Access method for emailConfirmed.
     *
     * @return true if and only if emailConfirmed is currently true
     */
    public boolean getEmailConfirmed() {
        return emailConfirmed;
    }

    /**
     * Setter method for emailConfirmed.
     *
     * @param aEmailConfirmed the new value for emailConfirmed
     */
    public void setEmailConfirmed(boolean aEmailConfirmed) {
        emailConfirmed = aEmailConfirmed;
    }

    /**
     * Access method for phoneNumber.
     *
     * @return the current value of phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Setter method for phoneNumber.
     *
     * @param aPhoneNumber the new value for phoneNumber
     */
    public void setPhoneNumber(String aPhoneNumber) {
        phoneNumber = aPhoneNumber;
    }

    /**
     * Access method for phoneNumberConfirmed.
     *
     * @return true if and only if phoneNumberConfirmed is currently true
     */
    public boolean getPhoneNumberConfirmed() {
        return phoneNumberConfirmed;
    }

    /**
     * Setter method for phoneNumberConfirmed.
     *
     * @param aPhoneNumberConfirmed the new value for phoneNumberConfirmed
     */
    public void setPhoneNumberConfirmed(boolean aPhoneNumberConfirmed) {
        phoneNumberConfirmed = aPhoneNumberConfirmed;
    }

    /**
     * Access method for twoFactorEnabled.
     *
     * @return true if and only if twoFactorEnabled is currently true
     */
    public boolean getTwoFactorEnabled() {
        return twoFactorEnabled;
    }

    /**
     * Setter method for twoFactorEnabled.
     *
     * @param aTwoFactorEnabled the new value for twoFactorEnabled
     */
    public void setTwoFactorEnabled(boolean aTwoFactorEnabled) {
        twoFactorEnabled = aTwoFactorEnabled;
    }

    /**
     * Access method for lockoutEndDateUtc.
     *
     * @return the current value of lockoutEndDateUtc
     */
    public Timestamp getLockoutEndDateUtc() {
        return lockoutEndDateUtc;
    }

    /**
     * Setter method for lockoutEndDateUtc.
     *
     * @param aLockoutEndDateUtc the new value for lockoutEndDateUtc
     */
    public void setLockoutEndDateUtc(Timestamp aLockoutEndDateUtc) {
        lockoutEndDateUtc = aLockoutEndDateUtc;
    }

    /**
     * Access method for lockoutEnabled.
     *
     * @return true if and only if lockoutEnabled is currently true
     */
    public boolean getLockoutEnabled() {
        return lockoutEnabled;
    }

    /**
     * Setter method for lockoutEnabled.
     *
     * @param aLockoutEnabled the new value for lockoutEnabled
     */
    public void setLockoutEnabled(boolean aLockoutEnabled) {
        lockoutEnabled = aLockoutEnabled;
    }

    /**
     * Access method for accessFailedCount.
     *
     * @return the current value of accessFailedCount
     */
    public int getAccessFailedCount() {
        return accessFailedCount;
    }

    /**
     * Setter method for accessFailedCount.
     *
     * @param aAccessFailedCount the new value for accessFailedCount
     */
    public void setAccessFailedCount(int aAccessFailedCount) {
        accessFailedCount = aAccessFailedCount;
    }

    /**
     * Access method for concurrencyStamp.
     *
     * @return the current value of concurrencyStamp
     */
    public String getConcurrencyStamp() {
        return concurrencyStamp;
    }

    /**
     * Setter method for concurrencyStamp.
     *
     * @param aConcurrencyStamp the new value for concurrencyStamp
     */
    public void setConcurrencyStamp(String aConcurrencyStamp) {
        concurrencyStamp = aConcurrencyStamp;
    }

    /**
     * Access method for aspNetUserClaims.
     *
     * @return the current value of aspNetUserClaims
     */
    public Set<AspNetUserClaims> getAspNetUserClaims() {
        return aspNetUserClaims;
    }

    /**
     * Setter method for aspNetUserClaims.
     *
     * @param aAspNetUserClaims the new value for aspNetUserClaims
     */
    public void setAspNetUserClaims(Set<AspNetUserClaims> aAspNetUserClaims) {
        aspNetUserClaims = aAspNetUserClaims;
    }

    /**
     * Access method for aspNetUserLogins.
     *
     * @return the current value of aspNetUserLogins
     */
    public Set<AspNetUserLogins> getAspNetUserLogins() {
        return aspNetUserLogins;
    }

    /**
     * Setter method for aspNetUserLogins.
     *
     * @param aAspNetUserLogins the new value for aspNetUserLogins
     */
    public void setAspNetUserLogins(Set<AspNetUserLogins> aAspNetUserLogins) {
        aspNetUserLogins = aAspNetUserLogins;
    }

    /**
     * Access method for aspNetUserRoles.
     *
     * @return the current value of aspNetUserRoles
     */
    public Set<AspNetUserRoles> getAspNetUserRoles() {
        return aspNetUserRoles;
    }

    /**
     * Setter method for aspNetUserRoles.
     *
     * @param aAspNetUserRoles the new value for aspNetUserRoles
     */
    public void setAspNetUserRoles(Set<AspNetUserRoles> aAspNetUserRoles) {
        aspNetUserRoles = aAspNetUserRoles;
    }

    /**
     * Compares the key for this instance with another AspNetUsers.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class AspNetUsers and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof AspNetUsers)) {
            return false;
        }
        AspNetUsers that = (AspNetUsers) other;
        Object myId = this.getId();
        Object yourId = that.getId();
        if (myId==null ? yourId!=null : !myId.equals(yourId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another AspNetUsers.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof AspNetUsers)) return false;
        return this.equalKeys(other) && ((AspNetUsers)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getId() == null) {
            i = 0;
        } else {
            i = getId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[AspNetUsers |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", getId());
        return ret;
    }

}
