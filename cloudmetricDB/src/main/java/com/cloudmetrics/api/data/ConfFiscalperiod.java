// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="conf_fiscalperiod")
public class ConfFiscalperiod implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    private String name;
    private Timestamp startdate;
    private Timestamp enddate;
    private String parentperiod;
    @Column(length=15)
    private String periodtype;
    @Column(length=1)
    private boolean openstatus;
    @Column(name="mstr_date_id", precision=10)
    private int mstrDateId;
    @OneToMany(mappedBy="confFiscalperiod")
    private Set<BudgCloudbudgets> budgCloudbudgets;

    /** Default constructor. */
    public ConfFiscalperiod() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for startdate.
     *
     * @return the current value of startdate
     */
    public Timestamp getStartdate() {
        return startdate;
    }

    /**
     * Setter method for startdate.
     *
     * @param aStartdate the new value for startdate
     */
    public void setStartdate(Timestamp aStartdate) {
        startdate = aStartdate;
    }

    /**
     * Access method for enddate.
     *
     * @return the current value of enddate
     */
    public Timestamp getEnddate() {
        return enddate;
    }

    /**
     * Setter method for enddate.
     *
     * @param aEnddate the new value for enddate
     */
    public void setEnddate(Timestamp aEnddate) {
        enddate = aEnddate;
    }

    /**
     * Access method for parentperiod.
     *
     * @return the current value of parentperiod
     */
    public String getParentperiod() {
        return parentperiod;
    }

    /**
     * Setter method for parentperiod.
     *
     * @param aParentperiod the new value for parentperiod
     */
    public void setParentperiod(String aParentperiod) {
        parentperiod = aParentperiod;
    }

    /**
     * Access method for periodtype.
     *
     * @return the current value of periodtype
     */
    public String getPeriodtype() {
        return periodtype;
    }

    /**
     * Setter method for periodtype.
     *
     * @param aPeriodtype the new value for periodtype
     */
    public void setPeriodtype(String aPeriodtype) {
        periodtype = aPeriodtype;
    }

    /**
     * Access method for openstatus.
     *
     * @return true if and only if openstatus is currently true
     */
    public boolean getOpenstatus() {
        return openstatus;
    }

    /**
     * Setter method for openstatus.
     *
     * @param aOpenstatus the new value for openstatus
     */
    public void setOpenstatus(boolean aOpenstatus) {
        openstatus = aOpenstatus;
    }

    /**
     * Access method for mstrDateId.
     *
     * @return the current value of mstrDateId
     */
    public int getMstrDateId() {
        return mstrDateId;
    }

    /**
     * Setter method for mstrDateId.
     *
     * @param aMstrDateId the new value for mstrDateId
     */
    public void setMstrDateId(int aMstrDateId) {
        mstrDateId = aMstrDateId;
    }

    /**
     * Access method for budgCloudbudgets.
     *
     * @return the current value of budgCloudbudgets
     */
    public Set<BudgCloudbudgets> getBudgCloudbudgets() {
        return budgCloudbudgets;
    }

    /**
     * Setter method for budgCloudbudgets.
     *
     * @param aBudgCloudbudgets the new value for budgCloudbudgets
     */
    public void setBudgCloudbudgets(Set<BudgCloudbudgets> aBudgCloudbudgets) {
        budgCloudbudgets = aBudgCloudbudgets;
    }

    /**
     * Compares the key for this instance with another ConfFiscalperiod.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class ConfFiscalperiod and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof ConfFiscalperiod)) {
            return false;
        }
        ConfFiscalperiod that = (ConfFiscalperiod) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another ConfFiscalperiod.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof ConfFiscalperiod)) return false;
        return this.equalKeys(other) && ((ConfFiscalperiod)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[ConfFiscalperiod |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
