// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="amazonaccinfo")
public class Amazonaccinfo implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    private String accesskey;
    private String secretkey;
    private String status;
    private Timestamp createdts;
    private Timestamp updatedts;
    private String region;

    /** Default constructor. */
    public Amazonaccinfo() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for accesskey.
     *
     * @return the current value of accesskey
     */
    public String getAccesskey() {
        return accesskey;
    }

    /**
     * Setter method for accesskey.
     *
     * @param aAccesskey the new value for accesskey
     */
    public void setAccesskey(String aAccesskey) {
        accesskey = aAccesskey;
    }

    /**
     * Access method for secretkey.
     *
     * @return the current value of secretkey
     */
    public String getSecretkey() {
        return secretkey;
    }

    /**
     * Setter method for secretkey.
     *
     * @param aSecretkey the new value for secretkey
     */
    public void setSecretkey(String aSecretkey) {
        secretkey = aSecretkey;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(String aStatus) {
        status = aStatus;
    }

    /**
     * Access method for createdts.
     *
     * @return the current value of createdts
     */
    public Timestamp getCreatedts() {
        return createdts;
    }

    /**
     * Setter method for createdts.
     *
     * @param aCreatedts the new value for createdts
     */
    public void setCreatedts(Timestamp aCreatedts) {
        createdts = aCreatedts;
    }

    /**
     * Access method for updatedts.
     *
     * @return the current value of updatedts
     */
    public Timestamp getUpdatedts() {
        return updatedts;
    }

    /**
     * Setter method for updatedts.
     *
     * @param aUpdatedts the new value for updatedts
     */
    public void setUpdatedts(Timestamp aUpdatedts) {
        updatedts = aUpdatedts;
    }

    /**
     * Access method for region.
     *
     * @return the current value of region
     */
    public String getRegion() {
        return region;
    }

    /**
     * Setter method for region.
     *
     * @param aRegion the new value for region
     */
    public void setRegion(String aRegion) {
        region = aRegion;
    }

    /**
     * Compares the key for this instance with another Amazonaccinfo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Amazonaccinfo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Amazonaccinfo)) {
            return false;
        }
        Amazonaccinfo that = (Amazonaccinfo) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Amazonaccinfo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Amazonaccinfo)) return false;
        return this.equalKeys(other) && ((Amazonaccinfo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Amazonaccinfo |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
