// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity
public class AspNetUserLogins implements Serializable {

    /** Primary key. */
    protected static final String PK = "AspNetUserLoginsAspNetUserLoginsPkey";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Column(name="LoginProvider", nullable=false)
    private String loginProvider;
    @Column(name="ProviderKey", nullable=false)
    private String providerKey;
    @Column(name="ProviderDisplayName")
    private String providerDisplayName;
    @ManyToOne(optional=false)
    @JoinColumn(name="UserId", nullable=false)
    private AspNetUsers aspNetUsers;

    /** Default constructor. */
    public AspNetUserLogins() {
        super();
    }

    /**
     * Access method for loginProvider.
     *
     * @return the current value of loginProvider
     */
    public String getLoginProvider() {
        return loginProvider;
    }

    /**
     * Setter method for loginProvider.
     *
     * @param aLoginProvider the new value for loginProvider
     */
    public void setLoginProvider(String aLoginProvider) {
        loginProvider = aLoginProvider;
    }

    /**
     * Access method for providerKey.
     *
     * @return the current value of providerKey
     */
    public String getProviderKey() {
        return providerKey;
    }

    /**
     * Setter method for providerKey.
     *
     * @param aProviderKey the new value for providerKey
     */
    public void setProviderKey(String aProviderKey) {
        providerKey = aProviderKey;
    }

    /**
     * Access method for providerDisplayName.
     *
     * @return the current value of providerDisplayName
     */
    public String getProviderDisplayName() {
        return providerDisplayName;
    }

    /**
     * Setter method for providerDisplayName.
     *
     * @param aProviderDisplayName the new value for providerDisplayName
     */
    public void setProviderDisplayName(String aProviderDisplayName) {
        providerDisplayName = aProviderDisplayName;
    }

    /**
     * Access method for aspNetUsers.
     *
     * @return the current value of aspNetUsers
     */
    public AspNetUsers getAspNetUsers() {
        return aspNetUsers;
    }

    /**
     * Setter method for aspNetUsers.
     *
     * @param aAspNetUsers the new value for aspNetUsers
     */
    public void setAspNetUsers(AspNetUsers aAspNetUsers) {
        aspNetUsers = aAspNetUsers;
    }

    /** Temporary value holder for group key fragment aspNetUsersId */
    private transient String tempAspNetUsersId;

    /**
     * Gets the key fragment id for member aspNetUsers.
     * If this.aspNetUsers is null, a temporary stored value for the key
     * fragment will be returned. The temporary value is set by setAspNetUsersId.
     * This behavior is required by some persistence libraries to allow
     * fetching of objects in arbitrary order.
     *
     * @return Current (or temporary) value of the key fragment
     * @see AspNetUsers
     */
    public String getAspNetUsersId() {
        return (getAspNetUsers() == null ? tempAspNetUsersId : getAspNetUsers().getId());
    }

    /**
     * Sets the key fragment id from member aspNetUsers.
     * If this.aspNetUsers is null, the passed value will be temporary
     * stored, and returned by subsequent calls to getAspNetUsersId.
     * This behaviour is required by some persistence libraries to allow
     * fetching of objects in arbitrary order.
     *
     * @param aId New value for the key fragment
     * @see AspNetUsers
     */
    public void setAspNetUsersId(String aId) {
        if (getAspNetUsers() == null) {
            tempAspNetUsersId = aId;
        } else {
            getAspNetUsers().setId(aId);
        }
    }

    /**
     * Compares the key for this instance with another AspNetUserLogins.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class AspNetUserLogins and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof AspNetUserLogins)) {
            return false;
        }
        AspNetUserLogins that = (AspNetUserLogins) other;
        Object myAspNetUsersId = this.getAspNetUsersId();
        Object yourAspNetUsersId = that.getAspNetUsersId();
        if (myAspNetUsersId==null ? yourAspNetUsersId!=null : !myAspNetUsersId.equals(yourAspNetUsersId)) {
            return false;
        }
        Object myLoginProvider = this.getLoginProvider();
        Object yourLoginProvider = that.getLoginProvider();
        if (myLoginProvider==null ? yourLoginProvider!=null : !myLoginProvider.equals(yourLoginProvider)) {
            return false;
        }
        Object myProviderKey = this.getProviderKey();
        Object yourProviderKey = that.getProviderKey();
        if (myProviderKey==null ? yourProviderKey!=null : !myProviderKey.equals(yourProviderKey)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another AspNetUserLogins.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof AspNetUserLogins)) return false;
        return this.equalKeys(other) && ((AspNetUserLogins)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getAspNetUsersId() == null) {
            i = 0;
        } else {
            i = getAspNetUsersId().hashCode();
        }
        result = 37*result + i;
        if (getLoginProvider() == null) {
            i = 0;
        } else {
            i = getLoginProvider().hashCode();
        }
        result = 37*result + i;
        if (getProviderKey() == null) {
            i = 0;
        } else {
            i = getProviderKey().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[AspNetUserLogins |");
        sb.append(" aspNetUsersId=").append(getAspNetUsersId());
        sb.append(" loginProvider=").append(getLoginProvider());
        sb.append(" providerKey=").append(getProviderKey());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("aspNetUsersId", getAspNetUsersId());
        ret.put("loginProvider", getLoginProvider());
        ret.put("providerKey", getProviderKey());
        return ret;
    }

}
