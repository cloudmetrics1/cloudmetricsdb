// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="mstr_amazonaccounts")
public class MstrAmazonaccounts implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    private String name;
    @Column(name="account_id", length=50)
    private String accountId;
    @Column(length=50)
    private String vendor;
    @Column(length=1)
    private boolean payeraccount;
    @Column(name="parent_id", precision=131089)
    private BigDecimal parentId;
    @Column(name="company_id", precision=131089)
    private BigDecimal companyId;
    @Column(length=1)
    private boolean active;
    private String accesskeyid;
    private String secretkeyid;
    private String region;

    /** Default constructor. */
    public MstrAmazonaccounts() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for accountId.
     *
     * @return the current value of accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Setter method for accountId.
     *
     * @param aAccountId the new value for accountId
     */
    public void setAccountId(String aAccountId) {
        accountId = aAccountId;
    }

    /**
     * Access method for vendor.
     *
     * @return the current value of vendor
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * Setter method for vendor.
     *
     * @param aVendor the new value for vendor
     */
    public void setVendor(String aVendor) {
        vendor = aVendor;
    }

    /**
     * Access method for payeraccount.
     *
     * @return true if and only if payeraccount is currently true
     */
    public boolean getPayeraccount() {
        return payeraccount;
    }

    /**
     * Setter method for payeraccount.
     *
     * @param aPayeraccount the new value for payeraccount
     */
    public void setPayeraccount(boolean aPayeraccount) {
        payeraccount = aPayeraccount;
    }

    /**
     * Access method for parentId.
     *
     * @return the current value of parentId
     */
    public BigDecimal getParentId() {
        return parentId;
    }

    /**
     * Setter method for parentId.
     *
     * @param aParentId the new value for parentId
     */
    public void setParentId(BigDecimal aParentId) {
        parentId = aParentId;
    }

    /**
     * Access method for companyId.
     *
     * @return the current value of companyId
     */
    public BigDecimal getCompanyId() {
        return companyId;
    }

    /**
     * Setter method for companyId.
     *
     * @param aCompanyId the new value for companyId
     */
    public void setCompanyId(BigDecimal aCompanyId) {
        companyId = aCompanyId;
    }

    /**
     * Access method for active.
     *
     * @return true if and only if active is currently true
     */
    public boolean getActive() {
        return active;
    }

    /**
     * Setter method for active.
     *
     * @param aActive the new value for active
     */
    public void setActive(boolean aActive) {
        active = aActive;
    }

    /**
     * Access method for accesskeyid.
     *
     * @return the current value of accesskeyid
     */
    public String getAccesskeyid() {
        return accesskeyid;
    }

    /**
     * Setter method for accesskeyid.
     *
     * @param aAccesskeyid the new value for accesskeyid
     */
    public void setAccesskeyid(String aAccesskeyid) {
        accesskeyid = aAccesskeyid;
    }

    /**
     * Access method for secretkeyid.
     *
     * @return the current value of secretkeyid
     */
    public String getSecretkeyid() {
        return secretkeyid;
    }

    /**
     * Setter method for secretkeyid.
     *
     * @param aSecretkeyid the new value for secretkeyid
     */
    public void setSecretkeyid(String aSecretkeyid) {
        secretkeyid = aSecretkeyid;
    }

    /**
     * Access method for region.
     *
     * @return the current value of region
     */
    public String getRegion() {
        return region;
    }

    /**
     * Setter method for region.
     *
     * @param aRegion the new value for region
     */
    public void setRegion(String aRegion) {
        region = aRegion;
    }

    /**
     * Compares the key for this instance with another MstrAmazonaccounts.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MstrAmazonaccounts and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MstrAmazonaccounts)) {
            return false;
        }
        MstrAmazonaccounts that = (MstrAmazonaccounts) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MstrAmazonaccounts.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MstrAmazonaccounts)) return false;
        return this.equalKeys(other) && ((MstrAmazonaccounts)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MstrAmazonaccounts |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
