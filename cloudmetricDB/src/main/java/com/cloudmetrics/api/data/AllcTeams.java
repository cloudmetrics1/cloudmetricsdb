// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity(name="allc_teams")
public class AllcTeams implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=19)
    private long id;
    @Column(name="blended_cost", precision=17, scale=9)
    private BigDecimal blendedCost;
    @ManyToOne(optional=false)
    @JoinColumn(name="rprt_aws_cost_actualsummary_id", nullable=false)
    private RprtAwsCostActualsummary rprtAwsCostActualsummary;
    @ManyToOne(optional=false)
    @JoinColumn(name="mstr_team_id", nullable=false)
    private MstrTeams mstrTeams;

    /** Default constructor. */
    public AllcTeams() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(long aId) {
        id = aId;
    }

    /**
     * Access method for blendedCost.
     *
     * @return the current value of blendedCost
     */
    public BigDecimal getBlendedCost() {
        return blendedCost;
    }

    /**
     * Setter method for blendedCost.
     *
     * @param aBlendedCost the new value for blendedCost
     */
    public void setBlendedCost(BigDecimal aBlendedCost) {
        blendedCost = aBlendedCost;
    }

    /**
     * Access method for rprtAwsCostActualsummary.
     *
     * @return the current value of rprtAwsCostActualsummary
     */
    public RprtAwsCostActualsummary getRprtAwsCostActualsummary() {
        return rprtAwsCostActualsummary;
    }

    /**
     * Setter method for rprtAwsCostActualsummary.
     *
     * @param aRprtAwsCostActualsummary the new value for rprtAwsCostActualsummary
     */
    public void setRprtAwsCostActualsummary(RprtAwsCostActualsummary aRprtAwsCostActualsummary) {
        rprtAwsCostActualsummary = aRprtAwsCostActualsummary;
    }

    /**
     * Access method for mstrTeams.
     *
     * @return the current value of mstrTeams
     */
    public MstrTeams getMstrTeams() {
        return mstrTeams;
    }

    /**
     * Setter method for mstrTeams.
     *
     * @param aMstrTeams the new value for mstrTeams
     */
    public void setMstrTeams(MstrTeams aMstrTeams) {
        mstrTeams = aMstrTeams;
    }

    /**
     * Compares the key for this instance with another AllcTeams.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class AllcTeams and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof AllcTeams)) {
            return false;
        }
        AllcTeams that = (AllcTeams) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another AllcTeams.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof AllcTeams)) return false;
        return this.equalKeys(other) && ((AllcTeams)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = (int)(getId() ^ (getId()>>>32));
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[AllcTeams |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Long.valueOf(getId()));
        return ret;
    }

}
