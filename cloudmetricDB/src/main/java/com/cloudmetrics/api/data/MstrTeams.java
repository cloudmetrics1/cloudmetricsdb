// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="mstr_teams")
public class MstrTeams implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    private String name;
    @Column(length=500)
    private String description;
    @Column(length=100)
    private String teamowner;
    @Column(length=1)
    private boolean active;
    @OneToMany(mappedBy="mstrTeams")
    private Set<AllcTeams> allcTeams;
    @OneToMany(mappedBy="mstrTeams")
    private Set<BudgCloudbudgetdetails> budgCloudbudgetdetails;

    /** Default constructor. */
    public MstrTeams() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for description.
     *
     * @return the current value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter method for description.
     *
     * @param aDescription the new value for description
     */
    public void setDescription(String aDescription) {
        description = aDescription;
    }

    /**
     * Access method for teamowner.
     *
     * @return the current value of teamowner
     */
    public String getTeamowner() {
        return teamowner;
    }

    /**
     * Setter method for teamowner.
     *
     * @param aTeamowner the new value for teamowner
     */
    public void setTeamowner(String aTeamowner) {
        teamowner = aTeamowner;
    }

    /**
     * Access method for active.
     *
     * @return true if and only if active is currently true
     */
    public boolean getActive() {
        return active;
    }

    /**
     * Setter method for active.
     *
     * @param aActive the new value for active
     */
    public void setActive(boolean aActive) {
        active = aActive;
    }

    /**
     * Access method for allcTeams.
     *
     * @return the current value of allcTeams
     */
    public Set<AllcTeams> getAllcTeams() {
        return allcTeams;
    }

    /**
     * Setter method for allcTeams.
     *
     * @param aAllcTeams the new value for allcTeams
     */
    public void setAllcTeams(Set<AllcTeams> aAllcTeams) {
        allcTeams = aAllcTeams;
    }

    /**
     * Access method for budgCloudbudgetdetails.
     *
     * @return the current value of budgCloudbudgetdetails
     */
    public Set<BudgCloudbudgetdetails> getBudgCloudbudgetdetails() {
        return budgCloudbudgetdetails;
    }

    /**
     * Setter method for budgCloudbudgetdetails.
     *
     * @param aBudgCloudbudgetdetails the new value for budgCloudbudgetdetails
     */
    public void setBudgCloudbudgetdetails(Set<BudgCloudbudgetdetails> aBudgCloudbudgetdetails) {
        budgCloudbudgetdetails = aBudgCloudbudgetdetails;
    }

    /**
     * Compares the key for this instance with another MstrTeams.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MstrTeams and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MstrTeams)) {
            return false;
        }
        MstrTeams that = (MstrTeams) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MstrTeams.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MstrTeams)) return false;
        return this.equalKeys(other) && ((MstrTeams)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MstrTeams |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
