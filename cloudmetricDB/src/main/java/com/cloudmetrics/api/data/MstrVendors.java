// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="mstr_vendors")
public class MstrVendors implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    private String name;
    private String vendortype;
    @Column(length=1)
    private boolean active;
    @Column(length=10)
    private String shortcode;
    private String referencecode;
    @OneToMany(mappedBy="mstrVendors")
    private Set<BudgCloudbudgetdetails> budgCloudbudgetdetails;
    @OneToMany(mappedBy="mstrVendors")
    private Set<MstrProducts> mstrProducts;

    /** Default constructor. */
    public MstrVendors() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for vendortype.
     *
     * @return the current value of vendortype
     */
    public String getVendortype() {
        return vendortype;
    }

    /**
     * Setter method for vendortype.
     *
     * @param aVendortype the new value for vendortype
     */
    public void setVendortype(String aVendortype) {
        vendortype = aVendortype;
    }

    /**
     * Access method for active.
     *
     * @return true if and only if active is currently true
     */
    public boolean getActive() {
        return active;
    }

    /**
     * Setter method for active.
     *
     * @param aActive the new value for active
     */
    public void setActive(boolean aActive) {
        active = aActive;
    }

    /**
     * Access method for shortcode.
     *
     * @return the current value of shortcode
     */
    public String getShortcode() {
        return shortcode;
    }

    /**
     * Setter method for shortcode.
     *
     * @param aShortcode the new value for shortcode
     */
    public void setShortcode(String aShortcode) {
        shortcode = aShortcode;
    }

    /**
     * Access method for referencecode.
     *
     * @return the current value of referencecode
     */
    public String getReferencecode() {
        return referencecode;
    }

    /**
     * Setter method for referencecode.
     *
     * @param aReferencecode the new value for referencecode
     */
    public void setReferencecode(String aReferencecode) {
        referencecode = aReferencecode;
    }

    /**
     * Access method for budgCloudbudgetdetails.
     *
     * @return the current value of budgCloudbudgetdetails
     */
    public Set<BudgCloudbudgetdetails> getBudgCloudbudgetdetails() {
        return budgCloudbudgetdetails;
    }

    /**
     * Setter method for budgCloudbudgetdetails.
     *
     * @param aBudgCloudbudgetdetails the new value for budgCloudbudgetdetails
     */
    public void setBudgCloudbudgetdetails(Set<BudgCloudbudgetdetails> aBudgCloudbudgetdetails) {
        budgCloudbudgetdetails = aBudgCloudbudgetdetails;
    }

    /**
     * Access method for mstrProducts.
     *
     * @return the current value of mstrProducts
     */
    public Set<MstrProducts> getMstrProducts() {
        return mstrProducts;
    }

    /**
     * Setter method for mstrProducts.
     *
     * @param aMstrProducts the new value for mstrProducts
     */
    public void setMstrProducts(Set<MstrProducts> aMstrProducts) {
        mstrProducts = aMstrProducts;
    }

    /**
     * Compares the key for this instance with another MstrVendors.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MstrVendors and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MstrVendors)) {
            return false;
        }
        MstrVendors that = (MstrVendors) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MstrVendors.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MstrVendors)) return false;
        return this.equalKeys(other) && ((MstrVendors)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MstrVendors |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
