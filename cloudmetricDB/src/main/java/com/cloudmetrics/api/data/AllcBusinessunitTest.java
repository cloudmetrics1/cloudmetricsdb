// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity(name="allc_businessunit_test")
public class AllcBusinessunitTest implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=19)
    private long id;
    @Column(name="blended_cost", precision=17, scale=9)
    private BigDecimal blendedCost;
    @Column(name="rprt_aws_cost_actualsummary_id", nullable=false, precision=10)
    private int rprtAwsCostActualsummaryId;
    @ManyToOne(optional=false)
    @JoinColumn(name="businessunit_id", nullable=false)
    private Businessunit businessunit;

    /** Default constructor. */
    public AllcBusinessunitTest() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(long aId) {
        id = aId;
    }

    /**
     * Access method for blendedCost.
     *
     * @return the current value of blendedCost
     */
    public BigDecimal getBlendedCost() {
        return blendedCost;
    }

    /**
     * Setter method for blendedCost.
     *
     * @param aBlendedCost the new value for blendedCost
     */
    public void setBlendedCost(BigDecimal aBlendedCost) {
        blendedCost = aBlendedCost;
    }

    /**
     * Access method for rprtAwsCostActualsummaryId.
     *
     * @return the current value of rprtAwsCostActualsummaryId
     */
    public int getRprtAwsCostActualsummaryId() {
        return rprtAwsCostActualsummaryId;
    }

    /**
     * Setter method for rprtAwsCostActualsummaryId.
     *
     * @param aRprtAwsCostActualsummaryId the new value for rprtAwsCostActualsummaryId
     */
    public void setRprtAwsCostActualsummaryId(int aRprtAwsCostActualsummaryId) {
        rprtAwsCostActualsummaryId = aRprtAwsCostActualsummaryId;
    }

    /**
     * Access method for businessunit.
     *
     * @return the current value of businessunit
     */
    public Businessunit getBusinessunit() {
        return businessunit;
    }

    /**
     * Setter method for businessunit.
     *
     * @param aBusinessunit the new value for businessunit
     */
    public void setBusinessunit(Businessunit aBusinessunit) {
        businessunit = aBusinessunit;
    }

    /**
     * Compares the key for this instance with another AllcBusinessunitTest.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class AllcBusinessunitTest and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof AllcBusinessunitTest)) {
            return false;
        }
        AllcBusinessunitTest that = (AllcBusinessunitTest) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another AllcBusinessunitTest.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof AllcBusinessunitTest)) return false;
        return this.equalKeys(other) && ((AllcBusinessunitTest)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = (int)(getId() ^ (getId()>>>32));
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[AllcBusinessunitTest |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Long.valueOf(getId()));
        return ret;
    }

}
