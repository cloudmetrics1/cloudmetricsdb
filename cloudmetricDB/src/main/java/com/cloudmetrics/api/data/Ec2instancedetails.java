// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="ec2instancedetails")
public class Ec2instancedetails implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    private String instanceid;
    private String instancetype;
    private String state;
    private String imageid;

    /** Default constructor. */
    public Ec2instancedetails() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for instanceid.
     *
     * @return the current value of instanceid
     */
    public String getInstanceid() {
        return instanceid;
    }

    /**
     * Setter method for instanceid.
     *
     * @param aInstanceid the new value for instanceid
     */
    public void setInstanceid(String aInstanceid) {
        instanceid = aInstanceid;
    }

    /**
     * Access method for instancetype.
     *
     * @return the current value of instancetype
     */
    public String getInstancetype() {
        return instancetype;
    }

    /**
     * Setter method for instancetype.
     *
     * @param aInstancetype the new value for instancetype
     */
    public void setInstancetype(String aInstancetype) {
        instancetype = aInstancetype;
    }

    /**
     * Access method for state.
     *
     * @return the current value of state
     */
    public String getState() {
        return state;
    }

    /**
     * Setter method for state.
     *
     * @param aState the new value for state
     */
    public void setState(String aState) {
        state = aState;
    }

    /**
     * Access method for imageid.
     *
     * @return the current value of imageid
     */
    public String getImageid() {
        return imageid;
    }

    /**
     * Setter method for imageid.
     *
     * @param aImageid the new value for imageid
     */
    public void setImageid(String aImageid) {
        imageid = aImageid;
    }

    /**
     * Compares the key for this instance with another Ec2instancedetails.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ec2instancedetails and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ec2instancedetails)) {
            return false;
        }
        Ec2instancedetails that = (Ec2instancedetails) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ec2instancedetails.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ec2instancedetails)) return false;
        return this.equalKeys(other) && ((Ec2instancedetails)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ec2instancedetails |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
