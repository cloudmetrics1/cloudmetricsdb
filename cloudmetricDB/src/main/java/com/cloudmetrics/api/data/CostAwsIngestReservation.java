// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="cost_aws_ingest_reservation", indexes={@Index(name="cost_aws_ingest_reservation_reservation_arn_IX", columnList="reservation_arn", unique=true)})
public class CostAwsIngestReservation implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="reservation_arn", unique=true, nullable=false)
    private String reservationArn;
    @Column(name="number_of_reservations", precision=10)
    private int numberOfReservations;
    @Column(name="units_per_reservation", precision=17, scale=9)
    private BigDecimal unitsPerReservation;
    @Column(name="start_time")
    private Timestamp startTime;
    @Column(name="end_time")
    private Timestamp endTime;
    @OneToMany(mappedBy="costAwsIngestReservation")
    private Set<CostAwsIngestActuallineitem> costAwsIngestActuallineitem;
    @OneToMany(mappedBy="costAwsIngestReservation")
    private Set<CostAwsIngestEstimatelineitem> costAwsIngestEstimatelineitem;
    @OneToMany(mappedBy="costAwsIngestReservation")
    private Set<CostAwsIngestEstimatelineitemTest> costAwsIngestEstimatelineitemTest;
    @OneToMany(mappedBy="costAwsIngestReservation")
    private Set<CostAwsIngestActuallineitemTest> costAwsIngestActuallineitemTest;

    /** Default constructor. */
    public CostAwsIngestReservation() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for reservationArn.
     *
     * @return the current value of reservationArn
     */
    public String getReservationArn() {
        return reservationArn;
    }

    /**
     * Setter method for reservationArn.
     *
     * @param aReservationArn the new value for reservationArn
     */
    public void setReservationArn(String aReservationArn) {
        reservationArn = aReservationArn;
    }

    /**
     * Access method for numberOfReservations.
     *
     * @return the current value of numberOfReservations
     */
    public int getNumberOfReservations() {
        return numberOfReservations;
    }

    /**
     * Setter method for numberOfReservations.
     *
     * @param aNumberOfReservations the new value for numberOfReservations
     */
    public void setNumberOfReservations(int aNumberOfReservations) {
        numberOfReservations = aNumberOfReservations;
    }

    /**
     * Access method for unitsPerReservation.
     *
     * @return the current value of unitsPerReservation
     */
    public BigDecimal getUnitsPerReservation() {
        return unitsPerReservation;
    }

    /**
     * Setter method for unitsPerReservation.
     *
     * @param aUnitsPerReservation the new value for unitsPerReservation
     */
    public void setUnitsPerReservation(BigDecimal aUnitsPerReservation) {
        unitsPerReservation = aUnitsPerReservation;
    }

    /**
     * Access method for startTime.
     *
     * @return the current value of startTime
     */
    public Timestamp getStartTime() {
        return startTime;
    }

    /**
     * Setter method for startTime.
     *
     * @param aStartTime the new value for startTime
     */
    public void setStartTime(Timestamp aStartTime) {
        startTime = aStartTime;
    }

    /**
     * Access method for endTime.
     *
     * @return the current value of endTime
     */
    public Timestamp getEndTime() {
        return endTime;
    }

    /**
     * Setter method for endTime.
     *
     * @param aEndTime the new value for endTime
     */
    public void setEndTime(Timestamp aEndTime) {
        endTime = aEndTime;
    }

    /**
     * Access method for costAwsIngestActuallineitem.
     *
     * @return the current value of costAwsIngestActuallineitem
     */
    public Set<CostAwsIngestActuallineitem> getCostAwsIngestActuallineitem() {
        return costAwsIngestActuallineitem;
    }

    /**
     * Setter method for costAwsIngestActuallineitem.
     *
     * @param aCostAwsIngestActuallineitem the new value for costAwsIngestActuallineitem
     */
    public void setCostAwsIngestActuallineitem(Set<CostAwsIngestActuallineitem> aCostAwsIngestActuallineitem) {
        costAwsIngestActuallineitem = aCostAwsIngestActuallineitem;
    }

    /**
     * Access method for costAwsIngestEstimatelineitem.
     *
     * @return the current value of costAwsIngestEstimatelineitem
     */
    public Set<CostAwsIngestEstimatelineitem> getCostAwsIngestEstimatelineitem() {
        return costAwsIngestEstimatelineitem;
    }

    /**
     * Setter method for costAwsIngestEstimatelineitem.
     *
     * @param aCostAwsIngestEstimatelineitem the new value for costAwsIngestEstimatelineitem
     */
    public void setCostAwsIngestEstimatelineitem(Set<CostAwsIngestEstimatelineitem> aCostAwsIngestEstimatelineitem) {
        costAwsIngestEstimatelineitem = aCostAwsIngestEstimatelineitem;
    }

    /**
     * Access method for costAwsIngestEstimatelineitemTest.
     *
     * @return the current value of costAwsIngestEstimatelineitemTest
     */
    public Set<CostAwsIngestEstimatelineitemTest> getCostAwsIngestEstimatelineitemTest() {
        return costAwsIngestEstimatelineitemTest;
    }

    /**
     * Setter method for costAwsIngestEstimatelineitemTest.
     *
     * @param aCostAwsIngestEstimatelineitemTest the new value for costAwsIngestEstimatelineitemTest
     */
    public void setCostAwsIngestEstimatelineitemTest(Set<CostAwsIngestEstimatelineitemTest> aCostAwsIngestEstimatelineitemTest) {
        costAwsIngestEstimatelineitemTest = aCostAwsIngestEstimatelineitemTest;
    }

    /**
     * Access method for costAwsIngestActuallineitemTest.
     *
     * @return the current value of costAwsIngestActuallineitemTest
     */
    public Set<CostAwsIngestActuallineitemTest> getCostAwsIngestActuallineitemTest() {
        return costAwsIngestActuallineitemTest;
    }

    /**
     * Setter method for costAwsIngestActuallineitemTest.
     *
     * @param aCostAwsIngestActuallineitemTest the new value for costAwsIngestActuallineitemTest
     */
    public void setCostAwsIngestActuallineitemTest(Set<CostAwsIngestActuallineitemTest> aCostAwsIngestActuallineitemTest) {
        costAwsIngestActuallineitemTest = aCostAwsIngestActuallineitemTest;
    }

    /**
     * Compares the key for this instance with another CostAwsIngestReservation.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class CostAwsIngestReservation and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof CostAwsIngestReservation)) {
            return false;
        }
        CostAwsIngestReservation that = (CostAwsIngestReservation) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another CostAwsIngestReservation.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CostAwsIngestReservation)) return false;
        return this.equalKeys(other) && ((CostAwsIngestReservation)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[CostAwsIngestReservation |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
