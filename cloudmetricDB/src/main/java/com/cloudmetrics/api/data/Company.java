// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="company")
public class Company implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private BigDecimal id;
    private String name;
    @Column(precision=131089)
    private BigDecimal parentid;
    @Column(length=1)
    private boolean customer;
    @Column(length=1)
    private boolean manufacturer;
    @Column(length=1)
    private boolean vendor;
    @Column(length=1)
    private boolean partner;
    @Column(name="date_created", nullable=false)
    private Timestamp dateCreated;
    @Column(nullable=false, precision=131089)
    private BigDecimal companyid;
    @OneToMany(mappedBy="company")
    private Set<Businessunit> businessunit;

    /** Default constructor. */
    public Company() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(BigDecimal aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for parentid.
     *
     * @return the current value of parentid
     */
    public BigDecimal getParentid() {
        return parentid;
    }

    /**
     * Setter method for parentid.
     *
     * @param aParentid the new value for parentid
     */
    public void setParentid(BigDecimal aParentid) {
        parentid = aParentid;
    }

    /**
     * Access method for customer.
     *
     * @return true if and only if customer is currently true
     */
    public boolean getCustomer() {
        return customer;
    }

    /**
     * Setter method for customer.
     *
     * @param aCustomer the new value for customer
     */
    public void setCustomer(boolean aCustomer) {
        customer = aCustomer;
    }

    /**
     * Access method for manufacturer.
     *
     * @return true if and only if manufacturer is currently true
     */
    public boolean getManufacturer() {
        return manufacturer;
    }

    /**
     * Setter method for manufacturer.
     *
     * @param aManufacturer the new value for manufacturer
     */
    public void setManufacturer(boolean aManufacturer) {
        manufacturer = aManufacturer;
    }

    /**
     * Access method for vendor.
     *
     * @return true if and only if vendor is currently true
     */
    public boolean getVendor() {
        return vendor;
    }

    /**
     * Setter method for vendor.
     *
     * @param aVendor the new value for vendor
     */
    public void setVendor(boolean aVendor) {
        vendor = aVendor;
    }

    /**
     * Access method for partner.
     *
     * @return true if and only if partner is currently true
     */
    public boolean getPartner() {
        return partner;
    }

    /**
     * Setter method for partner.
     *
     * @param aPartner the new value for partner
     */
    public void setPartner(boolean aPartner) {
        partner = aPartner;
    }

    /**
     * Access method for dateCreated.
     *
     * @return the current value of dateCreated
     */
    public Timestamp getDateCreated() {
        return dateCreated;
    }

    /**
     * Setter method for dateCreated.
     *
     * @param aDateCreated the new value for dateCreated
     */
    public void setDateCreated(Timestamp aDateCreated) {
        dateCreated = aDateCreated;
    }

    /**
     * Access method for companyid.
     *
     * @return the current value of companyid
     */
    public BigDecimal getCompanyid() {
        return companyid;
    }

    /**
     * Setter method for companyid.
     *
     * @param aCompanyid the new value for companyid
     */
    public void setCompanyid(BigDecimal aCompanyid) {
        companyid = aCompanyid;
    }

    /**
     * Access method for businessunit.
     *
     * @return the current value of businessunit
     */
    public Set<Businessunit> getBusinessunit() {
        return businessunit;
    }

    /**
     * Setter method for businessunit.
     *
     * @param aBusinessunit the new value for businessunit
     */
    public void setBusinessunit(Set<Businessunit> aBusinessunit) {
        businessunit = aBusinessunit;
    }

    /**
     * Compares the key for this instance with another Company.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Company and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Company)) {
            return false;
        }
        Company that = (Company) other;
        Object myId = this.getId();
        Object yourId = that.getId();
        if (myId==null ? yourId!=null : !myId.equals(yourId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Company.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Company)) return false;
        return this.equalKeys(other) && ((Company)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getId() == null) {
            i = 0;
        } else {
            i = getId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Company |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", getId());
        return ret;
    }

}
