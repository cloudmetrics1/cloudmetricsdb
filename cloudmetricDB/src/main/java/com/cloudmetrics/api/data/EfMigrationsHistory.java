// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Version;

@Entity(name="__EFMigrationsHistory")
public class EfMigrationsHistory implements Serializable {

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Column(name="MigrationId", nullable=false, length=150)
    private String migrationId;
    @Column(name="ProductVersion", nullable=false, length=32)
    private String productVersion;

    /** Default constructor. */
    public EfMigrationsHistory() {
        super();
    }

    /**
     * Access method for migrationId.
     *
     * @return the current value of migrationId
     */
    public String getMigrationId() {
        return migrationId;
    }

    /**
     * Setter method for migrationId.
     *
     * @param aMigrationId the new value for migrationId
     */
    public void setMigrationId(String aMigrationId) {
        migrationId = aMigrationId;
    }

    /**
     * Access method for productVersion.
     *
     * @return the current value of productVersion
     */
    public String getProductVersion() {
        return productVersion;
    }

    /**
     * Setter method for productVersion.
     *
     * @param aProductVersion the new value for productVersion
     */
    public void setProductVersion(String aProductVersion) {
        productVersion = aProductVersion;
    }

}
