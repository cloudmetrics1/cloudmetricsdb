// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="cost_aws_ingest_curdata", indexes={@Index(name="costAwsIngestCurdataCostAwsIngestCurdataBillTypePayerAccount6f101061Uniq", columnList="bill_type,payer_account_id,billing_period_start,provider_id", unique=true)})
public class CostAwsIngestCurdata implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(name="billing_resource", nullable=false, length=50)
    private String billingResource;
    @Column(name="bill_type", nullable=false, length=50)
    private String billType;
    @Column(name="payer_account_id", nullable=false, length=50)
    private String payerAccountId;
    @Column(name="billing_period_start", nullable=false)
    private Timestamp billingPeriodStart;
    @Column(name="billing_period_end", nullable=false)
    private Timestamp billingPeriodEnd;
    @Column(name="finalized_datetime")
    private Timestamp finalizedDatetime;
    @Column(name="summary_data_creation_datetime")
    private Timestamp summaryDataCreationDatetime;
    @Column(name="summary_data_updated_datetime")
    private Timestamp summaryDataUpdatedDatetime;
    @Column(name="provider_id", precision=10)
    private int providerId;
    @Column(name="derived_cost_datetime")
    private Timestamp derivedCostDatetime;
    @OneToMany(mappedBy="costAwsIngestCurdata")
    private Set<CostAwsIngestActuallineitem> costAwsIngestActuallineitem;
    @OneToMany(mappedBy="costAwsIngestCurdata")
    private Set<CostAwsIngestEstimatelineitem> costAwsIngestEstimatelineitem;
    @OneToMany(mappedBy="costAwsIngestCurdata")
    private Set<CostAwsIngestEstimatelineitemTest> costAwsIngestEstimatelineitemTest;
    @OneToMany(mappedBy="costAwsIngestCurdata")
    private Set<CostAwsIngestActuallineitemTest> costAwsIngestActuallineitemTest;
    @OneToMany(mappedBy="costAwsIngestCurdata")
    private Set<RprtAwsCostActualsummary> rprtAwsCostActualsummary;
    @OneToMany(mappedBy="costAwsIngestCurdata")
    private Set<RprtAwsCostActualsummaryTest> rprtAwsCostActualsummaryTest;
    @OneToMany(mappedBy="costAwsIngestCurdata")
    private Set<RprtAwsCostEstimatesummary> rprtAwsCostEstimatesummary;
    @OneToMany(mappedBy="costAwsIngestCurdata")
    private Set<RprtAwsCostEstimatesummaryTest> rprtAwsCostEstimatesummaryTest;

    /** Default constructor. */
    public CostAwsIngestCurdata() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for billingResource.
     *
     * @return the current value of billingResource
     */
    public String getBillingResource() {
        return billingResource;
    }

    /**
     * Setter method for billingResource.
     *
     * @param aBillingResource the new value for billingResource
     */
    public void setBillingResource(String aBillingResource) {
        billingResource = aBillingResource;
    }

    /**
     * Access method for billType.
     *
     * @return the current value of billType
     */
    public String getBillType() {
        return billType;
    }

    /**
     * Setter method for billType.
     *
     * @param aBillType the new value for billType
     */
    public void setBillType(String aBillType) {
        billType = aBillType;
    }

    /**
     * Access method for payerAccountId.
     *
     * @return the current value of payerAccountId
     */
    public String getPayerAccountId() {
        return payerAccountId;
    }

    /**
     * Setter method for payerAccountId.
     *
     * @param aPayerAccountId the new value for payerAccountId
     */
    public void setPayerAccountId(String aPayerAccountId) {
        payerAccountId = aPayerAccountId;
    }

    /**
     * Access method for billingPeriodStart.
     *
     * @return the current value of billingPeriodStart
     */
    public Timestamp getBillingPeriodStart() {
        return billingPeriodStart;
    }

    /**
     * Setter method for billingPeriodStart.
     *
     * @param aBillingPeriodStart the new value for billingPeriodStart
     */
    public void setBillingPeriodStart(Timestamp aBillingPeriodStart) {
        billingPeriodStart = aBillingPeriodStart;
    }

    /**
     * Access method for billingPeriodEnd.
     *
     * @return the current value of billingPeriodEnd
     */
    public Timestamp getBillingPeriodEnd() {
        return billingPeriodEnd;
    }

    /**
     * Setter method for billingPeriodEnd.
     *
     * @param aBillingPeriodEnd the new value for billingPeriodEnd
     */
    public void setBillingPeriodEnd(Timestamp aBillingPeriodEnd) {
        billingPeriodEnd = aBillingPeriodEnd;
    }

    /**
     * Access method for finalizedDatetime.
     *
     * @return the current value of finalizedDatetime
     */
    public Timestamp getFinalizedDatetime() {
        return finalizedDatetime;
    }

    /**
     * Setter method for finalizedDatetime.
     *
     * @param aFinalizedDatetime the new value for finalizedDatetime
     */
    public void setFinalizedDatetime(Timestamp aFinalizedDatetime) {
        finalizedDatetime = aFinalizedDatetime;
    }

    /**
     * Access method for summaryDataCreationDatetime.
     *
     * @return the current value of summaryDataCreationDatetime
     */
    public Timestamp getSummaryDataCreationDatetime() {
        return summaryDataCreationDatetime;
    }

    /**
     * Setter method for summaryDataCreationDatetime.
     *
     * @param aSummaryDataCreationDatetime the new value for summaryDataCreationDatetime
     */
    public void setSummaryDataCreationDatetime(Timestamp aSummaryDataCreationDatetime) {
        summaryDataCreationDatetime = aSummaryDataCreationDatetime;
    }

    /**
     * Access method for summaryDataUpdatedDatetime.
     *
     * @return the current value of summaryDataUpdatedDatetime
     */
    public Timestamp getSummaryDataUpdatedDatetime() {
        return summaryDataUpdatedDatetime;
    }

    /**
     * Setter method for summaryDataUpdatedDatetime.
     *
     * @param aSummaryDataUpdatedDatetime the new value for summaryDataUpdatedDatetime
     */
    public void setSummaryDataUpdatedDatetime(Timestamp aSummaryDataUpdatedDatetime) {
        summaryDataUpdatedDatetime = aSummaryDataUpdatedDatetime;
    }

    /**
     * Access method for providerId.
     *
     * @return the current value of providerId
     */
    public int getProviderId() {
        return providerId;
    }

    /**
     * Setter method for providerId.
     *
     * @param aProviderId the new value for providerId
     */
    public void setProviderId(int aProviderId) {
        providerId = aProviderId;
    }

    /**
     * Access method for derivedCostDatetime.
     *
     * @return the current value of derivedCostDatetime
     */
    public Timestamp getDerivedCostDatetime() {
        return derivedCostDatetime;
    }

    /**
     * Setter method for derivedCostDatetime.
     *
     * @param aDerivedCostDatetime the new value for derivedCostDatetime
     */
    public void setDerivedCostDatetime(Timestamp aDerivedCostDatetime) {
        derivedCostDatetime = aDerivedCostDatetime;
    }

    /**
     * Access method for costAwsIngestActuallineitem.
     *
     * @return the current value of costAwsIngestActuallineitem
     */
    public Set<CostAwsIngestActuallineitem> getCostAwsIngestActuallineitem() {
        return costAwsIngestActuallineitem;
    }

    /**
     * Setter method for costAwsIngestActuallineitem.
     *
     * @param aCostAwsIngestActuallineitem the new value for costAwsIngestActuallineitem
     */
    public void setCostAwsIngestActuallineitem(Set<CostAwsIngestActuallineitem> aCostAwsIngestActuallineitem) {
        costAwsIngestActuallineitem = aCostAwsIngestActuallineitem;
    }

    /**
     * Access method for costAwsIngestEstimatelineitem.
     *
     * @return the current value of costAwsIngestEstimatelineitem
     */
    public Set<CostAwsIngestEstimatelineitem> getCostAwsIngestEstimatelineitem() {
        return costAwsIngestEstimatelineitem;
    }

    /**
     * Setter method for costAwsIngestEstimatelineitem.
     *
     * @param aCostAwsIngestEstimatelineitem the new value for costAwsIngestEstimatelineitem
     */
    public void setCostAwsIngestEstimatelineitem(Set<CostAwsIngestEstimatelineitem> aCostAwsIngestEstimatelineitem) {
        costAwsIngestEstimatelineitem = aCostAwsIngestEstimatelineitem;
    }

    /**
     * Access method for costAwsIngestEstimatelineitemTest.
     *
     * @return the current value of costAwsIngestEstimatelineitemTest
     */
    public Set<CostAwsIngestEstimatelineitemTest> getCostAwsIngestEstimatelineitemTest() {
        return costAwsIngestEstimatelineitemTest;
    }

    /**
     * Setter method for costAwsIngestEstimatelineitemTest.
     *
     * @param aCostAwsIngestEstimatelineitemTest the new value for costAwsIngestEstimatelineitemTest
     */
    public void setCostAwsIngestEstimatelineitemTest(Set<CostAwsIngestEstimatelineitemTest> aCostAwsIngestEstimatelineitemTest) {
        costAwsIngestEstimatelineitemTest = aCostAwsIngestEstimatelineitemTest;
    }

    /**
     * Access method for costAwsIngestActuallineitemTest.
     *
     * @return the current value of costAwsIngestActuallineitemTest
     */
    public Set<CostAwsIngestActuallineitemTest> getCostAwsIngestActuallineitemTest() {
        return costAwsIngestActuallineitemTest;
    }

    /**
     * Setter method for costAwsIngestActuallineitemTest.
     *
     * @param aCostAwsIngestActuallineitemTest the new value for costAwsIngestActuallineitemTest
     */
    public void setCostAwsIngestActuallineitemTest(Set<CostAwsIngestActuallineitemTest> aCostAwsIngestActuallineitemTest) {
        costAwsIngestActuallineitemTest = aCostAwsIngestActuallineitemTest;
    }

    /**
     * Access method for rprtAwsCostActualsummary.
     *
     * @return the current value of rprtAwsCostActualsummary
     */
    public Set<RprtAwsCostActualsummary> getRprtAwsCostActualsummary() {
        return rprtAwsCostActualsummary;
    }

    /**
     * Setter method for rprtAwsCostActualsummary.
     *
     * @param aRprtAwsCostActualsummary the new value for rprtAwsCostActualsummary
     */
    public void setRprtAwsCostActualsummary(Set<RprtAwsCostActualsummary> aRprtAwsCostActualsummary) {
        rprtAwsCostActualsummary = aRprtAwsCostActualsummary;
    }

    /**
     * Access method for rprtAwsCostActualsummaryTest.
     *
     * @return the current value of rprtAwsCostActualsummaryTest
     */
    public Set<RprtAwsCostActualsummaryTest> getRprtAwsCostActualsummaryTest() {
        return rprtAwsCostActualsummaryTest;
    }

    /**
     * Setter method for rprtAwsCostActualsummaryTest.
     *
     * @param aRprtAwsCostActualsummaryTest the new value for rprtAwsCostActualsummaryTest
     */
    public void setRprtAwsCostActualsummaryTest(Set<RprtAwsCostActualsummaryTest> aRprtAwsCostActualsummaryTest) {
        rprtAwsCostActualsummaryTest = aRprtAwsCostActualsummaryTest;
    }

    /**
     * Access method for rprtAwsCostEstimatesummary.
     *
     * @return the current value of rprtAwsCostEstimatesummary
     */
    public Set<RprtAwsCostEstimatesummary> getRprtAwsCostEstimatesummary() {
        return rprtAwsCostEstimatesummary;
    }

    /**
     * Setter method for rprtAwsCostEstimatesummary.
     *
     * @param aRprtAwsCostEstimatesummary the new value for rprtAwsCostEstimatesummary
     */
    public void setRprtAwsCostEstimatesummary(Set<RprtAwsCostEstimatesummary> aRprtAwsCostEstimatesummary) {
        rprtAwsCostEstimatesummary = aRprtAwsCostEstimatesummary;
    }

    /**
     * Access method for rprtAwsCostEstimatesummaryTest.
     *
     * @return the current value of rprtAwsCostEstimatesummaryTest
     */
    public Set<RprtAwsCostEstimatesummaryTest> getRprtAwsCostEstimatesummaryTest() {
        return rprtAwsCostEstimatesummaryTest;
    }

    /**
     * Setter method for rprtAwsCostEstimatesummaryTest.
     *
     * @param aRprtAwsCostEstimatesummaryTest the new value for rprtAwsCostEstimatesummaryTest
     */
    public void setRprtAwsCostEstimatesummaryTest(Set<RprtAwsCostEstimatesummaryTest> aRprtAwsCostEstimatesummaryTest) {
        rprtAwsCostEstimatesummaryTest = aRprtAwsCostEstimatesummaryTest;
    }

    /**
     * Compares the key for this instance with another CostAwsIngestCurdata.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class CostAwsIngestCurdata and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof CostAwsIngestCurdata)) {
            return false;
        }
        CostAwsIngestCurdata that = (CostAwsIngestCurdata) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another CostAwsIngestCurdata.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CostAwsIngestCurdata)) return false;
        return this.equalKeys(other) && ((CostAwsIngestCurdata)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[CostAwsIngestCurdata |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
