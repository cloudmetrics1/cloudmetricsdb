// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="cost_aws_ingest_product", indexes={@Index(name="costAwsIngestProductCostAwsIngestProductSkuProductNameRegionFea902aeUniq", columnList="sku,product_name,region", unique=true)})
public class CostAwsIngestProduct implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(length=128)
    private String sku;
    @Column(name="product_name")
    private String productName;
    @Column(name="product_family", length=150)
    private String productFamily;
    @Column(name="service_code", length=50)
    private String serviceCode;
    @Column(length=50)
    private String region;
    @Column(name="instance_type", length=50)
    private String instanceType;
    private String memory;
    @Column(name="memory_unit", length=24)
    private String memoryUnit;
    private String vcpu;
    @OneToMany(mappedBy="costAwsIngestProduct")
    private Set<CostAwsIngestActuallineitem> costAwsIngestActuallineitem;
    @OneToMany(mappedBy="costAwsIngestProduct")
    private Set<CostAwsIngestEstimatelineitem> costAwsIngestEstimatelineitem;
    @OneToMany(mappedBy="costAwsIngestProduct")
    private Set<CostAwsIngestEstimatelineitemTest> costAwsIngestEstimatelineitemTest;
    @OneToMany(mappedBy="costAwsIngestProduct")
    private Set<CostAwsIngestActuallineitemTest> costAwsIngestActuallineitemTest;

    /** Default constructor. */
    public CostAwsIngestProduct() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for sku.
     *
     * @return the current value of sku
     */
    public String getSku() {
        return sku;
    }

    /**
     * Setter method for sku.
     *
     * @param aSku the new value for sku
     */
    public void setSku(String aSku) {
        sku = aSku;
    }

    /**
     * Access method for productName.
     *
     * @return the current value of productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Setter method for productName.
     *
     * @param aProductName the new value for productName
     */
    public void setProductName(String aProductName) {
        productName = aProductName;
    }

    /**
     * Access method for productFamily.
     *
     * @return the current value of productFamily
     */
    public String getProductFamily() {
        return productFamily;
    }

    /**
     * Setter method for productFamily.
     *
     * @param aProductFamily the new value for productFamily
     */
    public void setProductFamily(String aProductFamily) {
        productFamily = aProductFamily;
    }

    /**
     * Access method for serviceCode.
     *
     * @return the current value of serviceCode
     */
    public String getServiceCode() {
        return serviceCode;
    }

    /**
     * Setter method for serviceCode.
     *
     * @param aServiceCode the new value for serviceCode
     */
    public void setServiceCode(String aServiceCode) {
        serviceCode = aServiceCode;
    }

    /**
     * Access method for region.
     *
     * @return the current value of region
     */
    public String getRegion() {
        return region;
    }

    /**
     * Setter method for region.
     *
     * @param aRegion the new value for region
     */
    public void setRegion(String aRegion) {
        region = aRegion;
    }

    /**
     * Access method for instanceType.
     *
     * @return the current value of instanceType
     */
    public String getInstanceType() {
        return instanceType;
    }

    /**
     * Setter method for instanceType.
     *
     * @param aInstanceType the new value for instanceType
     */
    public void setInstanceType(String aInstanceType) {
        instanceType = aInstanceType;
    }

    /**
     * Access method for memory.
     *
     * @return the current value of memory
     */
    public String getMemory() {
        return memory;
    }

    /**
     * Setter method for memory.
     *
     * @param aMemory the new value for memory
     */
    public void setMemory(String aMemory) {
        memory = aMemory;
    }

    /**
     * Access method for memoryUnit.
     *
     * @return the current value of memoryUnit
     */
    public String getMemoryUnit() {
        return memoryUnit;
    }

    /**
     * Setter method for memoryUnit.
     *
     * @param aMemoryUnit the new value for memoryUnit
     */
    public void setMemoryUnit(String aMemoryUnit) {
        memoryUnit = aMemoryUnit;
    }

    /**
     * Access method for vcpu.
     *
     * @return the current value of vcpu
     */
    public String getVcpu() {
        return vcpu;
    }

    /**
     * Setter method for vcpu.
     *
     * @param aVcpu the new value for vcpu
     */
    public void setVcpu(String aVcpu) {
        vcpu = aVcpu;
    }

    /**
     * Access method for costAwsIngestActuallineitem.
     *
     * @return the current value of costAwsIngestActuallineitem
     */
    public Set<CostAwsIngestActuallineitem> getCostAwsIngestActuallineitem() {
        return costAwsIngestActuallineitem;
    }

    /**
     * Setter method for costAwsIngestActuallineitem.
     *
     * @param aCostAwsIngestActuallineitem the new value for costAwsIngestActuallineitem
     */
    public void setCostAwsIngestActuallineitem(Set<CostAwsIngestActuallineitem> aCostAwsIngestActuallineitem) {
        costAwsIngestActuallineitem = aCostAwsIngestActuallineitem;
    }

    /**
     * Access method for costAwsIngestEstimatelineitem.
     *
     * @return the current value of costAwsIngestEstimatelineitem
     */
    public Set<CostAwsIngestEstimatelineitem> getCostAwsIngestEstimatelineitem() {
        return costAwsIngestEstimatelineitem;
    }

    /**
     * Setter method for costAwsIngestEstimatelineitem.
     *
     * @param aCostAwsIngestEstimatelineitem the new value for costAwsIngestEstimatelineitem
     */
    public void setCostAwsIngestEstimatelineitem(Set<CostAwsIngestEstimatelineitem> aCostAwsIngestEstimatelineitem) {
        costAwsIngestEstimatelineitem = aCostAwsIngestEstimatelineitem;
    }

    /**
     * Access method for costAwsIngestEstimatelineitemTest.
     *
     * @return the current value of costAwsIngestEstimatelineitemTest
     */
    public Set<CostAwsIngestEstimatelineitemTest> getCostAwsIngestEstimatelineitemTest() {
        return costAwsIngestEstimatelineitemTest;
    }

    /**
     * Setter method for costAwsIngestEstimatelineitemTest.
     *
     * @param aCostAwsIngestEstimatelineitemTest the new value for costAwsIngestEstimatelineitemTest
     */
    public void setCostAwsIngestEstimatelineitemTest(Set<CostAwsIngestEstimatelineitemTest> aCostAwsIngestEstimatelineitemTest) {
        costAwsIngestEstimatelineitemTest = aCostAwsIngestEstimatelineitemTest;
    }

    /**
     * Access method for costAwsIngestActuallineitemTest.
     *
     * @return the current value of costAwsIngestActuallineitemTest
     */
    public Set<CostAwsIngestActuallineitemTest> getCostAwsIngestActuallineitemTest() {
        return costAwsIngestActuallineitemTest;
    }

    /**
     * Setter method for costAwsIngestActuallineitemTest.
     *
     * @param aCostAwsIngestActuallineitemTest the new value for costAwsIngestActuallineitemTest
     */
    public void setCostAwsIngestActuallineitemTest(Set<CostAwsIngestActuallineitemTest> aCostAwsIngestActuallineitemTest) {
        costAwsIngestActuallineitemTest = aCostAwsIngestActuallineitemTest;
    }

    /**
     * Compares the key for this instance with another CostAwsIngestProduct.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class CostAwsIngestProduct and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof CostAwsIngestProduct)) {
            return false;
        }
        CostAwsIngestProduct that = (CostAwsIngestProduct) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another CostAwsIngestProduct.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CostAwsIngestProduct)) return false;
        return this.equalKeys(other) && ((CostAwsIngestProduct)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[CostAwsIngestProduct |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
