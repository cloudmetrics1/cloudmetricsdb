// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Version;

@Entity(name="test_interval")
public class TestInterval implements Serializable {

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    private String timeinterval;
    @Column(name="interval_start")
    private Timestamp intervalStart;
    @Column(name="interval_end")
    private Timestamp intervalEnd;

    /** Default constructor. */
    public TestInterval() {
        super();
    }

    /**
     * Access method for timeinterval.
     *
     * @return the current value of timeinterval
     */
    public String getTimeinterval() {
        return timeinterval;
    }

    /**
     * Setter method for timeinterval.
     *
     * @param aTimeinterval the new value for timeinterval
     */
    public void setTimeinterval(String aTimeinterval) {
        timeinterval = aTimeinterval;
    }

    /**
     * Access method for intervalStart.
     *
     * @return the current value of intervalStart
     */
    public Timestamp getIntervalStart() {
        return intervalStart;
    }

    /**
     * Setter method for intervalStart.
     *
     * @param aIntervalStart the new value for intervalStart
     */
    public void setIntervalStart(Timestamp aIntervalStart) {
        intervalStart = aIntervalStart;
    }

    /**
     * Access method for intervalEnd.
     *
     * @return the current value of intervalEnd
     */
    public Timestamp getIntervalEnd() {
        return intervalEnd;
    }

    /**
     * Setter method for intervalEnd.
     *
     * @param aIntervalEnd the new value for intervalEnd
     */
    public void setIntervalEnd(Timestamp aIntervalEnd) {
        intervalEnd = aIntervalEnd;
    }

}
