// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="mstr_categories")
public class MstrCategories implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(length=200)
    private String name;
    @Column(length=250)
    private String description;
    private String active;
    @OneToMany(mappedBy="mstrCategories")
    private Set<BudgCloudbudgetdetails> budgCloudbudgetdetails;
    @OneToMany(mappedBy="mstrCategories")
    private Set<MstrProducts> mstrProducts;
    @OneToMany(mappedBy="mstrCategories")
    private Set<MstrSubcategories> mstrSubcategories;

    /** Default constructor. */
    public MstrCategories() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for description.
     *
     * @return the current value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter method for description.
     *
     * @param aDescription the new value for description
     */
    public void setDescription(String aDescription) {
        description = aDescription;
    }

    /**
     * Access method for active.
     *
     * @return the current value of active
     */
    public String getActive() {
        return active;
    }

    /**
     * Setter method for active.
     *
     * @param aActive the new value for active
     */
    public void setActive(String aActive) {
        active = aActive;
    }

    /**
     * Access method for budgCloudbudgetdetails.
     *
     * @return the current value of budgCloudbudgetdetails
     */
    public Set<BudgCloudbudgetdetails> getBudgCloudbudgetdetails() {
        return budgCloudbudgetdetails;
    }

    /**
     * Setter method for budgCloudbudgetdetails.
     *
     * @param aBudgCloudbudgetdetails the new value for budgCloudbudgetdetails
     */
    public void setBudgCloudbudgetdetails(Set<BudgCloudbudgetdetails> aBudgCloudbudgetdetails) {
        budgCloudbudgetdetails = aBudgCloudbudgetdetails;
    }

    /**
     * Access method for mstrProducts.
     *
     * @return the current value of mstrProducts
     */
    public Set<MstrProducts> getMstrProducts() {
        return mstrProducts;
    }

    /**
     * Setter method for mstrProducts.
     *
     * @param aMstrProducts the new value for mstrProducts
     */
    public void setMstrProducts(Set<MstrProducts> aMstrProducts) {
        mstrProducts = aMstrProducts;
    }

    /**
     * Access method for mstrSubcategories.
     *
     * @return the current value of mstrSubcategories
     */
    public Set<MstrSubcategories> getMstrSubcategories() {
        return mstrSubcategories;
    }

    /**
     * Setter method for mstrSubcategories.
     *
     * @param aMstrSubcategories the new value for mstrSubcategories
     */
    public void setMstrSubcategories(Set<MstrSubcategories> aMstrSubcategories) {
        mstrSubcategories = aMstrSubcategories;
    }

    /**
     * Compares the key for this instance with another MstrCategories.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MstrCategories and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MstrCategories)) {
            return false;
        }
        MstrCategories that = (MstrCategories) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MstrCategories.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MstrCategories)) return false;
        return this.equalKeys(other) && ((MstrCategories)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MstrCategories |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
