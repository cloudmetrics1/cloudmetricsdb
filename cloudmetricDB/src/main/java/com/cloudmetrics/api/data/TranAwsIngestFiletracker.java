// Generated with g9.

package com.cloudmetrics.api.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="tran_aws_ingest_filetracker")
public class TranAwsIngestFiletracker implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    private String account;
    private String bucket;
    private String filename;
    @Column(precision=131089)
    private BigDecimal records;
    private String processed;
    @Column(name="processed_date")
    private Timestamp processedDate;
    @Column(precision=131089)
    private BigDecimal filesize;

    /** Default constructor. */
    public TranAwsIngestFiletracker() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for account.
     *
     * @return the current value of account
     */
    public String getAccount() {
        return account;
    }

    /**
     * Setter method for account.
     *
     * @param aAccount the new value for account
     */
    public void setAccount(String aAccount) {
        account = aAccount;
    }

    /**
     * Access method for bucket.
     *
     * @return the current value of bucket
     */
    public String getBucket() {
        return bucket;
    }

    /**
     * Setter method for bucket.
     *
     * @param aBucket the new value for bucket
     */
    public void setBucket(String aBucket) {
        bucket = aBucket;
    }

    /**
     * Access method for filename.
     *
     * @return the current value of filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Setter method for filename.
     *
     * @param aFilename the new value for filename
     */
    public void setFilename(String aFilename) {
        filename = aFilename;
    }

    /**
     * Access method for records.
     *
     * @return the current value of records
     */
    public BigDecimal getRecords() {
        return records;
    }

    /**
     * Setter method for records.
     *
     * @param aRecords the new value for records
     */
    public void setRecords(BigDecimal aRecords) {
        records = aRecords;
    }

    /**
     * Access method for processed.
     *
     * @return the current value of processed
     */
    public String getProcessed() {
        return processed;
    }

    /**
     * Setter method for processed.
     *
     * @param aProcessed the new value for processed
     */
    public void setProcessed(String aProcessed) {
        processed = aProcessed;
    }

    /**
     * Access method for processedDate.
     *
     * @return the current value of processedDate
     */
    public Timestamp getProcessedDate() {
        return processedDate;
    }

    /**
     * Setter method for processedDate.
     *
     * @param aProcessedDate the new value for processedDate
     */
    public void setProcessedDate(Timestamp aProcessedDate) {
        processedDate = aProcessedDate;
    }

    /**
     * Access method for filesize.
     *
     * @return the current value of filesize
     */
    public BigDecimal getFilesize() {
        return filesize;
    }

    /**
     * Setter method for filesize.
     *
     * @param aFilesize the new value for filesize
     */
    public void setFilesize(BigDecimal aFilesize) {
        filesize = aFilesize;
    }

    /**
     * Compares the key for this instance with another TranAwsIngestFiletracker.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class TranAwsIngestFiletracker and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof TranAwsIngestFiletracker)) {
            return false;
        }
        TranAwsIngestFiletracker that = (TranAwsIngestFiletracker) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another TranAwsIngestFiletracker.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof TranAwsIngestFiletracker)) return false;
        return this.equalKeys(other) && ((TranAwsIngestFiletracker)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[TranAwsIngestFiletracker |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
